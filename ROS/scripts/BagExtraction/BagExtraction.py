
from __future__ import division
from __future__ import print_function

from datetime import datetime, timedelta
import json
import os

import cv2
from cv_bridge import CvBridge, CvBridgeError

import rosbag
from sensor_msgs.msg import Image

import pandas as pd

XIMEA_TIMESTAMP_YEAR  = 2016
XIMEA_TIMESTAMP_MONTH = 9
XIMEA_TIMESTAMP_DAY   = 7

def totimestamp(dt, epoch = datetime(1970,1,1)):
    td = dt - epoch
    # return td.total_seconds()
    return (td.microseconds + (td.seconds + td.days * 86400) * 10**6) / 10**6

def convert_ximea_timestamp(xts):
    # xts is a string looks like 
    # 19.28.53.1_2280\n

    # Split xts by period.
    parts = xts.strip().split(".")
    subSecondAndSeq = parts[-1].split("_")

    # Compose a datetime object.
    dt = datetime( \
        XIMEA_TIMESTAMP_YEAR, XIMEA_TIMESTAMP_MONTH, XIMEA_TIMESTAMP_DAY, \
        int(parts[0]), int(parts[1]), int(parts[2]), int(subSecondAndSeq[0]) * 100000 )

    secondsSinceEpoch = totimestamp( dt )

    return secondsSinceEpoch, int( subSecondAndSeq[1] )

class BagTopic(object):
    def __init__(self, topics, folderNames):
        self.topics = topics # A list contains the topics to be extracted.

        self.folderNames = folderNames

        self.isDebug = False
        self.nDebugMessages = 30
    
    def set_debug(self, nMessages = 30):
        self.isDebug = True
        if ( nMessages >= 1 ):
            self.nDebugMessages = (int)( nMessages )

    def process(self, bagFile, workingDir):
        # workingDir is the directory to save the data.
        # Test if the folder exists.
        for fn in self.folderNames:
            saveDir = workingDir + "/" + fn
            if ( False == os.path.isdir(saveDir) ):
                os.makedirs(saveDir)
                print("Create folder %s" % (saveDir))
            else:
                print("Found folder %s" % (saveDir))

class StereoTopicSteady(BagTopic):
    def __init__(self, topics, folderNames = ["Stereo/Left", "Stereo/Right"], indexToBeSaved = 5):
        super(StereoTopicSteady, self).__init__(topics, folderNames)

        self.ext = ".jpg"
        self.saveParams = [cv2.IMWRITE_JPEG_QUALITY, 100]

        self.indexToBeSaved = indexToBeSaved

    def process(self, bagFiles, workingDir):
        """bagFiles need to be the actual filenames."""
        super(StereoTopicSteady, self).process(None, workingDir)

        print("StereoTopic begins processing...")

        # process all the data in the bagfile.
        bridge = CvBridge()

        countLeft  = 0
        countRight = 0
        countParam = 0
        countTotal = 0

        indexToBeSavedLeft  = 0
        indexToBeSavedRight = 0
        indexToBeSavedParam = 0

        # === Lists for CSV files. ===
        
        # Image parameters.
        bagId     = []
        seq       = []
        tsSec_0   = []
        tsUSec_0  = []
        exp_0     = []
        gain_0    = []
        mb_0      = []
        tsSec_1   = []
        tsUSec_1  = []
        exp_1     = []
        gain_1    = []
        mb_1      = []

        nBagFiles = len( bagFiles )

        for ibf in range(nBagFiles):
            print("Bagfile index = %d." % (ibf))
            bagFile = rosbag.Bag(bagFiles[ibf])

            nMsgLeft  = bagFile.get_message_count( self.topics[0] )
            nMsgRight = bagFile.get_message_count( self.topics[1] )
            nMsgParam = bagFile.get_message_count( self.topics[2] )

            localCountLeft  = 0
            localCountRight = 0
            localCountParam = 0

            if ( self.indexToBeSaved >= nMsgLeft ):
                indexToBeSavedLeft = nMsgLeft - 1
            else:
                indexToBeSavedLeft = self.indexToBeSaved
            
            if ( self.indexToBeSaved >= nMsgRight ):
                indexToBeSavedRight = nMsgLeft - 1
            else:
                indexToBeSavedRight = self.indexToBeSaved

            if ( self.indexToBeSaved >= nMsgParam ):
                indexToBeSavedParam = nMsgParam - 1
            else:
                indexToBeSavedParam = self.indexToBeSaved

            for topic, msg, t in bagFile.read_messages(self.topics):
                
                if ( self.topics[0] == topic ):
                    if ( localCountLeft == indexToBeSavedLeft ):
                        # It should be a image message topic.
                        # Convert msg to OpenCV image.
                        cvImage = bridge.imgmsg_to_cv2(msg, "bgr8")

                        # Save the image.
                        filename = "%s/%s/%04d%s" % (workingDir, self.folderNames[0], countLeft, self.ext)
                        cv2.imwrite(filename, cvImage, self.saveParams)

                        print("%d: %s saved." % (countTotal, filename))

                        countLeft += 1
                    else:
                        print("%d: Ommit %d/%d left images." % (countTotal, localCountLeft+1, indexToBeSavedLeft))

                    localCountLeft += 1

                elif ( self.topics[1] == topic ):
                    if ( localCountRight == indexToBeSavedRight ):
                        # It should be a image message topic.
                        # Convert msg to OpenCV image.
                        cvImage = bridge.imgmsg_to_cv2(msg, "bgr8")

                        # Save the image.
                        filename = "%s/%s/%04d%s" % (workingDir, self.folderNames[1], countRight, self.ext)
                        cv2.imwrite(filename, cvImage, self.saveParams)

                        print("%d: %s saved." % (countTotal, filename))

                        countRight += 1
                    else:
                        print("%d: Ommit %d/%d right images." % (countTotal, localCountRight+1, indexToBeSavedRight))
                    
                    localCountRight += 1

                elif ( self.topics[2] == topic ):
                    if ( localCountParam == indexToBeSavedParam ):
                        # The parameter topic.
                        
                        # Convert the plain string into dict.
                        d = json.loads( msg.data )

                        # Save the information to the lists.
                        bagId.append( os.path.splitext( os.path.split( bagFiles[ibf] )[1] )[0] )
                        seq.append( d["seq"] )
                        tsSec_0.append( d["cams"][0]["tsSec"] )
                        tsUSec_0.append( d["cams"][0]["tsUSec"] )
                        exp_0.append( d["cams"][0]["exp"] )
                        gain_0.append( d["cams"][0]["gain"] )
                        mb_0.append( d["cams"][0]["mb"] )

                        tsSec_1.append( d["cams"][1]["tsSec"] )
                        tsUSec_1.append( d["cams"][1]["tsUSec"] )
                        exp_1.append( d["cams"][1]["exp"] )
                        gain_1.append( d["cams"][1]["gain"] )
                        mb_1.append( d["cams"][1]["mb"] )

                        countParam += 1

                        print("%d: %d parameters." % (countTotal, countParam) )
                    else:
                        print("%d: Ommit %d/%d parameter messages." % (countTotal, localCountParam+1, indexToBeSavedParam))

                    localCountParam += 1
                else:
                    # This is an error.
                    print("Error. Unexpected topic: %s." % (topic))

                countTotal += 1

                if ( localCountLeft  >= indexToBeSavedLeft + 1 and \
                     localCountRight >= indexToBeSavedRight + 1 and \
                     localCountParam >= indexToBeSavedParam + 1 ):
                    print("Done with bagfile %s." % (bagFiles[ibf]))
                    break
                
                if ( True == self.isDebug and countTotal == self.nDebugMessages ):
                    print("Debug mode is on. Stop here...")
                    break
            
        print("%d left and %d right images processed." % (countLeft, countRight))
        print("%d image parameters processed." % (countParam))
        print("%d messages processed." % (countTotal))

        print("Create CSV files...")

        # Stereo parameters.
        filename = "%s/%s/%s" % (workingDir, self.folderNames[2], "StereoParameters.csv")
        if ( 0 != len(bagId) ):
            df = pd.DataFrame( \
                data = { "bagID": bagId, "seq": seq, \
                    "tsSec_0": tsSec_0, "tsUSec_0": tsUSec_0, "exp_0": exp_0, "gain_0": gain_0, "mb_0": mb_0, \
                    "tsSec_1": tsSec_1, "tsUSec_1": tsUSec_1, "exp_1": exp_1, "gain_1": gain_1, "mb_1": mb_1 }, \
                columns = ["bagID", "seq", "tsSec_0", "tsSec_1", "tsUSec_0", "tsUSec_1", "exp_0", "exp_1", "gain_0", "gain_1", "mb_0", "mb_1" ] )
            df.to_csv(filename, index = False)
            print("%s saved." % (filename))
        else:
            if ( True == os.path.isfile(filename) ):
                os.remove(filename)
            print("No stereo parameters found. %s will be deleted if present." % (filename))

        print("StereoTopicSteady ends.")

        if ( True == self.isDebug ):
            print("=== Debug mode is on with nDebugMessages = %d. ===\n" % (self.nDebugMessages))

class StereoTopic(BagTopic):
    def __init__(self, topics, folderNames = ["Stereo/Left", "Stereo/Right"]):
        super(StereoTopic, self).__init__(topics, folderNames)

        self.ext = ".jpg"
        self.saveParams = [cv2.IMWRITE_JPEG_QUALITY, 100]

    def process(self, bagFile, workingDir):
        super(StereoTopic, self).process(bagFile, workingDir)

        print("StereoTopic begins processing...")

        # process all the data in the bagfile.
        bridge = CvBridge()

        countLeft  = 0
        countRight = 0
        countParam = 0
        countXTS   = 0
        countTotal = 0

        nMessages = bagFile.get_message_count()

        # === Lists for CSV files. ===
        # Left camera images.
        bagTime_L = []
        hSeq_L    = []
        hStamp_L  = []
        # Right camera images.
        bagTime_R = []
        hSeq_R    = []
        hStamp_R  = []
        # Image parameters.
        bagTime_P = []
        seq_P     = []
        tsSec_0   = []
        tsUSec_0  = []
        exp_0     = []
        gain_0    = []
        mb_0      = []
        tsSec_1   = []
        tsUSec_1  = []
        exp_1     = []
        gain_1    = []
        mb_1      = []
        # Ximea timestamps.
        bagTime_X = []
        tNo       = [] # Trigger No.
        tTime     = [] # Trigger time.

        for topic, msg, t in bagFile.read_messages(self.topics):
            if ( self.topics[0] == topic ):
                # It should be a image message topic.
                # Convert msg to OpenCV image.
                cvImage = bridge.imgmsg_to_cv2(msg, "bgr8")

                # Save the image.
                filename = "%s/%s/%04d%s" % (workingDir, self.folderNames[0], countLeft, self.ext)
                cv2.imwrite(filename, cvImage, self.saveParams)

                print("%d/%d: %s saved." % (countTotal, nMessages, filename))

                # Save the information to the lists.
                bagTime_L.append( t.to_time() )
                hSeq_L.append( msg.header.seq )
                hStamp_L.append( msg.header.stamp.to_time() )

                countLeft += 1

            elif ( self.topics[1] == topic ):
                # It should be a image message topic.
                # Convert msg to OpenCV image.
                cvImage = bridge.imgmsg_to_cv2(msg, "bgr8")

                # Save the image.
                filename = "%s/%s/%04d%s" % (workingDir, self.folderNames[1], countRight, self.ext)
                cv2.imwrite(filename, cvImage, self.saveParams)

                print("%d/%d: %s saved." % (countTotal, nMessages, filename))

                # Save the information to the lists.
                bagTime_R.append( t.to_time() )
                hSeq_R.append( msg.header.seq )
                hStamp_R.append( msg.header.stamp.to_time() )

                countRight += 1

            elif ( self.topics[2] == topic ):
                # The parameter topic.
                
                # Convert the plain string into dict.
                d = json.loads( msg.data )

                # Save the information to the lists.
                bagTime_P.append( t.to_time() )
                seq_P.append( d["seq"] )
                tsSec_0.append( d["cams"][0]["tsSec"] )
                tsUSec_0.append( d["cams"][0]["tsUSec"] )
                exp_0.append( d["cams"][0]["exp"] )
                gain_0.append( d["cams"][0]["gain"] )
                mb_0.append( d["cams"][0]["mb"] )

                tsSec_1.append( d["cams"][1]["tsSec"] )
                tsUSec_1.append( d["cams"][1]["tsUSec"] )
                exp_1.append( d["cams"][1]["exp"] )
                gain_1.append( d["cams"][1]["gain"] )
                mb_1.append( d["cams"][1]["mb"] )

                countParam += 1

                print("%d/%d: %d parameters." % (countTotal, nMessages, countParam) )

            elif ( self.topics[3] == topic ):
                # The timestamp topic.

                # Parse the time.
                seconds, seq = convert_ximea_timestamp(msg.data)

                # Save the information to the lists.
                bagTime_X.append( t.to_time() )
                tNo.append( seq )
                tTime.append( seconds )

                countXTS += 1

                print("%d/%d: %d XTSs." % (countTotal, nMessages, countXTS) )
            else:
                # This is an error.
                print("Error. Unexpected topic: %s." % (topic))

            countTotal += 1

            if ( True == self.isDebug and countTotal == self.nDebugMessages ):
                print("Debug mode is on. Stop here...")
                break
        
        print("%d left and %d right images processed." % (countLeft, countRight))
        print("%d image parameters processed." % (countParam))
        print("%d ximea timestamp processed." % (countXTS))
        print("%d/%d messages processed." % (countTotal, nMessages))

        print("Create CSV files...")
        
        # Left camera.
        filename = "%s/%s/%s" % (workingDir, self.folderNames[0], "StereoImages.csv")
        if ( 0 != len(bagTime_L) ):
            df = pd.DataFrame( \
                data = { "bagTime": bagTime_L, "hSeq": hSeq_L, "hStamp": hStamp_L }, \
                columns = [ "bagTime", "hSeq", "hStamp" ] )
            df.to_csv(filename, index = False)
            print("%s saved." % (filename))
        else:
            if ( True == os.path.isfile(filename) ):
                os.remove(filename)
            print("No Left images found. %s will be deleted if present." % (filename))

        # Right camera.
        filename = "%s/%s/%s" % (workingDir, self.folderNames[1], "StereoImages.csv")
        if ( 0 != len(bagTime_R) ):
            df = pd.DataFrame( \
                data = { "bagTime": bagTime_R, "hSeq": hSeq_R, "hStamp": hStamp_R }, \
                columns = [ "bagTime", "hSeq", "hStamp" ] )
            df.to_csv(filename, index = False)
            print("%s saved." % (filename))
        else:
            if ( True == os.path.isfile(filename) ):
                os.remove(filename)
            print("No Right images found. %s will be deleted if present." % (filename))

        # Stereo parameters.
        filename = "%s/%s/%s" % (workingDir, self.folderNames[2], "StereoParameters.csv")
        if ( 0 != len(bagTime_P) ):
            df = pd.DataFrame( \
                data = { "bagTime": bagTime_P, "seq": seq_P, \
                    "tsSec_0": tsSec_0, "tsUSec_0": tsUSec_0, "exp_0": exp_0, "gain_0": gain_0, "mb_0": mb_0, \
                    "tsSec_1": tsSec_1, "tsUSec_1": tsUSec_1, "exp_1": exp_1, "gain_1": gain_1, "mb_1": mb_1 }, \
                columns = ["bagTime", "seq", "tsSec_0", "tsSec_1", "tsUSec_0", "tsUSec_1", "exp_0", "exp_1", "gain_0", "gain_1", "mb_0", "mb_1" ] )
            df.to_csv(filename, index = False)
            print("%s saved." % (filename))
        else:
            if ( True == os.path.isfile(filename) ):
                os.remove(filename)
            print("No stereo parameters found. %s will be deleted if present." % (filename))

        # Ximea timestamps.
        filename = "%s/%s/%s" % (workingDir, self.folderNames[3], "StereoXTS.csv")
        if ( 0 != len(bagTime_X) ):
            df = pd.DataFrame( \
                data = { "bagTime": bagTime_X, "tNo": tNo, "tTime": tTime }, \
                columns = [ "bagTime", "tNo", "tTime" ] )
            df.to_csv(filename, index = False)
            print("%s saved." % (filename))
        else:
            if ( True == os.path.isfile(filename) ):
                os.remove(filename)
            print("No ximea timestamps found. %s will be deleted if present." % (filename))

        print("StereoTopic ends.")

        if ( True == self.isDebug ):
            print("=== Debug mode is on with nDebugMessages = %d. ===\n" % (self.nDebugMessages))
        
if __name__ == "__main__":
    # Test.

    # # Test StereoTopic.
    # bag = rosbag.Bag("/home/yyhu/expansion/Shimizu/20180923/2018-09-23-16-16-55.bag")

    # stereoTopic = StereoTopic(\
    #     ["/xic_stereo/left/image_raw", "/xic_stereo/right/image_raw", "/xic_stereo/sxc_test_msg", "/ximea_timestamp"],\
    #     ["Stereo/Left", "Stereo/Right", "Stereo", "Stereo"])

    # stereoTopic.process(bag, "/home/yyhu/expansion/Shimizu/20180923")

    # Test StereoTopicSteady.
    bagfiles = [ \
        "/home/yyhu/expansion/Shimizu/sep27/bags/camera/camera_data_01.bag", \
        "/home/yyhu/expansion/Shimizu/sep27/bags/camera/camera_data_02.bag", \
        "/home/yyhu/expansion/Shimizu/sep27/bags/camera/camera_data_03.bag"  \
        ""
        ]
    
    sts = StereoTopicSteady(["/xic_stereo/left/image_raw", "/xic_stereo/right/image_raw", "/xic_stereo/sxc_test_msg"],\
        ["color/left", "color/right", "color"])
    
    sts.process( bagfiles, "/home/yyhu/expansion/Shimizu/sep27/images" )
