from __future__ import division
from __future__ import print_function

import argparse
import math
import numpy as np
import pandas as pd
import os

STEREO_IMAGES_0   = "StereoImages.csv"
STEREO_IMAGES_1   = "StereoImages.csv"
STEREO_PARAMETERS = "StereoParameters.csv"
STEREO_XTS        = "StereoXTS.csv"

def find_first(sxts, si, timeLag):
    """
    sxts: A pandas dataframe object holding the stereo ximea timestamps.
    si: A pandas dataframe object holding the images ROS timestamps.
    timeLag: A positive floating point value representing the required time lag between a trigger
        signal and a ROS timestamp for an image. Unit is second.
    return: A integer representing the starting index in sxts.
    """

    assert( timeLag > 0.0 )

    # Check the number of entries in both the dataframes.
    if ( len(sxts) < len(si) ):
        raise Exception("sxts (%d) has fewer rows than si (%d)." % (len(sxts), len(si)))

    # Just transfer everything into NumPy.
    dSxts = sxts.values
    dSi   = si.values

    # Find the closest one.
    if ( dSxts[0, 0] > dSi[0, 0] ):
        raise Exception("sxts has later starting time.")

    idx = np.abs( dSxts[:, 0] - dSi[0, 0] ).argmin()

    if ( dSxts[idx, 0] > dSi[0, 0] - timeLag ):
        idx -= 1
    
    if ( idx < 0 ):
        raise Exception("No valid starting index found.")
    
    # Double check.
    if ( dSxts[idx, 0] > dSi[0, 0] - timeLag ):
        idx -= 1
    
    if ( idx < 0 ):
        raise Exception("No valid starting index found.")

    return idx

def align_images(sxts, si, sp, startingIdx):
    """
    sxts: A pandas dataframe object holding the stereo ximea timestamps.
    si: A pandas dataframe object holding the images ROS timestamps.
    sp: A pandas dataframe object holding the image parameters. This parameters only contains 
        the information of one camera. Thus the suffix like "_0" is removed from the column header.
    return: A NumPy array of valid image seq values. Contains np.nan.
    """

    # Check the lengths.
    nSxts = len(sxts)

    if ( nSxts <= startingIdx ):
        raise Exception("Wrong startigIdx (%d)." % startingIdx)

    # Calculate the time differences between the
    # tTime value of sxts and the tsSec and tsUSec values
    # of sp.
    tsSPFirst = sp.loc[0, "tsSec"] + sp.loc[0, "tsUSec"] / 1e6
    tsDiff    = tsSPFirst - sxts.loc[startingIdx, "tTime"]

    # Shift all the timestamps.
    spTimestampCols = sp[["tsSec", "tsUSec"]].values
    tsSP = spTimestampCols[:,0] + spTimestampCols[:,1] / 1e6 - tsDiff

    nSi = len(si)
    nSp = len(sp)
    N   = nSxts - startingIdx # Includes the startingIdx.

    seqArray = np.zeros((N), dtype = np.int)
    idxSI    = 0
    i        = 0

    # Looping from startingIdx.
    while ( i < N ):
        if ( idxSI >= nSi or idxSI >= nSp):
            seqArray[i] = -1
            i += 1
            continue

        idx = startingIdx + i
            
        if ( 0 == i ):
            # Put the first seq value in seqArray.
            seqArray[i] = si.loc[idxSI, "hSeq"]
            previousTS_X = sxts.loc[idx, "tTime"]
            previousTS_I = tsSP[idxSI]
            idxSI += 1
            i += 1
            continue
        
        # The ximea timestamp measure.
        crntTS_X = sxts.loc[idx, "tTime"] # Current timestamp from ximea timestamp.
        diffTS_X = crntTS_X - previousTS_X

        # The image timestamp measure.
        crntTS_I = tsSP[idxSI] # Current timestamp from image timestamp.
        diffTS_I = crntTS_I - previousTS_I

        if ( math.fabs( diffTS_X - diffTS_I ) < 0.05 ):
            # Good alignment.
            seqArray[i] = si.loc[idxSI, "hSeq"]
            idxSI += 1

            previousTS_I = crntTS_I
            previousTS_X = crntTS_X

            i += 1
        else:
            if ( diffTS_I > diffTS_X ):
                seqArray[i] = np.nan

                previousTS_X = crntTS_X
                i += 1
            else:
                idxSI += 1
                previousTS_I = crntTS_I

    return seqArray

def align_stereo_images(workingDir, dir0 = "Left", dir1 = "Right"):
    SI_0 = workingDir + "/" + dir0 + "/" + STEREO_IMAGES_0
    SI_1 = workingDir + "/" + dir1 + "/" + STEREO_IMAGES_1
    SP   = workingDir + "/" + STEREO_PARAMETERS
    SXTS = workingDir + "/" + STEREO_XTS
    
    # Test if workingDir exits.
    if ( False == os.path.isdir(workingDir) ):
        raise Exception("%s does not exist." % (workingDir))
    
    # Test if SI_0 exits.
    if ( False == os.path.isfile(SI_0) ):
        raise Exception("%s does not exist." % (SI_0))

    # Test if SI_1 exits.
    if ( False == os.path.isfile(SI_1) ):
        raise Exception("%s does not exist." % (SI_1))

    # Test if SP exits.
    if ( False == os.path.isfile(SP) ):
        raise Exception("%s does not exist." % (SP))

    # Test if SXTS exits.
    if ( False == os.path.isfile(SXTS) ):
        raise Exception("%s does not exist." % (SXTS))

    # === All files exist. ===

    # Open all the CSV files.

    si_0 = pd.read_csv(SI_0)
    si_1 = pd.read_csv(SI_1)
    sp   = pd.read_csv(SP)
    sxts = pd.read_csv(SXTS)

    print("%s loaded." % (SI_0))
    print("%s loaded." % (SI_1))
    print("%s loaded." % (SP))
    print("%s loaded." % (SXTS))

    # Find the starting index.
    startingIdx = find_first(sxts, si_0, 0.1)
    print("startingIdx = %d." % (startingIdx))

    # Get the alignment of camera 0.
    sp_0 = sp[["tsSec_0", "tsUSec_0"]].copy()
    sp_0.rename( columns = {"tsSec_0": "tsSec", "tsUSec_0": "tsUSec"}, inplace = True )
    seqArray_0 = align_images( sxts, si_0, sp_0, startingIdx )

    # Get the alignment of camera 1.
    sp_1 = sp[["tsSec_1", "tsUSec_1"]].copy()
    sp_1.rename( columns = {"tsSec_1": "tsSec", "tsUSec_1": "tsUSec"}, inplace = True )
    seqArray_1 = align_images( sxts, si_1, sp_1, startingIdx )

    # dfAlignment.
    dfAlignment = pd.DataFrame( \
        data = { "tNo": sxts["tNo"].values[startingIdx:], "hSeq_0": seqArray_0, "hSeq_1": seqArray_1 }, \
        columns = [ "tNo", "hSeq_0", "hSeq_1" ] )
    
    # Save the alignment to filesystem.
    filename = workingDir + "/Aligned.csv"
    dfAlignment.to_csv(filename, index = False)
    print("%s is written." % (filename))
    
if __name__ == "__main__":
    # Test.
    # align_stereo_images("/home/yyhu/expansion/Shimizu/20180923/2018-09-23-16-16-55/Stereo")
    # align_stereo_images("/home/yyhu/expansion/Shimizu/20180925/camera_data_1/Stereo")

    parser = argparse.ArgumentParser(description='Align the image information for an unsteady scene..')

    parser.add_argument("--input_dir", help = "The the directory in which the images have been extracted.", default = "../images/calibration/stereo_calibration_01")
    parser.add_argument("--dir_0", help = "The 0 directory.", default = "Left")
    parser.add_argument("--dir_1", help = "The 1 directory.", default = "Right")

    args = parser.parse_args()

    align_stereo_images(args.input_dir, args.dir_0, args.dir_1)
