
from __future__ import print_function

import argparse
import copy
import cv2
import glob
import json
import math
import numpy as np
import numpy.linalg as LA
import os
import pcl

from ColorMapping import color_map

DEFAULT_INPUT = "input.json"

ply_header = '''ply
format ascii 1.0
element vertex %(vert_num)d
property float x
property float y
property float z
property uchar red
property uchar green
property uchar blue
end_header
'''

PLY_COLORS = [\
    "#2980b9",\
    "#27ae60",\
    "#f39c12",\
    "#c0392b",\
    ]

PLY_COLOR_LEVELS = 20

UPWARDS_Y_R = np.eye(3, dtype = np.float)
UPWARDS_Y_R[1, 1] = -1.0
UPWARDS_Y_R[2, 2] = -1.0

def show_delimiter(title = "", c = "=", n = 50, leading = "\n", ending = "\n"):
    d = [c for i in range(n/2)]
    s = "".join(d) + " " + title + " " + "".join(d)

    print("%s%s%s" % (leading, s, ending))

def write_ply(fn, verts, colors):
    verts  = verts.reshape(-1, 3)
    colors = colors.reshape(-1, 3)
    verts  = np.hstack([verts, colors])

    with open(fn, 'wb') as f:
        f.write((ply_header % dict(vert_num=len(verts))).encode('utf-8'))
        np.savetxt(f, verts, fmt='%f %f %f %d %d %d ')

def depth_to_color(depth, limit = None):

    d  = copy.deepcopy(depth)
    if ( limit is not None ):
        d[ d>limit ] = limit

    color = np.zeros((depth.shape[0], depth.shape[1], 3), dtype = float)
    color[:, :, 0] = d
    color[:, :, 1] = d
    color[:, :, 2] = d

    color = ( color - d.min() ) / ( d.max() - d.min() ) * 255
    color = color.astype(np.uint8)

    return color

def output_to_ply(fn, X, rLimit, origin):
    rv = copy.deepcopy(X)
    rv[:, 0] = X[:, 0] - origin[0, 0]
    rv[:, 1] = X[:, 1] - origin[1, 0]
    rv[:, 2] = X[:, 2] - origin[2, 0]

    r = LA.norm(rv, axis=1).reshape((-1,1))
    mask = r < rLimit
    mask = mask.reshape(( mask.size ))

    r = r[ mask ]

    cr, cg, cb = color_map(r, PLY_COLORS, PLY_COLOR_LEVELS)

    colors = np.zeros( (r.size, 3), dtype = np.uint8 )

    colors[:, 0] = cr.reshape( cr.size )
    colors[:, 1] = cg.reshape( cr.size )
    colors[:, 2] = cb.reshape( cr.size )

    write_ply(fn, X[mask, :], colors)

def filter_in_sight_points(points, cm, sensor):
    """
    points: A three-row NumPy array. Each column is a 3D point coordiante.
    cm: The camera projection matrix. 3x3.
    sensor: A two-element array-like, the height (0) and width (1) of the camera sensor in pixel.
    """

    # Calculate the FOV of the camera.
    fx = cm[0, 0]
    fy = cm[1, 1]

    # The diagonal angle.
    a = math.atan2( math.sqrt( sensor[1]**2 + sensor[0]**2 ) / 2, min(fx, fy) )

    # Filter the points.
    r  = np.sqrt( points[0, :] * points[0, :] + points[1, :] * points[1, :] )
    ar = np.arctan2( r, points[2, :] )

    mask = ar <= a

    return points[:, mask]

def compose_depth_map_from_2D_points(points, imageSize, depths, defualtDepth = 0):
    """
    points: A two-row NumPy array. Each column is a 2D point coordiante on a image plane.
    imageSize: An two-element array-like. The height and width of the image.
    depths: A 1D NumPy array contains the depths corresponding to points.
    defualtDepth: The depth value assigned to the pixel where no valid depth is available.

    NOTE: Coordinate in points will be rounded to its nearest integer.
    """

    # Round up to the nearest integers.
    iPoints = ( points + 0.5 ).astype(np.int)

    # Crop out all the outliers.
    mask0 = iPoints[0, :] >= 0
    mask1 = iPoints[0, :] < imageSize[1]
    mask2 = iPoints[1, :] >= 0
    mask3 = iPoints[1, :] < imageSize[0]

    mask = np.logical_and( mask3, np.logical_and(mask2, np.logical_and( mask1, mask0 ) ) )

    m = iPoints[ :, mask ]
    d = depths[ mask ]

    # Compose the map.
    dm = np.ones((imageSize[0], imageSize[1]), dtype = np.float) * defualtDepth

    # Compose the indices.
    dm[ m[1, :], m[0, :] ] = d

    return dm, mask

def convert_depth_map_to_grayscale(dm):
    """
    All pixel values that are originally NaN or Inf will be changed to zeros.
    """

    # Make a deep copy of dm.
    img = copy.deepcopy(dm)

    # Change all the infinite values to zeros.
    mask = np.logical_not( np.isfinite( img ) )
    img[mask] = 0

    maxDm = np.max(img)

    img = ( img / maxDm * 255 ).astype(np.uint8)

    return img

def gather_from_point_cloud(params, flagDebug):
    workDir = params["workDir"]

    outPath = workDir + "/" + params["outDir"]
    if ( False == os.path.isdir(outPath) ):
        os.makedirs( outPath )
    
    # Scan all the files to be processed.
    filePattern = workDir + "/*." + params["ext"]
    pcFiles = glob.glob(filePattern)
    pcFiles.sort()
    
    # Read all the point cloud files.
    print("%d files to be read in %s." % (len(pcFiles), workDir))

    pcList = []
    count = 0

    for pcf in pcFiles:
        p = pcl.load(pcf).to_array()
        pcList.append( p )
        count += p.shape[0]

        print("Process {}, shape = {}".format(pcf, p.shape))

    print("%d points in total." % (count))

    # Make a single NumPy array to hold all the points.
    pca = np.concatenate( pcList, axis = 0 ).transpose()

    print("pca.shape = {}".format(pca.shape))

    # Save this NumPy array to file.
    outFile = outPath + "/pca.npy"
    np.save(outFile, pca)
    print("%s is written." % (outFile))

    # Write all the points into PLY file.
    if ( True == flagDebug ):
        outFile = outPath + "/Debug_pca.ply"
        output_to_ply(\
            outFile, pca.transpose(), 50, np.zeros((3,1)) )
        print("%s is written." % (outFile))

    # Load the transfomation matrices.
    m2LT = np.array( params["motorBase2LeftCameraTranslation"] ).reshape((3, 1))
    m2LR = np.array( params["motorBase2LeftCameraRotation"] ).reshape((3, 3))

    l2RT = np.loadtxt( params["stereoCameraCalibrationDir"] + "/" + params["l2RT"], dtype = np.float ).reshape((3, 1))
    l2RR = np.loadtxt( params["stereoCameraCalibrationDir"] + "/" + params["l2RR"], dtype = np.float ).reshape((3, 3))

    # Transform the points from the motor base frame to left camera frame.
    pcaLFrame = np.matmul( m2LR, pca ) + m2LT
    
    outFile = outPath + "/pcaLFrame.npy"
    np.save( outFile, pcaLFrame )
    print("%s is written." % (outFile))

    if ( True == flagDebug ):
        outFile = outPath + "/Debug_pcaLFrame.ply"
        output_to_ply(\
            outFile, pcaLFrame.transpose(), 50, np.zeros((3,1)) )
        print("%s is written." % (outFile))

    # Transform the points from the left camera frame to the right camera frame.
    pcaRFrame = np.matmul( l2RR, pcaLFrame ) + l2RT

    outFile = outPath + "/pcaRFrame.npy"
    np.save( outFile, pcaRFrame )
    print("%s is written." % (outFile))

    if ( True == flagDebug ):
        outFile = outPath + "/Debug_pcaRFrame.ply"
        output_to_ply(\
            outFile, pcaRFrame.transpose(), 50, np.zeros((3,1)) )
        print("%s is written." % (outFile))

    # Load the camera matrices and image size.
    cmL = np.loadtxt( params["stereoCameraCalibrationDir"] + "/" + params["cmL"], dtype = np.float )[0:3, 0:3]
    cmR = np.loadtxt( params["stereoCameraCalibrationDir"] + "/" + params["cmR"], dtype = np.float )[0:3, 0:3]
    fp = open( params["stereoCameraCalibrationDir"] + "/" + params["sensorJSON"] )
    sensorJSON = json.load(fp)
    fp.close()
    sensor = ( sensorJSON["height"], sensorJSON["width"] )

    # Filter the points.
    pcaLFF = filter_in_sight_points( pcaLFrame, cmL, sensor )
    pcaRFF = filter_in_sight_points( pcaRFrame, cmR, sensor )

    # Save the filtered points.
    outFile = outPath + "/pcaLFF.npy"
    np.save(outFile, pcaLFF)
    print("%s is written." % (outFile))

    outFile = outPath + "/pcaRFF.npy"
    np.save(outFile, pcaRFF)
    print("%s is written." % (outFile))

    print("Average depths are %f, %f." % (np.mean( pcaLFF[2, :] ), np.mean( pcaRFF[2, :] )))

    if ( True == flagDebug ):
        outFile = outPath + "/Debug_pcaLFF.ply"
        output_to_ply(\
            outFile, pcaLFF.transpose(), 50, np.zeros((3,1)) )
        print("%s is written." % (outFile))

        outFile = outPath + "/Debug_pcaRFF.ply"
        output_to_ply(\
            outFile, pcaRFF.transpose(), 50, np.zeros((3,1)) )
        print("%s is written." % (outFile))

    # Project the points.
    pcaLP = np.matmul( cmL, pcaLFF )
    pcaRP = np.matmul( cmR, pcaRFF )

    uvL = pcaLP[0:2, :] / pcaLP[2, :]
    uvR = pcaRP[0:2, :] / pcaRP[2, :]

    print("uvL.shape = {}".format( uvL.shape ))
    print("uvR.shape = {}".format( uvR.shape ))

    # Compose depth map.
    dmL, maskL = compose_depth_map_from_2D_points(uvL, sensor, pcaLFF[2, :])
    dmR, maskR = compose_depth_map_from_2D_points(uvR, sensor, pcaRFF[2, :])

    # Save the depth maps.
    outFile = outPath + "/dmL.npy"
    np.save(outFile, dmL)
    print("%s is written." % (outFile))

    outFile = outPath + "/dmR.npy"
    np.save(outFile, dmR)
    print("%s is written." % (outFile))

    # Convert the depth maps into grayscale images.
    dmLG = convert_depth_map_to_grayscale(dmL)
    dmRG = convert_depth_map_to_grayscale(dmR)

    jpegParams = [cv2.IMWRITE_JPEG_QUALITY, 100]

    outFile = outPath + "/dmLG.jpg"
    cv2.imwrite(outFile, dmLG, jpegParams)
    print("Figure %s saved." % (outFile))

    outFile = outPath + "/dmRG.jpg"
    cv2.imwrite(outFile, dmRG, jpegParams)
    print("Figure %s saved." % (outFile))

    if ( True == flagDebug ):
        outFile = outPath + "/Debug_dmL.ply"
        output_to_ply(\
            outFile, pcaLFF[:, maskL].transpose(), 50, np.zeros((3,1)) )
        print("%s is written." % (outFile))

        outFile = outPath + "/Debug_dmR.ply"
        output_to_ply(\
            outFile, pcaRFF[:, maskR].transpose(), 50, np.zeros((3,1)) )
        print("%s is written." % (outFile))

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Gather points from point clouds.")

    parser.add_argument("--input", help = "The filename of the input JSON file.", default = DEFAULT_INPUT)
    parser.add_argument("--debug", help = "Write debug information to file system.", action = "store_true", default = False)

    args = parser.parse_args()

    # Parse the input JSON file.
    fp = open( args.input, "r" )
    params = json.load(fp)
    fp.close()

    gather_from_point_cloud(params, args.debug)

    print("Done.")
