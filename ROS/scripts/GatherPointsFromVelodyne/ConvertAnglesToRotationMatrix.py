
from __future__ import print_function

import argparse
import math
import numpy as np

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Convert three rotation angles (extrinsic rotations) into a rotation matrix.")

    parser.add_argument("x", help = "x-angle.", type = float)
    parser.add_argument("y", help = "y-angle.", type = float)
    parser.add_argument("z", help = "z-angle.", type = float)
    parser.add_argument("--deg", help = "Set this flag for using unit degree.", action = "store_true", default = False)

    args = parser.parse_args()

    if ( True == args.deg ):
        ax = args.x / 180.0 * np.pi
        ay = args.y / 180.0 * np.pi
        az = args.z / 180.0 * np.pi
    else:
        ax = args.x
        ay = args.y
        az = args.z

    rotx = np.eye(3, dtype = np.float)
    rotx[1, 1] =  math.cos(ax)
    rotx[1, 2] = -math.sin(ax)
    rotx[2, 1] =  math.sin(ax)
    rotx[2, 2] =  math.cos(ax)

    roty = np.eye(3, dtype = np.float)
    roty[0, 0] =  math.cos(ay)
    roty[0, 2] = -math.sin(ay)
    roty[2, 0] =  math.sin(ay)
    roty[2, 2] =  math.cos(ay)

    rotz = np.eye(3, dtype = np.float)
    rotz[0, 0] =  math.cos(az)
    rotz[0, 1] = -math.sin(az)
    rotz[1, 0] =  math.sin(az)
    rotz[1, 1] =  math.cos(az)

    R = np.matmul( rotz, np.matmul( roty, rotx ) )

    print(rotx)
    print(roty)
    print(rotz)
    print(R)

    print( np.matmul(R, R.transpose()) )
    
    RL = R.reshape((1, -1))

    for i in range( RL.size ):
        print("%e, " % (RL[0, i]), end = "")

    print("")
