from __future__ import print_function

import cv2
import numpy as np

AVI_FILE  = '../../Zeng/Test/001.avi'
OUT_DIR   = '../../Zeng/Test/OriImages'

WIDTH     = 2560
HEIGHT    = 720
HALF      = WIDTH / 2
SKIP_STEP = 10

if __name__ == "__main__":
    # Create a VideoCapture object and read from input file
    # If the input is the camera, pass 0 instead of the video file name
    cap = cv2.VideoCapture(AVI_FILE)
    
    # Check if camera opened successfully
    if (cap.isOpened()== False): 
        print("Error opening video stream or file")
    
    count = 0

    # Read until video is completed
    while(cap.isOpened()):
        # Capture frame-by-frame
        ret, frame = cap.read()

        if ( SKIP_STEP != 0 and count % SKIP_STEP != 0 ):
            count += 1
            continue

        if ret == True:
            print("Image frame %d." % (count))

            # Display the resulting frame
            cv2.imshow('Frame', frame)

            # Save the frame to file.
            filename = "%s/%04d.jpg" % ( OUT_DIR, count )
            cv2.imwrite(filename, frame)
        
            # Press Q on keyboard to  exit
            if cv2.waitKey(500) & 0xFF == ord('q'):
                break
        # Break the loop
        else:
            print("Failed to get frame %d." % (count))
            break

        count += 1

    # When everything done, release the video capture object
    cap.release()
    
    # Closes all the frames
    cv2.destroyAllWindows()

    print("Done.")