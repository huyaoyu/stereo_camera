import cv2
import numpy as np
import glob
import copy


def thermal_to_visual(PARAM_PATH,thermal_path,working_Dir,img_3d,rectified_left):
	#load params
	H_t=np.loadtxt(PARAM_PATH+"/"+"flir_mtx.dat")
	H_t[0][2]=H_t[0][2]-1
	H_t[1][2]=H_t[1][2]
	dist_t=np.loadtxt(PARAM_PATH+"/"+"flir_dist.dat")
	RT=np.loadtxt(PARAM_PATH+"/"+"RT.dat")
	R=RT[0:3,0:3]
	T=RT[0:3,3]
	T=np.array([[T[0]],
       			[T[1]],
       			[T[2]]],dtype=np.float32)
	
	dst_t=cv2.imread(thermal_path)
	dst_v=rectified_left.copy()
	ht,wt=dst_t.shape[:2]
	out=dst_v.copy()

	#undistort image and get undistort camera matrix
	new_Ht,roi=cv2.getOptimalNewCameraMatrix(H_t,dist_t,(wt,ht),1,(wt,ht))
	dst_t=cv2.undistort(dst_t,H_t,dist_t,None,new_Ht)
	H_t=new_Ht

	img_3d=img_3d*1000
	img_3d[:,:,0]=-img_3d[:,:,0]
	depth=np.reshape(img_3d,(img_3d.shape[0]*img_3d.shape[1],1,3))
	depth=R.dot(depth.transpose()[:,0,:])+T
	depth=depth/depth[2,:]
	depth=H_t.dot(depth).transpose()
	depth=np.reshape(depth,(img_3d.shape[0],img_3d.shape[1],3))

	for y in range(depth.shape[0]):
		for x in range(depth.shape[1]):
			th_c=depth[y][x][0]
			th_r=depth[y][x][1]
			out[y][x]=dst_t[int(th_r)][int(th_c)]
	
	thermal=out
	visual=rectified_left
	thermal_ratio=0.7
	img = cv2.addWeighted(thermal, thermal_ratio, visual, 1-thermal_ratio, 0)
	cv2.imwrite(working_Dir+'/thermal.jpg',img)

