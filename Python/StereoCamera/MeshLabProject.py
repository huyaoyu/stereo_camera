
from __future__ import print_function

import copy
import io
from xml.dom import minidom
import xml.etree.ElementTree as et

import numpy as np

class MLP_Mesh(object):
    def __init__(self, filename, label, matrix):
        self.filename = filename
        self.label = label

        if ( matrix is None ):
            self.mlMatrix = np.eye(4, 4, dtype = np.float32)
        else:
            self.mlMatrix = copy.deepcopy(matrix)

class MLP_Raster(object):
    def __init__(self, filename, label, focalMm, pixelSizeMm, viewportPx, translationVector):
        """
        pixelSizeMm - two-element list.
        viewportPx - two-element list.
        translationVector - four-element list or four-element numpy array.
        """
        self.filename = filename
        self.label    = label

        self.rotatingMatrix = np.eye(4, 4, dtype=np.float32)
        self.focalMm        = focalMm
        self.pixelSizeMm    = pixelSizeMm
        self.viewportPx     = viewportPx
        self.cameraType     = 0
        self.lensDistortion = [0, 0]
        self.translationVector = translationVector
        self.centerPx       = [ (int)(viewportPx[0]/2), (int)(viewportPx[1]/2) ]
        self.plane_semantic = 1

class MeshLabProjectFile(object):
    def __init__(self):
        self.meshGroup   = []
        self.rasterGroup = []

        self.ele_MLMesh          = "MLMesh"
        self.ele_MLMesh_filename = "filename"
        self.ele_MLMesh_label    = "label"

        self.ele_MLMatrix44 = "MLMatrix44"

        self.ele_MLRaster       = "MLRaster"
        self.ele_MLRaster_label = "label"

        self.ele_VCGCamera                   = "VCGCamera"
        self.ele_VCGCamera_RotationMatrix    = "RotationMatrix"
        self.ele_VCGCamera_FocalMm           = "FocalMm"
        self.ele_VCGCamera_PixelSizeMm       = "PixelSizeMm"
        self.ele_VCGCamera_ViewportPx        = "ViewportPx"
        self.ele_VCGCamera_CameraType        = "CameraType"
        self.ele_VCGCamera_LensDistortion    = "LensDistortion"
        self.ele_VCGCamera_TranslationVector = "TranslationVector"
        self.ele_VCGCamera_CenterPx          = "CenterPx"

        self.ele_Plane          = "Plane"
        self.ele_Plane_semantic = "semantic"
        self.ele_Plane_filename = "fileName"

        self.ele_MeshLabProject = "MeshLabProject"
        self.ele_MeshGroup      = "MeshGroup"
        self.ele_RasterGroup    = "RasterGroup"

        self.DOCTYPE = "MeshLabDocument"

    def add_mesh(self, mesh):
        self.meshGroup.append(mesh)

    def add_raster(self, raster):
        self.rasterGroup.append(raster)

    def convert_mesh(self, root, mesh):
        MLMesh = et.SubElement(root, self.ele_MLMesh)

        MLMesh.set(self.ele_MLMesh_filename, mesh.filename)
        MLMesh.set(self.ele_MLMesh_label, mesh.label)

        MLMatrix44 = et.SubElement(MLMesh, self.ele_MLMatrix44)
        sio = io.StringIO()
        np.savetxt(sio, mesh.mlMatrix, fmt="%f %f %f %f ")
        MLMatrix44.text = "\n%s" % (sio.getvalue())

        return MLMesh

    def get_matrix_string_one_line(self, m, f):
        """m is a list, numpy array, numpy vector, numpy matrix."""

        sio = io.StringIO()
        mr = m.reshape(1, -1)
        np.savetxt(sio, mr, fmt=f)

        return sio.getvalue()

    def convert_raster(self, root, raster):
        # MLRaster element.
        MLRaster = et.SubElement(root, self.ele_MLRaster)
        # Attributes for MLRaster.
        MLRaster.set(self.ele_MLRaster_label, raster.label)

        # VCGCamera element as the child element of MLRaster.
        VCGCamera = et.SubElement(MLRaster, self.ele_VCGCamera)

        # Attributes for VCGCamera.
        rotationMatrixString = self.get_matrix_string_one_line(raster.rotatingMatrix, "%f")
        VCGCamera.set(self.ele_VCGCamera_RotationMatrix, rotationMatrixString[:-1])

        VCGCamera.set(self.ele_VCGCamera_FocalMm, str(raster.focalMm))
        twoElementListString = "%f %f" % (raster.pixelSizeMm[0], raster.pixelSizeMm[1])
        VCGCamera.set(self.ele_VCGCamera_PixelSizeMm, twoElementListString)
        twoElementListString = "%d %d" % (raster.viewportPx[0], raster.viewportPx[1])
        VCGCamera.set(self.ele_VCGCamera_ViewportPx, twoElementListString)
        VCGCamera.set(self.ele_VCGCamera_CameraType, str(raster.cameraType))
        twoElementListString = "%f %f" % (raster.lensDistortion[0], raster.lensDistortion[1])
        VCGCamera.set(self.ele_VCGCamera_LensDistortion, twoElementListString)

        translationVectorString = self.get_matrix_string_one_line( raster.translationVector, "%f" )
        VCGCamera.set(self.ele_VCGCamera_TranslationVector, translationVectorString[:-1])

        twoElementListString = "%d %d" % (raster.centerPx[0], raster.centerPx[1])
        VCGCamera.set(self.ele_VCGCamera_CenterPx, twoElementListString)
        
        # Plane element as the child element of MLRaster.
        Plane = et.SubElement(MLRaster, self.ele_Plane)

        # Attributes for Plane.
        Plane.set(self.ele_Plane_semantic, str(raster.plane_semantic))
        Plane.set(self.ele_Plane_filename, raster.filename)

        return MLRaster

    def write_mlp(self, fn):
        """Write the mlp file."""

        # Open the file for output.
        with open(fn, "w") as fp:
            openingString = "<!DOCTYPE %s>" % (self.DOCTYPE)
            fp.write(openingString)

            MeshLabProject = et.Element(self.ele_MeshLabProject)

            MeshGroup = et.SubElement(MeshLabProject, self.ele_MeshGroup)

            for mesh in self.meshGroup:
                self.convert_mesh(MeshGroup, mesh)

            RasterGroup = et.SubElement(MeshLabProject, self.ele_RasterGroup)

            for raster in self.rasterGroup:
                self.convert_raster(RasterGroup, raster)
            
            xmlstr = minidom.parseString(et.tostring( MeshLabProject )).toprettyxml(indent="    ")

            fp.write( xmlstr[22:] )

            fp.close()
            