
ply_header = '''ply
format ascii 1.0
element vertex %(vert_num)d
property float x
property float y
property float z
property uchar red
property uchar green
property uchar blue
end_header
'''

ply_header_uv = '''ply
format ascii 1.0
comment VCGLIB generated
comment TextureFile %(texture_file)s
element vertex %(vert_num)d
property float x
property float y
property float z
property uchar red
property uchar green
property uchar blue
property float texture_u
property float texture_v
end_header
'''

ply_header_binary = '''ply
format binary 1.0
element vertex %(vert_num)d
property float x
property float y
property float z
property uchar red
property uchar green
property uchar blue
end_header
'''
