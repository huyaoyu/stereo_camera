#!/usr/bin/env python

'''
This file the reproduction of the project of HJCYFY's (https://github.com/HJCYFY/Vignetting-Correction).
The original code is written in C++.

This code performs vignetting correction on grayscale image.

'''

# Python 2/3 compatibility
from __future__ import print_function

import copy
import math
import os
import time

import numpy as np
import cv2

class VignettingCorrector(object):
    """Class VignettingCorrector"""

    def __init__(self, name):
        self.iterationLimit = 1.0 / 256

        self.name = name

        self.rows = 0
        self.cols = 0

        self.c_x = 0
        self.c_y = 0

        self.d = 0.0

        self.row = None
        self.col = None
        self.x   = None
        self.y   = None
        self.r   = None
        self.r2  = None
        self.r4  = None
        self.r6  = None

        self.onlyA = False

    def print_msg(self, msg):
        print("%s: %s" % (self.name, msg))

    def check(self, a, b, c):
        if ((a > 0) and (b == 0) and (c == 0)):
            return True

        if (a >=0 and b > 0 and c == 0):
            return True

        if (c == 0 and b < 0 and -a <= 2*b):
            return True

        if (c > 0 and b*b < 3*a*c):
            return True

        if (c > 0 and b*b == 3*a*c and b >= 0):
            return True

        if (c > 0 and b*b == 3*a*c and -b >= 3*c):
            return True

        if ( 0 == c ):
            return False

        q_p = ( -2 * b + math.sqrt( 4 * b * b - 12 * a * c ) ) / ( 6 * c )

        if (c > 0 and b*b > 3*a*c and q_p <= 0):
            return True

        q_d = ( -2 * b - math.sqrt( 4 * b * b - 12 * a * c ) ) / ( 6 * c )

        if (c > 0 and b*b > 3*a*c and q_d >= 1):
            return True

        if (c < 0 and b*b > 3*a*c and q_p >= 1 and q_d <= 0):
            return True
        
        return False

    def calH_Ori(self, a, b, c, GrayImg):
        GrayFloatImg = np.zeros(GrayImg.shape, dtype=np.float32)
        rows = GrayImg.shape[0]
        cols = GrayImg.shape[1]

        c_x = cols/2.0
        c_y = rows/2.0
        d = math.sqrt(c_x*c_x+c_y*c_y)

        for row in range(rows):
            for col in range(cols):
                r  = math.sqrt( (row-c_y) * (row-c_y) + (col-c_x) * (col-c_x) ) / d
                r2 = r*r
                r4 = r2*r2
                r6 = r2*r2*r2
                g  = 1+a*r2+b*r4+c*r6
                GrayFloatImg[row, col] = GrayImg[row, col] * g

        logImg = np.zeros( (rows, cols), dtype=np.float32 )

        for row in range(rows):
            for col in range(cols):
                logImg[row, col] = 255 * math.log( 1 + GrayFloatImg[row, col] ) / 8
        
        histogram = np.zeros((256), dtype=np.float32)
        
        for row in range(rows):
            for col in range(cols):
                value = logImg[row, col]
                k_d = (int)(math.floor( value ))
                k_u = (int)(math.ceil( value ))
                histogram[k_d] += ( 1 + k_d - value )
                histogram[k_u] += ( k_u - value )

        TempHist = np.zeros( (256 + 2 * 4), dtype=np.float32 ) # SmoothRadius = 4
        TempHist[0] = histogram[4]
        TempHist[1] = histogram[3]
        TempHist[2] = histogram[2]
        TempHist[3] = histogram[1]
        TempHist[260] = histogram[254]
        TempHist[261] = histogram[253]
        TempHist[262] = histogram[252]
        TempHist[263] = histogram[251]

        # memcpy(TempHist + 4, histogram, 256 * sizeof(float));

        TempHist[4:-4] = histogram

        # Smooth.
        for X in range(256):
            histogram[X] = (TempHist[X] + 2 * TempHist[X + 1] + 3 * TempHist[X + 2] + 4 * TempHist[X + 3] + 5 * TempHist[X + 4] + 4 * TempHist[X + 5] + 3 * TempHist[X + 6] + 2 * TempHist[X + 7]) + TempHist[X + 8] / 25.0

        sum = 0
        for i in range(256):
            sum += histogram[i]
        
        H  = 0
        pk = 0
        for i in range(256):
            pk = histogram[i] / sum

            if( pk != 0 ):
                H += pk * math.log(pk)
        
        return -H

    def calH(self, a, b, c, GrayImg):
        g  = 1.0 + a * self.r2 + b * self.r4 + c * self.r6

        GrayFloatImg = np.multiply(GrayImg, g)

        logImg = 255.0 * np.log( 1.0 + GrayFloatImg ) / 8.0
        
        histogram = np.zeros((256), dtype=np.float32)

        k_d = np.floor( logImg ).astype(np.int32).reshape( 1, logImg.size )
        k_u = np.ceil( logImg ).astype(np.int32).reshape( 1, logImg.size )
        logImgR = logImg.reshape( 1, logImg.size )

        histogram[ k_d ] += ( 1 + k_d - logImgR )
        histogram[ k_u ] += ( k_u - logImgR )

        TempHist = np.zeros( (256 + 2 * 4), dtype=np.float32 ) # SmoothRadius = 4
        TempHist[0] = histogram[4]
        TempHist[1] = histogram[3]
        TempHist[2] = histogram[2]
        TempHist[3] = histogram[1]
        TempHist[260] = histogram[254]
        TempHist[261] = histogram[253]
        TempHist[262] = histogram[252]
        TempHist[263] = histogram[251]

        TempHist[4:-4] = histogram

        # Smooth.
        for X in range(256):
            histogram[X] = (TempHist[X] + 2 * TempHist[X + 1] + 3 * TempHist[X + 2] + 4 * TempHist[X + 3] + 5 * TempHist[X + 4] + 4 * TempHist[X + 5] + 3 * TempHist[X + 6] + 2 * TempHist[X + 7]) + TempHist[X + 8] / 25.0

        sum = np.sum( histogram )
        histogram = histogram / sum

        idx = histogram != 0
        pk = histogram[idx]

        H = np.sum( np.multiply(pk, np.log(pk)) )
        
        return -H

    def prepare_for_calH(self, GrayImg):
        self.rows = GrayImg.shape[0]
        self.cols = GrayImg.shape[1]

        self.c_x = self.cols / 2.0
        self.c_y = self.rows / 2.0
        self.d   = math.sqrt( self.c_x * self.c_x + self.c_y * self.c_y )

        self.row = np.linspace(1, self.rows, self.rows)
        self.col = np.linspace(1, self.cols, self.cols)
        [self.x, self.y] = np.meshgrid( self.col, self.row)

        self.r  = np.sqrt( (self.x - self.c_x) * (self.x - self.c_x) + (self.y - self.c_y) * (self.y - self.c_y) ) / self.d
        self.r2 = np.power(self.r, 2)
        self.r4 = np.power(self.r2, 2)
        self.r6 = np.multiply(self.r4, self.r2)

    def correct(self, img):
        """img must be grayscale image."""

        self.prepare_for_calH(img)

        aa = copy.deepcopy(img)

        a = 0
        b = 0
        c = 0
        a_min = 0
        b_min = 0
        c_min = 0
        delta = 8
        Hmin = self.calH(a, b, c, img)

        while ( delta > self.iterationLimit ):
            self.print_msg("delta = %.6f, limit = %.6f" % (delta, self.iterationLimit))

            a_temp = a + delta
            if( self.check(a_temp, b, c) ):
                H = self.calH(a_temp, b, c, img)

                if(Hmin>H):
                    a_min = a_temp
                    b_min = b
                    c_min = c
                    Hmin  = H
            
            a_temp = a - delta
            if( self.check(a_temp, b, c) ):
                H = self.calH(a_temp, b, c, img)

                if( Hmin > H ):
                    a_min = a_temp
                    b_min = b
                    c_min = c
                    Hmin  = H
            
            if ( False == self.onlyA ):
                b_temp = b + delta
                if( self.check(a, b_temp, c) ):
                    H = self.calH(a, b_temp, c, img)

                    if( Hmin > H ):
                        a_min = a
                        b_min = b_temp
                        c_min = c
                        Hmin  = H
                
                b_temp = b - delta
                if( self.check(a, b_temp, c) ):
                    H = self.calH(a, b_temp, c, img)

                    if( Hmin > H ):
                        a_min = a
                        b_min = b_temp
                        c_min = c
                        Hmin  = H
                
                c_temp = c + delta
                if( self.check(a, b, c_temp) ):
                    H = self.calH(a, b, c_temp, img)

                    if( Hmin > H ):
                        a_min = a
                        b_min = b
                        c_min = c_temp
                        Hmin  = H
                
                c_temp = c - delta
                if( self.check(a, b, c_temp) ):
                    H = self.calH(a, b, c_temp, img)

                    if( Hmin > H ):
                        a_min = a
                        b_min = b
                        c_min = c_temp
                        Hmin  = H

            delta = delta / 2.0
        
        self.print_msg( "a_min = %f, b_min = %f, c_min = %f" % (a_min, b_min, c_min) )

        g = 1.0 + a_min * self.r2 + b_min * self.r4 + c_min * self.r6
        result = np.round( np.multiply(aa, g) )
        result[ result > 255 ] = 255
        result[ result < 0 ]   = 0
        
        return result.astype(np.uint8)

if __name__ == "__main__":
    imgOri = cv2.imread("VC_Data/input.bmp")
    imgGray = cv2.cvtColor(imgOri, cv2.COLOR_BGR2GRAY)

    cv2.imwrite("VC_Data/gray.bmp", imgGray)

    cvObject = VignettingCorrector("Test")
    cvObject.onlyA = True

    startTime = time.time()
    imgCorrected = cvObject.correct(imgGray)
    endTime = time.time()
    print("%ds elapsed." % (endTime - startTime))

    cv2.imwrite("VC_Data/corrected.bmp", imgCorrected)
