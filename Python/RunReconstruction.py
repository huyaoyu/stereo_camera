#!/usr/bin/env python

import math
import time
import Thermal_to_Visual
from Reconstruction import Reconstructor

# DR_PARAM = "/home/yaoyu/Documents/CMU/AirLab/Shimizu/Stereo/Calibration/calibrationdata_20180703_02/ToOpenCV"
# IMG_L = "/home/yaoyu/Documents/CMU/AirLab/Shimizu/Stereo/CapturedImages/Data_20180703/08/16_0.bmp"
# IMG_R = "/home/yaoyu/Documents/CMU/AirLab/Shimizu/Stereo/CapturedImages/Data_20180703/08/16_1.bmp"
# WORKING_DIR = "/home/yaoyu/Documents/CMU/AirLab/Shimizu/Stereo/CapturedImages/Data_20180703/08/Reconstruction"

# # DR_PARAM = "/home/yyhu/Documents/CMU/AirLab/Shimizu/Stereo/Calibration/calibrationdata_20180703_02/ToOpenCV"
# DR_PARAM    = "/home/yyhu/Documents/CMU/AirLab/Shimizu/Stereo/Calibration/calibrationdata_20180703_02/OpenCV"
# IMG_L       = "/home/yyhu/Documents/CMU/AirLab/Shimizu/Stereo/CapturedImages/Data_20180711/23/23_0.bmp"
# IMG_R       = "/home/yyhu/Documents/CMU/AirLab/Shimizu/Stereo/CapturedImages/Data_20180711/23/23_1.bmp"
# WORKING_DIR = "/home/yyhu/Documents/CMU/AirLab/Shimizu/Stereo/CapturedImages/Data_20180711/23/Reconstruction"

# DR_PARAM    = "/home/yyhu/Documents/CMU/AirLab/Shimizu/Stereo/Calibration/calibrationdata_20180718_01/OpenCV"
# IMG_L       = "/home/yyhu/Documents/CMU/AirLab/Shimizu/Stereo/CapturedImages/Data_20180718/bag/Left/frame0016.jpg"
# IMG_R       = "/home/yyhu/Documents/CMU/AirLab/Shimizu/Stereo/CapturedImages/Data_20180718/bag/Right/frame0016.jpg"
# WORKING_DIR = "/home/yyhu/Documents/CMU/AirLab/Shimizu/Stereo/CapturedImages/Data_20180718/bag/Reconstruction"

# DR_PARAM    = "/home/yyhu/Documents/CMU/AirLab/Shimizu/Stereo/Calibration/calibrationdata_20180703_02/OpenCV"
DR_PARAM    = "/home/yyhu/Documents/CMU/AirLab/Shimizu/Stereo/Calibration/calibrationdata_20180718_01/OpenCV"
IMG_L       = "/home/yyhu/Documents/CMU/AirLab/Shimizu/Stereo/CapturedImages/Data_20180718/07182018/ice/02/2_0.bmp"
IMG_R       = "/home/yyhu/Documents/CMU/AirLab/Shimizu/Stereo/CapturedImages/Data_20180718/07182018/ice/02/2_1.bmp"
WORKING_DIR = "/home/yyhu/Documents/CMU/AirLab/Shimizu/Stereo/CapturedImages/Data_20180718/07182018/ice/02/Reconstruction"

# DR_PARAM    = "/home/yyhu/Documents/CMU/AirLab/Shimizu/Stereo/Calibration/calibrationdata_20180718_01/OpenCV"
# IMG_L       = "/home/yyhu/Documents/CMU/AirLab/Shimizu/Stereo/CapturedImages/Data_20180718/07182018/1/10_0.bmp"
# IMG_R       = "/home/yyhu/Documents/CMU/AirLab/Shimizu/Stereo/CapturedImages/Data_20180718/07182018/1/10_1.bmp"
# WORKING_DIR = "/home/yyhu/Documents/CMU/AirLab/Shimizu/Stereo/CapturedImages/Data_20180718/07182018/1/Reconstruction"

DR_PARAM ='calibrationdata_20180703_02\\ToOpenCV'
IMG_L ='left\\2.bmp'
IMG_R = 'right\\2.bmp'
WORKING_DIR = 'Reconstruction'
THERMAL_IMG_PATH='2.jpg'


if __name__ == "__main__":
    rec = Reconstructor(DR_PARAM)

    rec.minDisp           = 500
    rec.numDisp           = 320
    rec.blockSize         = 15
    rec.disp12MaxDiff     = 10
    rec.uniquenessRatio   = 20
    rec.preFilterCap      = (int)(math.floor(63 * 0.5))

    rec.SGBM_P1           = 8  * rec.blockSize**2
    rec.SGBM_P2           = 32 * rec.blockSize**2
    
    # Speckle filter.
    rec.speckleWindowSize = 1000
    rec.speckleRange      = 2

    # WLS filter.
    rec.enableVignettingCorrector = True
    rec.enableVCOnlyA             = True

    rec.haveWLSFilter             = True

    # Camera sensor.
    rec.sensorPixelSize = [0.00346, 0.00346] # Actual size of the pixel on the camera sensor.

    rec.reload_params(1) # Reload with higher level of detail.
    startTime = time.time()
    # rec.reconstruct(WORKING_DIR, (IMG_L, IMG_R))
    img_3d,rectified_left,mask=rec.reconstruct_multithreading(WORKING_DIR, (IMG_L, IMG_R))
    endTime = time.time()
    print("%ds for reconstruction." % ( endTime - startTime ))
    
    print("Reconstruction done.")

    t1=time.time()
    thermal_img=Thermal_to_Visual.thermal_to_visual(DR_PARAM,THERMAL_IMG_PATH,IMG_L,img_3d,rectified_left,mask)
    t2=time.time()
    print("%ds for building thermal image." % (t2-t1))