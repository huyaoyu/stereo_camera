from __future__ import print_function

import numpy as np
import cv2
import glob
import os
from StereoCalibrator import StereoCalibrator

# G_GRID_ROW = 6
# G_GRID_COL = 9

# G_CHECKERBOARD_SIZE = 37 # mm.

# G_L_IMAGE_DIR = "../Data_20180501/left_dst_resized"
# # G_L_IMAGE_DIR = "../Data_20180501/left_small_resized"
# G_R_IMAGE_DIR = "../Data_20180501/right_dst"
# # G_R_IMAGE_DIR = "../Data_20180501/right_small"
# G_OUT_DIR = "./TempCalibrated_20180521"

# G_GRID_ROW = 6
# G_GRID_COL = 8

# G_CHECKERBOARD_SIZE = 107 # mm.

# G_L_IMAGE_DIR = "../../Zeng/Test/SeparatedImages_L"
# G_R_IMAGE_DIR = "../../Zeng/Test/SeparatedImages_R"
# G_OUT_DIR     = "../../Zeng/Test/CV_Calibrated"

G_GRID_ROW = 6
G_GRID_COL = 8

G_CHECKERBOARD_SIZE = 0.119 # m.

# G_BASE_DIR = "/home/theairlab/shimizu/demo/calibration"
# G_L_IMAGE_DIR = G_BASE_DIR + "/Left"
# G_R_IMAGE_DIR = G_BASE_DIR + "/Right"
# G_OUT_DIR     = G_BASE_DIR + "/OpenCV"
# G_INDIVIDUALLY_CALIBRATION = False

G_BASE_DIR = "/home/yyhu/expansion/Shimizu/20180919/StereoCalibration/shimizu_test_2018-09-19-14-15-46/Stereo"
G_L_IMAGE_DIR = G_BASE_DIR + "/LeftCalib"
G_R_IMAGE_DIR = G_BASE_DIR + "/RightCalib"
G_OUT_DIR     = G_BASE_DIR + "/OpenCV"
G_INDIVIDUALLY_CALIBRATION = False

if __name__ == "__main__":
    calib = StereoCalibrator(\
        (G_GRID_COL, G_GRID_ROW),\
         G_CHECKERBOARD_SIZE, G_L_IMAGE_DIR, G_R_IMAGE_DIR, G_OUT_DIR)

    calib.isDebugging = False
    calib.debuggingNImagePairs = 20

    calib.filenamePattern = "*.jpg"
    calib.isIndividuallyCalibrated = G_INDIVIDUALLY_CALIBRATION

    calib.calibrate()
    calib.show_calibration_results()
    calib.write_calibration_results(G_OUT_DIR)
