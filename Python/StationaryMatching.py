
from __future__ import print_function

import argparse
import copy
import cv2
import math
import numpy as np
from numpy import linalg as LA
import xml.etree.ElementTree as ET

# For test use.
np.set_printoptions(suppress = True, precision = 4)

import os

from Camera.CameraBase import CameraBase
from Camera.CameraBase import rvec_to_rotation_matrix
from Camera.CameraBase import FLIP_Y

from StereoCamera.MeshLabProject import MLP_Mesh, MLP_Raster, MeshLabProjectFile

DIR_BASE = "/home/yyhu/Documents/CMU/AirLab/Shimizu/BagFiles/20170726/Outside/FeatureMatching"

def update_MeshLab_project(MLP, cam, ele):
    """
    MLP: A MeshLabProjectFile object.
    cam: A CameraBase object.
    ele: A ElementTree object representing the <PC> element of the input xml file.
    
    After execution, MLP will be changed. New mesh and raster will be added to MLP.

    """

    meshMatrix = np.eye(4, dtype = np.float32)

    meshMatrix[0:3, 0:3] = cam.R2G
    meshMatrix[0, 3]     = cam.T2G[0]
    meshMatrix[1, 3]     = cam.T2G[1]
    meshMatrix[2, 3]     = cam.T2G[2]

    # Find the PLYFolder element.
    elePLYF = ele.find("PLYFolder")
    if ( elePLYF is None ):
        print("Could not find PLYFolder.")
        return -1
    
    # Find the PLY element.
    elePLY = elePLYF.find("PLY")
    if ( elePLY is None ):
        print("Could not find PLY.")
        return -1

    mesh = MLP_Mesh(\
        elePLYF.attrib["baseDir"] + "/" + elePLY.attrib["filename"],\
        elePLY.attrib["label"],\
        meshMatrix)

    # Find Q_PC element.
    eleQPC = elePLYF.find("Q_PC")
    if ( eleQPC is None ):
        print("Could not find Q_PC.")
        return -1
    
    # Retreive focal length.
    Q = np.loadtxt( elePLYF.attrib["baseDir"] + "/" + eleQPC.attrib["filename"], dtype = np.float)
    f = math.fabs( Q[2, 3] )

    focalLengthMm = math.fabs(f * cam.sensorPixelSize[0])

    # Find the Raster element.
    eleRT = elePLYF.find("Raster")
    if ( eleRT is None ):
        print("Could not find Raster.")
        return -1

    # Translation vector.
    invR2G = LA.inv(cam.R2G)
    translationVector = np.ones((1,4), dtype=np.float32)
    translationVector[0, 0:3] = -1.0 * invR2G.dot(cam.T2G).transpose()
    translationVector[0, 2]  += 1e-6

    raster = MLP_Raster(\
        elePLYF.attrib["baseDir"] + "/" + eleRT.attrib["filename"],\
        eleRT.attrib["label"],\
        focalLengthMm, cam.sensorPixelSize,\
        [cam.imageSize[1], cam.imageSize[0]],\
        translationVector)

    h, w = cam.imageSize
    
    raster.centerPx = [\
        (int)( w - math.fabs(Q[0,3]) ),\
        (int)( h - math.fabs(Q[1,3]) )]

    rotationMatrix = np.eye(4, dtype=np.float32)
    rotationMatrix[0:3, 0:3] = invR2G
    raster.rotatingMatrix    = rotationMatrix

    MLP.add_mesh(mesh)
    MLP.add_raster(raster)

    return 0

if __name__ == "__main__":

    parser = argparse.ArgumentParser(description='Feature matching with stereo camera.')

    parser.add_argument('--out_dir', help='Output path.', default=DIR_BASE + '/Output')
    parser.add_argument("--input", help="The filename of the input xml file.", default="ProjectFiles/FeatureMatching_20180729.xml")

    args = parser.parse_args()

    # Output directory.
    if ( os.path.isdir(args.out_dir) ):
        print("Found %s." % (args.out_dir))
    else:
        os.makedirs(args.out_dir)
        print("Create %s." % (args.out_dir))

    # Parse the input xml file.
    xmlTree = ET.parse(args.input)
    xmlRoot = xmlTree.getroot()

    PCCount = 0
    cam_1   = None
    cam_2   = None

    # # ========== Test use. ==========
    # cm = np.array([\
    #     [4700.0,    0.0, 2056.0],\
    #     [   0.0, 4700.0, 1504.0],\
    #     [   0.0,    0.0,    1.0]\
    # ], dtype = np.float)
    dc = np.array([0.0, 0.0, 0.0, 0.0, 0.0], dtype = np.float).reshape([1, 5])
    # cam_1.cameraMatrix = cm
    # cam_2.cameraMatrix = cm
    # cam_1.distortionCoefficients = dc
    # cam_2.distortionCoefficients = dc
    # # ========== End of test use. ==========

    imgPreprocessed = [np.zeros((1,1)), np.zeros((1,1))]

    minHessian = 600
    detector = cv2.xfeatures2d_SURF.create(hessianThreshold=minHessian)
    keypoints1, descriptors1 = None, None
    matcher = cv2.DescriptorMatcher_create(cv2.DescriptorMatcher_FLANNBASED)

    # MeshLab project file.
    MLP = MeshLabProjectFile()

    for PC in xmlRoot:
        print("PCCount = %d." % (PCCount))

        # Find the child element ReferenceImage.
        eleRI = PC.find("ReferenceImage")

        if (eleRI is None):
            print("Could not find ReferenceImage.")
            break
        
        imgFilename = eleRI.attrib["baseDir"] + "/" + eleRI.attrib["filename"]
        img_2 = cv2.imread( imgFilename, cv2.IMREAD_GRAYSCALE )
        imageSize = ( img_2.shape[1], img_2.shape[0] )

        if ( img_2 is None ):
            print("Could not open image %s." % (imgFilename))
            break
        
        cam_2 = CameraBase("Pos_2")

        # Find the child element ReferenceCamera.
        eleRC = PC.find("ReferenceCamera")
        if ( eleRC is None ):
            print("Could not find ReferenceCamera.")
            break
        
        # Find element CameraMatrix.
        eleCM = eleRC.find("CameraMatrix")
        if ( eleCM is None ):
            print("Could not find CameraMatrix")
            break

        # Find element DistortionCoefficient.
        eleDC = eleRC.find("DistortionCoefficient")
        if ( eleDC is None ):
            print("Could not find DistortionCoefficient")
            break

        # Find element ImageSize.
        eleIS = eleRC.find("ImageSize")
        if ( eleIS is None ):
            print("Could not find ImageSize.")
            break

        cam_2.load_parameters_from_file(\
            eleRC.attrib["calibDir"],\
            eleCM.attrib["filename"],\
            eleDC.attrib["filename"],\
            eleIS.attrib["filename"])

        cam_2.sensorPixelSize = (0.00346, 0.00346)

        # Find element R12 and P12.
        eleR12 = eleRC.find("R12")
        if ( eleR12 is None ):
            print("Could not find R12")
            break
        
        eleP12 = eleRC.find("P12")
        if ( eleP12 is None ):
            print("Could not find P12")
            break
        
        R12 = np.loadtxt( eleRC.attrib["calibDir"] + "/" + eleR12.attrib["filename"], dtype = np.float )
        P12 = np.loadtxt( eleRC.attrib["calibDir"] + "/" + eleP12.attrib["filename"], dtype = np.float )

        cam_2.get_remap(imageSize, R12, P12)

        imgPreprocessed[1] = cam_2.rectify_image(img_2)

        cam_2.cameraMatrix = P12[:, :3]
        cam_2.distortionCoefficients = dc

        # Detect the keypoints using SURF Detector, compute the descriptors.
        keypoints2, descriptors2 = detector.detectAndCompute(imgPreprocessed[1], None)
        print("Keypoints detected.")

        # ===

        if ( 0 != PCCount ):
            # Matching descriptor vectors with a FLANN based matcher.
            # Since SURF is a floating-point descriptor NORM_L2 is used
            knn_matches = matcher.knnMatch(descriptors1, descriptors2, 2)
            print("FLANN matching done.")

            #-- Filter matches using the Lowe's ratio test
            ratio_thresh = 0.3
            good_matches = []
            for m,n in knn_matches:
                if m.distance < ratio_thresh * n.distance:
                    good_matches.append(m)
                    # print("queryIdx = %d, trainIdx = %d, imgIdx = %d, distance = %f" % (m.queryIdx, m.trainIdx, m.imgIdx, m.distance))

            print("Matching result filtered. %d good matches." % (len(good_matches)))

            #-- Draw matches
            img_matches = np.empty((max(imgPreprocessed[0].shape[0], imgPreprocessed[1].shape[0]), imgPreprocessed[0].shape[1]+imgPreprocessed[1].shape[1], 3), dtype=np.uint8)
            debugMatchImage = np.zeros( (imgPreprocessed[0].shape[0], imgPreprocessed[0].shape[1]*2, 3), dtype = np.uint8 )

            cv2.drawMatches(imgPreprocessed[0], keypoints1, imgPreprocessed[1], keypoints2, good_matches, img_matches, flags=cv2.DrawMatchesFlags_NOT_DRAW_SINGLE_POINTS)

            # Get the points in place frist.
            goodPoints1 = []
            goodPoints2 = []

            for GM in good_matches:
                goodPoints1.append( [ keypoints1[ GM.queryIdx ].pt[0], keypoints1[ GM.queryIdx ].pt[1] ] )
                goodPoints2.append( [ keypoints2[ GM.trainIdx ].pt[0], keypoints2[ GM.trainIdx ].pt[1] ] )

            points1 = np.array(goodPoints1)
            points2 = np.array(goodPoints2)

            distanceOnImage = points2 - points1
            distanceOnImage = np.sqrt( distanceOnImage[:, 0] * distanceOnImage[:, 0] + distanceOnImage[:, 1] * distanceOnImage[:, 1] )
            print("distanceOnImage: min = %f, max = %f, mean = %f." % (distanceOnImage.min(), distanceOnImage.max(), distanceOnImage.mean()))

            #-- Show detected matches
            cv2.namedWindow("GoodMatches", cv2.WINDOW_NORMAL)
            cv2.imshow("GoodMatches", img_matches)
            jpgParams = [ cv2.IMWRITE_JPEG_QUALITY, 100 ]
            goodMatchsImageFilename = "%s/GoodMatches_%04d.jpg" % (args.out_dir, PCCount + 1)
            cv2.imwrite(goodMatchsImageFilename, img_matches, jpgParams )

            # debugMatchImage[:, :imgPreprocessed[0].shape[1], :] = cv2.cvtColor( imgPreprocessed[0], cv2.COLOR_GRAY2BGR )
            # debugMatchImage[:, imgPreprocessed[0].shape[1]:, :] = cv2.cvtColor( imgPreprocessed[1], cv2.COLOR_GRAY2BGR )

            # cv2.namedWindow("DebugMatch", cv2.WINDOW_NORMAL)
            # for i in range( points1.shape[0] ):
            #     p1 = ((int)(points1[i, 0]), (int)(points1[i, 1]))
            #     p2 = (imgPreprocessed[0].shape[1] + (int)(points2[i, 0]), (int)(points2[i, 1]))
            #     cv2.circle(debugMatchImage, p1, 10, (255, 0, 0), thickness = 10)
            #     cv2.putText(debugMatchImage, "%d" % (i), p1, cv2.FONT_HERSHEY_SIMPLEX, 5.0, (255, 0, 0), 5)
            #     cv2.circle(debugMatchImage, p2, 10, (0, 0, 255), thickness = 10)
            #     cv2.putText(debugMatchImage, "%d" % (i), p2, cv2.FONT_HERSHEY_SIMPLEX, 5.0, (0, 0, 255), 5)
            #     cv2.line(debugMatchImage, p1, p2, (0, 255, 0), thickness = 7)
            #     cv2.imshow("DebugMatch", debugMatchImage)
            #     # cv2.waitKey()
            # cv2.imwrite(args.out_dir + "/DebugMatcheImage.jpg", debugMatchImage, jpgParams)

            #-- Find the 3D coordinates for the good points in cam_2 by using the stereo disparity map.

            # Find the xml element PLYFolder.
            elePLYF = PC.find("PLYFolder")
            if ( elePLYF is None ):
                print("Could not find PLYFolder")
                break
            
            # Find the xml element DisparityMap.
            eleDM = elePLYF.find("DisparityMap")
            if ( eleDM is None ):
                print("Could not find DisparityMap")
                break

            # Load the disparity map generated by cam_2.
            disp = np.load( elePLYF.attrib["baseDir"] + "/" + eleDM.attrib["filename"] )
            print("Disparity map loaded.")

            # Find the xml element Q_PC.
            eleQPC = elePLYF.find("Q_PC")
            if ( eleQPC is None ):
                print("Could not find Q_PC")
                break

            # Load the Q_PointCloud matrix.
            Q_PC = np.loadtxt(elePLYF.attrib["baseDir"] + "/" + eleQPC.attrib["filename"], dtype = np.float)
            print("Q_PC loaded: ")
            print(Q_PC)

            # Compute the reprojected point cloud.
            worldPoints = cv2.reprojectImageTo3D(disp, Q_PC)
            maskDisp    = disp > disp.min()

            goodFeaturePoints_world = []
            points1_Camera          = []
            for i in range( len(points2) ):
                p = points2[i, :].astype(np.int32)

                if ( True == maskDisp[ p[1], p[0] ] ):
                    goodFeaturePoints_world.append( worldPoints[ p[1], p[0], : ])
                    points1_Camera.append( points1[i] )
            
            goodFeaturePoints_world = np.array(goodFeaturePoints_world)
            points1_Camera          = np.array(points1_Camera)

            # print("goodFeaturePoints_world = ")
            # print(goodFeaturePoints_world)

            # print("points_Camera = ")
            # print(points1_Camera)

            print("Done with finding the world points and the camera points.")

            # -- SolvePnPRansac.
            inliers = []

            rvec, tvec, inliers = cam_1.solve_PnP_RANSAC(goodFeaturePoints_world, points1_Camera, None, None)
            cam_2.from_rvec_tvec_to_R2G_T2G(rvec, tvec, cam_1.R2G, cam_1.T2G)

            print("cam_2.R2G = ")
            print(cam_2.R2G)

            rvec_cam_2_R2G, _ = cv2.Rodrigues(cam_2.R2G)
            print("rvec_cam_2_R2G")
            print(rvec_cam_2_R2G)

            theta = LA.norm(rvec_cam_2_R2G, 2)
            print("theta in degree is %f." % (theta / np.pi * 180))

            print("cam_2.T2G = ")
            print(cam_2.T2G)

            cv2.waitKey(2000)
            cv2.destroyAllWindows()

        cam_1 = cam_2
        img_1 = img_2
        imgPreprocessed[0] = imgPreprocessed[1]
        cam_1.name = "Pos_1"

        keypoints1, descriptors1 = keypoints2, descriptors2

        # Update MeshLap project file.
        update_MeshLab_project(MLP, cam_1, PC)

        print("Done with PCCount = %d.\n" % (PCCount))

        PCCount += 1

    # Write the MeshLap project file.
    MLP.write_mlp( args.out_dir + "/mlp_aligned.mlp" )
    
    print("Done with all point cloud.")
