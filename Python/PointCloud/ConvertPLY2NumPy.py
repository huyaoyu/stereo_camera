#!/usr/bin/python

from __future__ import print_function

import argparse
import numpy as np
import os

if __name__ == "__main__":
    parser = argparse.ArgumentParser( description = "Reads a PLY file and converts it into NumPy arrays." )
    parser.add_argument( "inFile", nargs = "?", help="The input file name." )
    parser.add_argument( "outDir", nargs = "?", help="The output directory." )
    parser.add_argument( "--binary", help = "If set, the binary format will also be written to the file system.", action = "store_true", default = False)

    args = parser.parse_args()

    # Open and read the content of the input file.
    print("Read {}...".format(args.inFile))
    with open(args.inFile) as fp:
        content = fp.readlines()

    print(content[2])

    content = content[10:]

    # Convert the list of strings into NumPy array.
    print("Coverting into raw data ...")
    npRaw = np.loadtxt(content, dtype = np.float)

    npCD = npRaw[:, 0:3] # The point coordinate.
    npCL = npRaw[:, 3:6].astype(np.uint8) # The point color.

    # Save the arrays.
    if ( not os.path.isdir(args.outDir) ):
        os.makedirs(args.outDir)
    
    # Split the filename.
    fnBase, ext = os.path.splitext(os.path.basename(args.inFile))

    print("Saving data ...")
    np.savetxt(args.outDir + "/" + fnBase + "_CD.dat", npCD, fmt = "%+6e")
    np.savetxt(args.outDir + "/" + fnBase + "_CL.dat", npCL, fmt = "%3d")

    if ( args.binary ):
        print("Saving binary ...")
        np.save(args.outDir + "/" + fnBase + "_CD.npy", npCD)
        np.save(args.outDir + "/" + fnBase + "_CL.npy", npCL)

    print("Done!")
