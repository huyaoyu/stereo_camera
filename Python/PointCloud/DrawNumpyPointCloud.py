#!/usr/bin/python

from __future__ import print_function

import argparse
import copy
import cv2
import json
import numpy as np
import os

FRONT_Z_ROT = np.eye(3, dtype = np.float)
FRONT_Z_ROT[1, 1] = -1.0
FRONT_Z_ROT[2, 2] = -1.0

if __name__ == "__main__":
    parser = argparse.ArgumentParser( description = "Read the coordinate file and the color file, sample, and draw a 2D image." )
    parser.add_argument( "inFile", nargs = "?", help="The input JSON file." )
    parser.add_argument( "--sf",\
        help = "Overwite the sampleFactor parameter in the JSON file.",\
        default = -1.0, type = float)
    parser.add_argument("--right", \
        help = "Turn of right image mode.", \
        action = "store_true", default = False)

    args = parser.parse_args()

    # Open and read the content of the input files.
    fpJSON = open(args.inFile, "r")
    if (not fpJSON):
        print("Could not open {}.".format(args.inFile))
        raise( SystemExit() )

    params = json.load(fpJSON)

    # Read the input data.
    fnBase, ext = os.path.splitext( os.path.basename(params["CD"]) )
    if ( ext == ".npy" ):
        CD = np.load( params["dataDir"] + "/" + params["CD"] )
    else:
        CD = np.loadtxt( params["dataDir"] + "/" + params["CD"], dtype = np.float)

    print("CD.shape = {}".format(CD.shape))

    fnBase, ext = os.path.splitext( os.path.basename(params["CL"]) )
    if ( ext == ".npy" ):
        CL = np.load( params["dataDir"] + "/" + params["CL"] )
    else:
        CL = np.loadtxt( params["dataDir"] + "/" + params["CL"], dtype = np.uint8)

    print("CL.shape = {}".format(CL.shape))

    CM = np.loadtxt( params["calibDir"] + "/" + params["CM"], dtype = np.float )
    CM = CM[0:3, 0:3]

    if ( True == args.right ):
        R = np.loadtxt( params["calibDir"] + "/" + params["Q"], dtype = np.float )
        B = 1.0 / R[3, 2]
        F = CM[0, 0]

    H = params["H"]
    W = params["W"]

    print("H = %d, W = %d, HxW = %d" % (H, W, H * W))

    if ( True == args.right ):
        suffix = "_R"
    else:
        suffix = ""

    # Project all the coordinates to the image plane.
    coorImageFrame  = FRONT_Z_ROT.dot( CM.dot( CD.transpose() ) )
    coorImage       = copy.deepcopy( coorImageFrame )
    coorImage[0, :] = coorImageFrame[0, :] / coorImageFrame[2, :]
    coorImage[1, :] = coorImageFrame[1, :] / coorImageFrame[2, :]

    if ( True == args.right ):
        d = B * F / coorImageFrame[2, :]
        coorImage[0, :] -= d

    coorImage = coorImage[0:2, :]

    # Compose an image.
    image = np.zeros( (H, W, 3), dtype = np.uint8 )
    image[ coorImage[1, :].astype(np.int), coorImage[0, :].astype(np.int), : ] = CL
    image = cv2.cvtColor(image, cv2.COLOR_RGB2BGR)

    # Output directory.
    if ( not os.path.isdir(params["outDir"]) ):
        os.makedirs( params["outDir"] )
    
    cv2.imwrite( params["outDir"] + "/" + fnBase + "_Full" + suffix + ".jpg", image, [cv2.IMWRITE_JPEG_QUALITY, 100] )

    cv2.namedWindow("Reconstructed image (full)", cv2.WINDOW_NORMAL)
    cv2.imshow("Reconstructed image (full)", image)

    # Compose an distance map.
    distMap = np.zeros( (H, W), dtype = np.float )
    distMap[ coorImage[1, :].astype(np.int), coorImage[0, :].astype(np.int) ] = coorImageFrame[2, :]

    np.save( params["outDir"] + "/" + fnBase + "_Distance" + suffix + ".npy", distMap )

    cv2.namedWindow("Distance map", cv2.WINDOW_NORMAL)
    cv2.imshow("Distance map", distMap / np.max(distMap))

    # Down sample.
    if ( args.sf > 0.0 and args.sf <= 1.0 ):
        sf = args.sf
    else:
        sf = params["sampleFactor"]

    uArray = np.linspace(0, W-1, (int)((W-1) * sf)).astype(np.int)
    vArray = np.linspace(0, H-1, (int)((H-1) * sf)).astype(np.int)

    u, v = np.meshgrid(uArray, vArray)

    # Down sampled image.
    imageDS = np.zeros_like(image, dtype = np.uint8)
    imageDS[ v, u, :] = image[ v, u, : ]

    cv2.namedWindow("Down sampled image", cv2.WINDOW_NORMAL)
    cv2.imshow("Down sampled image", imageDS)
    cv2.imwrite( params["outDir"] + "/" + fnBase + "_DownSample" + suffix + ".jpg", imageDS, [cv2.IMWRITE_JPEG_QUALITY, 100] )

    # Down sampled distance map.
    distMapDS = np.zeros_like(distMap, dtype = np.float)
    distMapDS[v, u] = distMap[v, u]

    np.save( params["outDir"] + "/" + fnBase + "_Distance_DownSample" + suffix + ".npy", distMapDS )

    # Statistics of dispMapDS.
    mask = distMapDS > 0
    maskedNum = distMapDS[mask].size
    print("distMapDS.shape = {}, masked = {}, masked ratio = {}".format(distMapDS.shape, maskedNum, 1.0 * maskedNum / distMapDS.size))

    # Wait interaction.
    cv2.waitKey()

    cv2.destroyAllWindows()

    print("Done!")
