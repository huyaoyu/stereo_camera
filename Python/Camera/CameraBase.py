from __future__ import print_function

import copy
import json
import os

import cv2
import numpy as np
from numpy import linalg as LA

FLIP_Y = np.array([[ 1.0,  0.0,  0.0],\
                   [ 0.0, -1.0,  0.0],\
                   [ 0.0,  0.0, -1.0]], dtype = np.float)

def rvec_to_rotation_matrix(rvec, flip = None):
    # Get theta.
    theta = LA.norm(rvec, ord = 2)
    k     = 1.0 * rvec / theta

    K = np.array(\
        [[    0.0, -k[2,0],  k[1,0]],\
         [ k[2,0],     0.0, -k[0,0]],\
         [-k[1,0],  k[0,0],    0.0]])

    R = np.eye(3) + np.sin(theta) * K + (1 - np.cos(theta)) * np.matmul(K, K)

    if ( flip is not None ):
        R = np.matmul(flip, R)

    return R

def transform_from_world_to_camera_frame(v, rvec, tvec):
    """
    v: a 3x1 vector, numpy array, original vector.
    rvec: a 3x1 vector, numpy array, rotation vector.
    tvec: a 3x1 vector, numpy array, translation vector.
    return vc: a 3x1 vector, numpy array, the same point in the camera frame.

    coordinate_in_camera_frame = R * coordinate_in_world_frame + t

    R is the rotation matrix.
    t is the translation vector.
    """

    R = rvec_to_rotation_matrix(rvec)

    vc = R.dot(v) + tvec

    return vc

class CameraBase(object):
    def __init__(self, name):
        self.name = name

        self.cameraMatrix           = []
        self.distortionCoefficients = []
        self.imageSize              = (1 ,1) # Two-element tuple, height, width.
        self.sensorPixelSize        = (1, 1)

        self.rmap = None

        # It is assumed that the axes of the local reference frame are NOT in the same direction
        # with the image frame of the camera. The local reference frame will have its y-axis
        # pointing upwards and its z-axis pointing towards the reader.
        # x_global = R2G * x_local + T2G
        # x_global and x_local are all 3-element vectors for 3D points.
        self.T2G = np.zeros((3, 1)) # Translation vector from the local to the global reference frame.
        self.R2G = np.eye(3)        # Rotation matrix with respect to the global reference frame.

    def load_parameters_from_file(self, calibDir, fnCameraMatrix, fnDistCoeff, fnImageSize):
        """
        calibDir: The directory where the calibration files locate.
        fnCameraMatrix: The filename of the camera matrix.
        fnDistCoeff: The filename of the distortion coefficient.
        fnImageSize: The JSON file of the image size.
        """

        self.cameraMatrix = np.loadtxt( calibDir + "/" + fnCameraMatrix )
        self.distortionCoefficients = np.loadtxt( calibDir + "/" + fnDistCoeff )

        fp = open( calibDir + "/" + fnImageSize, "r" )
        if ( fp is None ):
            print("%s could not be opened." % (fnImageSize))
            return

        jImageSize = json.load(fp)

        self.imageSize = ( jImageSize["height"], jImageSize["width"] )

    def get_remap(self, imageSize, R12 = None, P12 = None):
        """
        imageSize - Two-element tuple, width and height.
        """
        
        self.rmap = []

        if ( R12 is not None and P12 is not None ):
            map1, map2 = cv2.initUndistortRectifyMap(\
                self.cameraMatrix, self.distortionCoefficients,\
                R12, P12,\
                imageSize, cv2.CV_16SC2)
        else:
            map1, map2 = cv2.initUndistortRectifyMap(\
                self.cameraMatrix, self.distortionCoefficients,\
                None, self.cameraMatrix,\
                imageSize, cv2.CV_16SC2)

        self.rmap = [map1, map2]

    def rectify_image(self, img):
        rimg = cv2.remap( img, self.rmap[0], self.rmap[1], cv2.INTER_LINEAR )
        
        return rimg

    def solve_PnP_RANSAC(self, objectPoints, imagePoints, rvec, tvec, useExtrinsicGuess = False,\
        iterationsCount = 10000, reprojectionError = 0.1, confidence = 0.999, flags = cv2.SOLVEPNP_ITERATIVE):
            
        ret, rvec, tvec, inliers =\
        cv2.solvePnPRansac( objectPoints, imagePoints, self.cameraMatrix, self.distortionCoefficients,\
            useExtrinsicGuess = useExtrinsicGuess,\
            iterationsCount = iterationsCount, reprojectionError = reprojectionError, confidence = confidence,\
            flags = flags )

        # print("ret is ", end = "")
        # print(ret)
        
        return rvec, tvec, inliers
        
    def from_rvec_tvec_to_R2G_T2G(self, rvec, tvec, RBase, TBase):

        R_2_1 = rvec_to_rotation_matrix(rvec, FLIP_Y)
        R_2_0 = np.matmul(RBase, R_2_1)

        T_2_0 = RBase.dot(FLIP_Y.dot(tvec)) + TBase

        self.R2G = R_2_0
        self.T2G = T_2_0

if __name__ == "__main__":
    # Create Camera 1.
    cam_1 = CameraBase("cam_1")
    # cam_1.load_parameters_from_file(\
    #     "/home/yyhu/Documents/CMU/AirLab/Shimizu/Stereo/Calibration/calibrationdata_20180726_01/OpenCV",\
    #     "CameraMatrixLeft.dat",\
    #     "DistortionCoefficientLeft.dat")

    cam_1.cameraMatrix = np.array(\
    [[300,   0, 300],\
     [  0, 300, 300],\
     [  0,   0,   1]], dtype = np.float)

    cam_1.distortionCoefficients = np.zeros((1, 5), dtype = np.float)

    cam_2 = copy.deepcopy(cam_1)

    # Dummy points.
    # Points in cam_2 as the world points.
    pointsInCam_2 = np.zeros((8, 3, 1))
    pointsInCam_2[0, :, 0] = [ 1,  0,  0]
    pointsInCam_2[1, :, 0] = [ 0,  1,  0]
    pointsInCam_2[2, :, 0] = [-1,  0,  0]
    pointsInCam_2[3, :, 0] = [ 0, -1,  0]
    pointsInCam_2[4, :, 0] = [ 1,  0, -1]
    pointsInCam_2[5, :, 0] = [ 0, -1, -1]
    pointsInCam_2[6, :, 0] = [-1,  0, -1]
    pointsInCam_2[7, :, 0] = [ 0, -1, -1]
    
    # Points in the image plane of cam_1.
    oneOverTwo   = 1.0 / 2 * 300
    oneOverThree = 1.0 / 3 * 300
    imagePointsInCam_1 = np.zeros((8, 2), dtype = np.float)
    imagePointsInCam_1[0, :] = [ oneOverTwo + 300,             300 - 0 ]
    imagePointsInCam_1[1, :] = [          0 + 300,    300 - oneOverTwo ]
    imagePointsInCam_1[2, :] = [-oneOverTwo + 300,            300 -  0 ]
    imagePointsInCam_1[3, :] = [          0 + 300, 300 - (-oneOverTwo) ]
    imagePointsInCam_1[4, :] = [ oneOverThree + 300,              300 -  0 ]
    imagePointsInCam_1[5, :] = [            0 + 300,   300 -  oneOverThree ]
    imagePointsInCam_1[6, :] = [-oneOverThree + 300,              300 -  0 ]
    imagePointsInCam_1[7, :] = [            0 + 300, 300 - (-oneOverThree) ]

    rvec = np.zeros((3, 1), dtype = np.float)
    tvec = np.array([[0], [0], [2]], dtype = np.float)

    inliers = []

    rvec, tvec, inliers = cam_1.solve_PnP_RANSAC(pointsInCam_2, imagePointsInCam_1, None, None)

    # Test point 01.
    tpWorld = np.array([[1], [0], [0]])
    print("The test point's world coordinate is")
    print(tpWorld)

    tpCamera = transform_from_world_to_camera_frame(tpWorld, rvec, tvec)
    print("The test point's camera coordiante is")
    print(tpCamera)

    tpCameraFlipped = FLIP_Y.dot(tpCamera)
    print("The test point's coordinate in the flipped camera frame is")
    print(tpCameraFlipped)

    # Test point 02.
    tpWorld = np.array([[0], [1], [0]])
    print("The test point's world coordinate is")
    print(tpWorld)

    tpCamera = transform_from_world_to_camera_frame(tpWorld, rvec, tvec)
    print("The test point's camera coordiante is")
    print(tpCamera)

    tpCameraFlipped = FLIP_Y.dot(tpCamera)
    print("The test point's coordinate in the flipped camera frame is")
    print(tpCameraFlipped)

    cam_1.R2G = np.eye(3, dtype = np.float)
    cam_1.T2G = np.zeros((3,1), dtype = np.float)

    cam_2.from_rvec_tvec_to_R2G_T2G(rvec, tvec, cam_1.R2G, cam_1.T2G)
    print("cam_2.R2G = ")
    print(cam_2.R2G)
    print("cam_2.T2G = ")
    print(cam_2.T2G)
