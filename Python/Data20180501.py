from __future__ import print_function

import FileListBase
import numpy as np

BASE_DIR = "/home/yaoyu/Documents/CMU/AirLab/Weikun/Data_20180501/left"
DEST_DIR = "/home/yaoyu/Documents/CMU/AirLab/Weikun/Data_20180501/left_dst"

PREFIX = "left"
SURFIX = ".jpg"

if __name__ == "__main__":

    fileListAuto = []

    seqStart = 0
    seqEnd   = 886

    idx = np.rint( np.linspace(seqStart, seqEnd, 100) )

    for i in np.nditer(idx):
        ii = int(i)

        fileName = "%s%04d%s" % (PREFIX, ii, SURFIX)
        print(fileName)

        fileListAuto.append(fileName)

    Data20180501_left = FileListBase.FileList(BASE_DIR, fileListAuto)
    Data20180501_left.copy(DEST_DIR)


