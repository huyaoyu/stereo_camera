
from __future__ import print_function

import argparse
import cv2
import json
import math
import numpy as np
from numpy import linalg as LA
import xml.etree.ElementTree as ET

# For test use.
np.set_printoptions(suppress = True, precision = 4)

import os

from Camera.CameraBase import CameraBase
from Camera.CameraBase import rvec_to_rotation_matrix
from Camera.CameraBase import FLIP_Y

from StereoCamera.MeshLabProject import MLP_Mesh, MLP_Raster, MeshLabProjectFile

DIR_BASE = "."

def update_MeshLab_project(MLP, cam, ele, R0, R1):
    """
    MLP: A MeshLabProjectFile object.
    cam: A CameraBase object.
    ele: A ElementTree object representing the <PC> element of the input xml file.
    
    After execution, MLP will be changed. New mesh and raster will be added to MLP.

    """

    meshMatrix = np.eye(4, dtype = np.float32)

    meshMatrix[0:3, 0:3] = np.matmul( cam.R2G, R0 )
    meshMatrix[0, 3]     = cam.T2G[0]
    meshMatrix[1, 3]     = cam.T2G[1]
    meshMatrix[2, 3]     = cam.T2G[2]

    # Find the PLYFolder element.
    elePLYF = ele.find("PLYFolder")
    if ( elePLYF is None ):
        print("Could not find PLYFolder.")
        return -1
    
    # Find the PLY element.
    elePLY = elePLYF.find("PLY")
    if ( elePLY is None ):
        print("Could not find PLY.")
        return -1

    mesh = MLP_Mesh(\
        elePLYF.attrib["baseDir"] + "/" + elePLY.attrib["filename"],\
        elePLY.attrib["label"],\
        meshMatrix)

    # Find Q_PC element.
    eleQPC = elePLYF.find("Q_PC")
    if ( eleQPC is None ):
        print("Could not find Q_PC.")
        return -1
    
    # Retreive focal length.
    Q = np.loadtxt( elePLYF.attrib["baseDir"] + "/" + eleQPC.attrib["filename"], dtype = np.float)
    f = math.fabs( Q[2, 3] )

    focalLengthMm = math.fabs(f * cam.sensorPixelSize[0])

    # Find the Raster element.
    eleRT = elePLYF.find("Raster")
    if ( eleRT is None ):
        print("Could not find Raster.")
        return -1

    # Translation vector.
    invR2G = np.matmul( cam.R2G, R1 ).transpose()
    translationVector = np.ones((1,4), dtype=np.float32)
    translationVector[0, 0:3] = -1.0 * invR2G.dot(cam.T2G).transpose()
    translationVector[0, 2]  += 1e-6

    raster = MLP_Raster(\
        elePLYF.attrib["baseDir"] + "/" + eleRT.attrib["filename"],\
        eleRT.attrib["label"],\
        focalLengthMm, cam.sensorPixelSize,\
        [cam.imageSize[1], cam.imageSize[0]],\
        translationVector)

    h, w = cam.imageSize
    
    raster.centerPx = [\
        (int)( w - math.fabs(Q[0,3]) + 0.5 ),\
        (int)( h - math.fabs(Q[1,3]) + 0.5 )]

    rotationMatrix = np.eye(4, dtype=np.float32)
    rotationMatrix[0:3, 0:3] = invR2G
    raster.rotatingMatrix    = rotationMatrix

    MLP.add_mesh(mesh)
    MLP.add_raster(raster)

    return 0

if __name__ == "__main__":

    parser = argparse.ArgumentParser(description='Feature matching with stereo camera.')

    parser.add_argument('--out_dir', help='Output path.', default=DIR_BASE + '/MatchingOutput')
    parser.add_argument("--pc_file", help="The filename of the input xml file.", default="ProjectFiles/20180919/MatchedPointClouds.xml")
    parser.add_argument("--pose_file", help="The filename of the input pose json file.", default="ProjectFiles/20180919/MatchedCameraPoses.json")
    parser.add_argument("--flip", help="Set this if the y-axis of the coordinate system in pose_file is pointing downward.", action = "store_true", default = False)

    args = parser.parse_args()

    # Output directory.
    if ( os.path.isdir(args.out_dir) ):
        print("Found %s." % (args.out_dir))
    else:
        os.makedirs(args.out_dir)
        print("Create %s." % (args.out_dir))

    # Flip.
    R0 = np.eye(3, dtype = np.float)
    if ( True == args.flip ):
        R0[1, 1] = -1.0
        R0[2, 2] = -1.0

    # R1 = np.eye(3, dtype = np.float)
    # R1[0, 0] = -1.0
    # R1[2, 2] = -1.0

    # Parse the input pose json file.
    fp = open(args.pose_file, "r")
    poses = json.load(fp)
    fp.close()
    print("%s loaded." % (args.pose_file))

    # Parse the input xml file.
    xmlTree = ET.parse(args.pc_file)
    xmlRoot = xmlTree.getroot()

    PCCount = 0
    cam   = None

    # # ========== Test use. ==========
    # cm = np.array([\
    #     [4700.0,    0.0, 2056.0],\
    #     [   0.0, 4700.0, 1504.0],\
    #     [   0.0,    0.0,    1.0]\
    # ], dtype = np.float)
    dc = np.array([0.0, 0.0, 0.0, 0.0, 0.0], dtype = np.float).reshape([1, 5])
    # cam.cameraMatrix = cm
    # cam.distortionCoefficients = dc
    # # ========== End of test use. ==========

    # MeshLab project file.
    MLP = MeshLabProjectFile()

    for PC in xmlRoot:
        print("PCCount = %d." % (PCCount))

        # Find the child element ReferenceImage.
        # eleRI = PC.find("ReferenceImage")

        # if (eleRI is None):
        #     print("Could not find ReferenceImage.")
        #     break
        
        # imageSize = ( img.shape[1], img.shape[0] )

        cam = CameraBase("Pos_%02d" % PCCount)

        # Find the child element ReferenceCamera.
        eleRC = PC.find("ReferenceCamera")
        if ( eleRC is None ):
            print("Could not find ReferenceCamera.")
            break
        
        # Find element CameraMatrix.
        eleCM = eleRC.find("CameraMatrix")
        if ( eleCM is None ):
            print("Could not find CameraMatrix")
            break

        # Find element DistortionCoefficient.
        eleDC = eleRC.find("DistortionCoefficient")
        if ( eleDC is None ):
            print("Could not find DistortionCoefficient")
            break

        # Find element ImageSize.
        eleIS = eleRC.find("ImageSize")
        if ( eleIS is None ):
            print("Could not find ImageSize.")
            break

        cam.load_parameters_from_file(\
            eleRC.attrib["calibDir"],\
            eleCM.attrib["filename"],\
            eleDC.attrib["filename"],\
            eleIS.attrib["filename"])

        cam.sensorPixelSize = (0.00346, 0.00346)

        # Find element R12 and P12.
        eleR12 = eleRC.find("R12")
        if ( eleR12 is None ):
            print("Could not find R12")
            break
        
        eleP12 = eleRC.find("P12")
        if ( eleP12 is None ):
            print("Could not find P12")
            break
        
        R12 = np.loadtxt( eleRC.attrib["calibDir"] + "/" + eleR12.attrib["filename"], dtype = np.float )
        P12 = np.loadtxt( eleRC.attrib["calibDir"] + "/" + eleP12.attrib["filename"], dtype = np.float )

        cam.get_remap(cam.imageSize, R12, P12)

        cam.cameraMatrix = P12[:, :3]
        cam.distortionCoefficients = dc

        # Assign R2G and T2G.
        transArray = np.array( poses["data"][PCCount]["transform"], dtype = np.float ).reshape( (3, 4) )
        cam.R2G = np.matmul( R0, transArray[:, 0:3] )
        cam.T2G = R0.dot( transArray[:, 3] )

        # Update MeshLap project file.
        update_MeshLab_project(MLP, cam, PC, R0, R0)

        print("Done with PCCount = %d.\n" % (PCCount))

        PCCount += 1

        # if ( PCCount == 2 ):
        #     break

    # Write the MeshLap project file.
    MLP.write_mlp( args.out_dir + "/mlp_aligned.mlp" )
    
    print("Done with all point cloud.")
