from __future__ import print_function

import numpy as np
import cv2
import glob
import os

G_GRID_ROW = 6
G_GRID_COL = 8

G_CHECKERBOARD_SIZE = 0.119 # m.

G_IMAGE_PATTERN = "/*.png"

G_BASE_DIR = "/home/yyhu/Documents/CMU/AirLab/Shimizu/Stereo/Calibration/calibrationdata_20180726_01"

G_IMAGE_DIR = [\
    G_BASE_DIR + "/Left",\
    G_BASE_DIR + "/Right",\
]

G_OUT_DIR = [\
    G_BASE_DIR + "/OpenCV/IndividualLeft",\
    G_BASE_DIR + "/OpenCV/IndividualRight",\
]

G_COPY_DIR = [\
    G_BASE_DIR + "/OpenCV",
    G_BASE_DIR + "/OpenCV",
]

G_COPY_SUFFIX = [\
    "Left",\
    "Right",\
]

G_FLAG_DEBUG  = 0
G_DEBUG_COUNT = 2

FN_EXT                    = ".dat"
FN_CAMERA_MATRIX          = "CameraMatrix"
FN_DISTORTION_COEFFICIENT = "DistortionCoefficient"

def calibrate_single_camera(imageDir, outDir, copyDir = None, copySuffix = None):
    """
    copyDir: The directory to copy the intrinsics.
    copySuffix: The suffix string added to the filenames of the intrinsics.
    """
    # Termination criteria.
    criteria = ( cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER, 30, 0.001 )

    # Prepare object points.
    objIdx = np.zeros( (G_GRID_COL * G_GRID_ROW, 3), np.float32 )
    objIdx[:, :2] = np.mgrid[0:G_GRID_COL, 0:G_GRID_ROW].T.reshape(-1, 2)*G_CHECKERBOARD_SIZE

    # Arrays to store object points and image points from all images.
    objPoints = [] # 3D points in the real world.
    imgPoints = [] # 2D points in the image plane.

    imageFileNameList = sorted(glob.glob(imageDir + G_IMAGE_PATTERN))

    nImages = len(imageFileNameList)

    print("%d files to process..." % (nImages))

    count = 0
    countFailed  = 0

    for f in imageFileNameList:
        print("Process %s (%d / %d)..." % (f, count+1, nImages), end = '')

        img = cv2.imread(f)
        frameGray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

        # Find the corners on the checkerboard.
        ret, corners = cv2.findChessboardCorners(frameGray, (G_GRID_COL, G_GRID_ROW),\
         cv2.CALIB_CB_ADAPTIVE_THRESH + cv2.CALIB_CB_NORMALIZE_IMAGE + cv2.CALIB_CB_FAST_CHECK)

        # Check the result.
        if ( ret == True ):
            objPoints.append(objIdx)

            cornersSubPix = cv2.cornerSubPix(frameGray, corners, (11, 11), (-1, -1), criteria)
            imgPoints.append(cornersSubPix)

            img = cv2.drawChessboardCorners(img, (G_GRID_COL, G_GRID_ROW), cornersSubPix, ret)
            imgResized = cv2.resize(img, (960, 640))
            cv2.imshow('img', imgResized)

            # For debug use only.
            if ( 1 == G_FLAG_DEBUG ):
                cv2.waitKey(500)
                if ( G_DEBUG_COUNT - 1 == count ):
                    break
            else:
                cv2.waitKey(500)
            
            print("OK.")
        else:
            countFailed += 1
            print("Failed.")

        count += 1

    cv2.destroyAllWindows()

    print("%d of %d images failed." % (countFailed, nImages))

    print("Begin calibrating...")

    # Calibration.
    reprojectError, cameraMatrix, distortionCoefficients, rvecs, tvecs \
     = cv2.calibrateCamera(objPoints, imgPoints, frameGray.shape[::-1], None, None)

    # Check if the destination folder exists.
    if ( False == os.path.isdir(outDir) ):
        # Folder does not exist.
        os.makedirs(outDir)

    # Print and save the data.
    print("reprojectError = ")
    print(reprojectError)
    reprojectErrorWrapper = np.array([reprojectError])
    np.savetxt(outDir + "/ReprojectError.dat", reprojectErrorWrapper)

    print("cameraMatrix = ")
    print(cameraMatrix)
    # np.savetxt( outDir + "/CameraMatrix.dat", cameraMatrix )
    np.savetxt( outDir + "/" + FN_CAMERA_MATRIX + FN_EXT, cameraMatrix )

    print("distortionCoefficients = ")
    print(distortionCoefficients)
    # np.savetxt( outDir + "/DistortionCoefficients.dat", distortionCoefficients )
    np.savetxt( outDir + "/" + FN_DISTORTION_COEFFICIENT + FN_EXT, distortionCoefficients )

    if ( copyDir is not None ):
        if ( copySuffix is not None ):
            suffix = copySuffix
        else:
            suffix = ""
        
        np.savetxt( copyDir + "/" + FN_CAMERA_MATRIX          + suffix + FN_EXT, cameraMatrix )
        np.savetxt( copyDir + "/" + FN_DISTORTION_COEFFICIENT + suffix + FN_EXT, distortionCoefficients )
        print("Intrinsics copied to %s." % (copyDir))

    print("rvecs = ")
    print(rvecs)
    # np.savetxt( outDir + "/RVecs.dat", rvecs )

    print("tvecs = ")
    print(tvecs)
    # np.savetxt( outDir + "/TVecs.dat", tvecs )

    print("Done.")

if __name__ == "__main__":
    for i in range( len(G_IMAGE_DIR) ):
        calibrate_single_camera(G_IMAGE_DIR[i], G_OUT_DIR[i], G_COPY_DIR[i], G_COPY_SUFFIX[i])
