from __future__ import print_function

import shutil

class FileList(object):
    def __init__(self, baseDir, fl):
        self.baseDir  = baseDir
        self.fileList = fl

    def copy(self, dst):
        # Copy to the destination directory.

        for f in self.fileList:
            fromPath = self.baseDir + "/" + f

            shutil.copy2(fromPath, dst)

        