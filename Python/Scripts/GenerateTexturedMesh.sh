#!/bin/sh

meshlab.meshlabserver -l log_02_PointCloudAndTriangularMesh.log -p mlp_01_OriginalPointCloud.mlp -w mlp_02_PointCloudAndTriangularMesh.mlp -o ply_PointCloudWithNormal.ply -m vc vq vn vt fc fq fn wc wn wt -s generate_mesh.mlx

meshlab.meshlabserver -l log_04_TexturedMesh.log -p mlp_03_TriangularMesh.mlp -w mlp_04_TexturedMesh.mlp -o ply_MeshTextured.ply -m vc vq vn vt fc fq fn wc wn wt -s parameterization_texture.mlx

if [ -f thermal.jpg ];
then
	cp Poissonmesh_out.ply thermal_out.ply

	meshlab.meshlabserver -l log_06_TexturedMesh.log -p mlp_05_TriangularMesh.mlp -w mlp_06_TexturedMesh_Thermal.mlp -o ply_MeshTextured_Thermal.ply -m vc vq vn vt fc fq fn wc wn wt -s parameterization_thermal.mlx
else
	echo "thermal.jpg not found. Skipping the texture generation for thermal image."
fi
