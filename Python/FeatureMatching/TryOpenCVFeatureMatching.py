
from __future__ import print_function

# This code is obtained at
# https://docs.opencv2.org/master/d5/d6f/tutorial_feature_flann_matcher.html
#

import copy
import cv2
import numpy as np
import argparse

G_IDX_L = 0
G_IDX_R = 1

FN_CAMERA_MATRIX_LEFT    = "CameraMatrixLeft.dat"
FN_CAMERA_MATRIX_RIGHT   = "CameraMatrixRight.dat"
FN_DISTORTION_COEFFICIENT_LEFT  = "DistortionCoefficientLeft.dat"
FN_DISTORTION_COEFFICIENT_RIGHT = "DistortionCoefficientRight.dat"

FN_R1    = "R1.dat"
FN_R2    = "R2.dat"
FN_P1    = "P1.dat"
FN_P2    = "P2.dat"
FN_Q     = "Q.dat"
FN_ROI_1 = "ROI_1.dat"
FN_ROI_2 = "ROI_2.dat"

class StereoCamera(object):
    def __init__(self, paramDir):
        self.paramDir = paramDir
        
        # Camera matrices.
        self.cameraMatrix = [[], []]

        # Distortion coefficients.
        self.distortionCoefficients = [[], []]
        
        # Load R1 and R2.
        self.R12 = []

        # Load P1 and P2.
        self.P12 = []

        # Load Q.
        self.Q = None

        # Load Roi1 and Roi2.
        self.Roi12 = []

        self.isOpenCV = False

        self.rmap = [[], []]

        self.enableVignettingCorrector = False
        self.enableVCOnlyA             = False

        # The information of the camera
        self.sensorPixelSize  = [1.0, 1.0] # The actual pixel size, heigth and width, mm.

    def reload_params(self, isOpenCV = 0):
        # Camera matrices.
        self.cameraMatrix = [[], []]
        self.cameraMatrix[G_IDX_L] =\
            np.loadtxt( self.paramDir + "/" + FN_CAMERA_MATRIX_LEFT  )
        self.cameraMatrix[G_IDX_R] =\
            np.loadtxt( self.paramDir + "/" + FN_CAMERA_MATRIX_RIGHT )

        # Distortion coefficients.
        self.distortionCoefficients = [[], []]
        self.distortionCoefficients[G_IDX_L] =\
            np.loadtxt( self.paramDir + "/" + FN_DISTORTION_COEFFICIENT_LEFT  )
        self.distortionCoefficients[G_IDX_R] =\
            np.loadtxt( self.paramDir + "/" + FN_DISTORTION_COEFFICIENT_RIGHT )
        
        # Load R1 and R2.
        self.R12 = []
        filename = self.paramDir + "/" + FN_R1
        self.R12.append( np.loadtxt(filename) )
        filename = self.paramDir + "/" + FN_R2
        self.R12.append( np.loadtxt(filename) )

        # Load P1 and P2.
        self.P12 = []
        filename = self.paramDir + "/" + FN_P1
        self.P12.append( np.loadtxt(filename) )
        filename = self.paramDir + "/" + FN_P2
        self.P12.append( np.loadtxt(filename) )

        if ( isOpenCV > 0 ):
            print("Reloading higher level of details.")
            # Load Q.
            filename = self.paramDir + "/" + FN_Q
            self.Q = np.loadtxt(filename)

            # Load Roi1 and Roi2.
            self.Roi12 = []
            filename = self.paramDir + "/" + FN_ROI_1
            self.Roi12.append( np.loadtxt(filename) )
            filename = self.paramDir + "/" + FN_ROI_2
            self.Roi12.append( np.loadtxt(filename) )
            
            self.isOpenCV = True
        else:
            self.isOpenCV = False
    
    def get_remap(self, imageSize):
        """
        imageSize - Two-element tuple, width and height.
        """
        
        self.rmap = [[], []]

        # Left camera.
        map1, map2 = cv2.initUndistortRectifyMap(\
            self.cameraMatrix[G_IDX_L], self.distortionCoefficients[G_IDX_L],\
            self.R12[G_IDX_L], self.P12[G_IDX_L],\
            imageSize, cv2.CV_16SC2)
        self.rmap[G_IDX_L] = [map1, map2]

        # Right camera.
        map1, map2 = cv2.initUndistortRectifyMap(\
            self.cameraMatrix[G_IDX_R], self.distortionCoefficients[G_IDX_R],\
            self.R12[G_IDX_R], self.P12[G_IDX_R],\
            imageSize, cv2.CV_16SC2)
        self.rmap[G_IDX_R] = [map1, map2]

    def get_remap_by_idx(self, imageSize, idx):
        """
        imageSize - Two-element tuple, width and height.
        """

        map1, map2 = cv2.initUndistortRectifyMap(\
            self.cameraMatrix[idx], self.distortionCoefficients[idx],\
            self.R12[idx], self.P12[idx],\
            imageSize, cv2.CV_16SC2)
        self.rmap[idx] = [map1, map2]

    def rectify_image(self, img, idx):
        rimg = cv2.remap( img, self.rmap[idx][0], self.rmap[idx][1], cv2.INTER_LINEAR )

        if ( True == self.isOpenCV ):
            # Get the valid region by using the ROI. Note the idx.
            pass
        
        return rimg

if __name__ == "__main__":

    parser = argparse.ArgumentParser(description='Code for Feature Matching with FLANN tutorial.')

    parser.add_argument('--input1', help='Path to input image 1.', default='../data/TryOpenCVFeatureMatching/img02.jpg')
    parser.add_argument('--input2', help='Path to input image 2.', default='../data/TryOpenCVFeatureMatching/img01.jpg')
    parser.add_argument('--calib_dir', help='Path to input image 2.', default='../data/TryOpenCVFeatureMatching/Calibration')

    args = parser.parse_args()

    img1 = cv2.imread(args.input1, cv2.IMREAD_GRAYSCALE)
    img2 = cv2.imread(args.input2, cv2.IMREAD_GRAYSCALE)

    if img1 is None or img2 is None:
        print('Could not open or find the images!')
        exit(0)

    # img1 = cv2.pyrDown(img1)
    # img2 = cv2.pyrDown(img2)

    sc = StereoCamera(args.calib_dir)
    sc.reload_params(1)

    # ========== Test use. ==========
    cm = np.array([\
        [4700.0,    0.0, 2056.0],\
        [   0.0, 4700.0, 1504.0],\
        [   0.0,    0.0,    1.0]\
    ], dtype = np.float)
    dc = np.array([0.0, 0.0, 0.0, 0.0, 0.0], dtype = np.float).reshape([1, 5])
    sc.cameraMatrix[0] = cm
    sc.cameraMatrix[1] = cm
    sc.distortionCoefficients[0] = dc
    sc.distortionCoefficients[1] = dc
    # ========== End of test use. ==========

    imageSize = ( img1.shape[1], img1.shape[0] )
    sc.get_remap(imageSize)

    imgRectified = []
    imgRectified.append( sc.rectify_image( img1, G_IDX_L ) )
    # imgRectified.append( sc.rectify_image( img2, G_IDX_R ) )
    imgRectified.append( sc.rectify_image( img2, G_IDX_L ) )
    
    #-- Step 1: Detect the keypoints using SURF Detector, compute the descriptors
    minHessian = 400
    detector = cv2.xfeatures2d_SURF.create(hessianThreshold=minHessian)
    keypoints1, descriptors1 = detector.detectAndCompute(imgRectified[G_IDX_L], None)
    keypoints2, descriptors2 = detector.detectAndCompute(imgRectified[G_IDX_R], None)
    print("Keypoints detected.")

    #-- Step 2: Matching descriptor vectors with a FLANN based matcher
    # Since SURF is a floating-point descriptor NORM_L2 is used
    matcher = cv2.DescriptorMatcher_create(cv2.DescriptorMatcher_FLANNBASED)
    knn_matches = matcher.knnMatch(descriptors1, descriptors2, 2)
    print("FLANN matching done.")

    #-- Filter matches using the Lowe's ratio test
    ratio_thresh = 0.05
    good_matches = []
    for m,n in knn_matches:
        if m.distance < ratio_thresh * n.distance:
            good_matches.append(m)
            print("queryIdx = %d, trainIdx = %d, imgIdx = %d, distance = %f" % (m.queryIdx, m.trainIdx, m.imgIdx, m.distance))

    print("Matching result filtered. %d good matches." % (len(good_matches)))

    #-- Draw matches
    img_matches = np.empty((max(imgRectified[G_IDX_L].shape[0], imgRectified[G_IDX_R].shape[0]), imgRectified[G_IDX_L].shape[1]+imgRectified[G_IDX_R].shape[1], 3), dtype=np.uint8)
    debugMatchImage = np.zeros( (imgRectified[G_IDX_L].shape[0], imgRectified[G_IDX_L].shape[1]*2, 3), dtype = np.uint8 )

    cv2.drawMatches(imgRectified[G_IDX_L], keypoints1, imgRectified[G_IDX_R], keypoints2, good_matches, img_matches, flags=cv2.DrawMatchesFlags_NOT_DRAW_SINGLE_POINTS)

    # Get the points in place frist.
    goodPoints1 = []
    goodPoints2 = []

    for GM in good_matches:
        goodPoints1.append( [ keypoints1[ GM.queryIdx ].pt[0], keypoints1[ GM.queryIdx ].pt[1] ] )
        goodPoints2.append( [ keypoints2[ GM.trainIdx ].pt[0], keypoints2[ GM.trainIdx ].pt[1] ] )

    points1 = np.array(goodPoints1)
    points2 = np.array(goodPoints2)

    # # import pdb
    # # pdb.set_trace()
    # debugMatchImage[:, :imgRectified[G_IDX_L].shape[1], :] = cv2.cvtColor( imgRectified[G_IDX_L], cv2.COLOR_GRAY2BGR )
    # debugMatchImage[:, imgRectified[G_IDX_L].shape[1]:, :] = cv2.cvtColor( imgRectified[G_IDX_R], cv2.COLOR_GRAY2BGR )

    # cv2.namedWindow("DebugMatch", cv2.WINDOW_NORMAL)
    # for i in range( points1.shape[0] ):
    #     p1 = ((int)(points1[i, 0]), (int)(points1[i, 1]))
    #     p2 = (imgRectified[G_IDX_L].shape[1] + (int)(points2[i, 0]), (int)(points2[i, 1]))
    #     cv2.circle(debugMatchImage, p1, 10, (255, 0, 0), thickness = 5)
    #     cv2.circle(debugMatchImage, p2, 10, (0, 0, 255), thickness = 5)
    #     cv2.line(debugMatchImage, p1, p2, (0, 255, 0), thickness = 5)
    #     cv2.imshow("DebugMatch", debugMatchImage)
    #     cv2.waitKey()

    diff = points1[:, 0] - points2[:, 0]
    print("diff = %f." % (diff.mean()))
    print("diff.min = %f, diff.max = %f." % (diff.min(), diff.max()))

    #-- Show detected matches
    cv2.namedWindow("GoodMatches", cv2.WINDOW_NORMAL)
    cv2.imshow("GoodMatches", img_matches)

    #-- Compute the essential matrix.
    
    print("Begin estimating the essential matrix.")
    f = sc.cameraMatrix[G_IDX_L][0][0]
    pp = (sc.cameraMatrix[G_IDX_L][0][2], sc.cameraMatrix[G_IDX_L][1][2])
    # WARNING: This may fail since all the corresponding points may roughly lie on a single plane!
    essentialMatrix, mask = cv2.findEssentialMat(points1, points2, focal = f, pp = pp)
    print(essentialMatrix)

    print("Begin estimateing the rotation matrix and translation vector.")
    R1, R2, T = cv2.decomposeEssentialMat(essentialMatrix)

    print("R1 = "); print(R1)
    print("R2 = "); print(R2)
    print("T = "); print(T)

    cv2.waitKey()
