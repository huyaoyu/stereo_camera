#!/usr/bin/env python

'''
Resulting .ply file cam be easily viewed using MeshLab ( http://meshlab.sourceforge.net/ )
'''

# Python 2/3 compatibility
from __future__ import print_function

import copy
import math
import os
import io
import shutil
import stat
import threading
import time
from xml.dom import minidom
import xml.etree.ElementTree as et

import numpy as np
import cv2

from VignettingCorrection import VignettingCorrector

ply_header = '''ply
format ascii 1.0
element vertex %(vert_num)d
property float x
property float y
property float z
property uchar red
property uchar green
property uchar blue
end_header
'''

ply_header_uv = '''ply
format ascii 1.0
comment VCGLIB generated
comment TextureFile %(texture_file)s
element vertex %(vert_num)d
property float x
property float y
property float z
property uchar red
property uchar green
property uchar blue
property float texture_u
property float texture_v
end_header
'''

ply_header_binary = '''ply
format binary 1.0
element vertex %(vert_num)d
property float x
property float y
property float z
property uchar red
property uchar green
property uchar blue
end_header
'''

G_IDX_L = 0
G_IDX_R = 1

FN_CAMERA_MATRIX_LEFT    = "CameraMatrixLeft.dat"
FN_CAMERA_MATRIX_RIGHT   = "CameraMatrixRight.dat"
FN_DISTORTION_COEFFICIENT_LEFT  = "DistortionCoefficientLeft.dat"
FN_DISTORTION_COEFFICIENT_RIGHT = "DistortionCoefficientRight.dat"

FN_R1    = "R1.dat"
FN_R2    = "R2.dat"
FN_P1    = "P1.dat"
FN_P2    = "P2.dat"
FN_Q     = "Q.dat"
FN_ROI_1 = "ROI_1.dat"
FN_ROI_2 = "ROI_2.dat"

SCRIPT_DIR = "Scripts"
SCRIPT_MESHLAB_GENERATE_MESH            = "generate_mesh.mlx"
SCRIPT_MESHLAB_PARAMETERIZATION_TEXTURE = "parameterization_texture.mlx"
SCRIPT_GENERATE_TEXTURED_MESH_SH        = "GenerateTexturedMesh.sh"

def test_create_path(p):
    if ( os.path.isdir(p) ):
        pass
    else:
        os.makedirs(p)

class MLP_Mesh(object):
    def __init__(self, filename, label, matrix):
        self.filename = filename
        self.label = label

        if ( matrix is None ):
            self.mlMatrix = np.eye(4, 4, dtype = np.float32)
        else:
            self.mlMatrix = matrix

class MLP_Raster(object):
    def __init__(self, filename, label, focalMm, pixelSizeMm, viewportPx, translationVector):
        """
        pixelSizeMm - two-element list.
        viewportPx - two-element list.
        translationVector - four-element list or four-element numpy array.
        """
        self.filename = filename
        self.label    = label

        self.rotatingMatrix = np.eye(4, 4, dtype=np.float32)
        self.focalMm        = focalMm
        self.pixelSizeMm    = pixelSizeMm
        self.viewportPx     = viewportPx
        self.cameraType     = 0
        self.lensDistortion = [0, 0]
        self.translationVector = translationVector
        self.centerPx       = [ (int)(viewportPx[0]/2), (int)(viewportPx[1]/2) ]
        self.plane_semantic = 1

class MeshLabProjectFile(object):
    def __init__(self):
        self.meshGroup = []
        self.rasterGroup = []

        self.ele_MLMesh = "MLMesh"
        self.ele_MLMesh_filename = "filename"
        self.ele_MLMesh_label = "label"

        self.ele_MLMatrix44 = "MLMatrix44"

        self.ele_MLRaster = "MLRaster"
        self.ele_MLRaster_label = "label"

        self.ele_VCGCamera = "VCGCamera"
        self.ele_VCGCamera_RotationMatrix    = "RotationMatrix"
        self.ele_VCGCamera_FocalMm           = "FocalMm"
        self.ele_VCGCamera_PixelSizeMm       = "PixelSizeMm"
        self.ele_VCGCamera_ViewportPx        = "ViewportPx"
        self.ele_VCGCamera_CameraType        = "CameraType"
        self.ele_VCGCamera_LensDistortion    = "LensDistortion"
        self.ele_VCGCamera_TranslationVector = "TranslationVector"
        self.ele_VCGCamera_CenterPx          = "CenterPx"

        self.ele_Plane = "Plane"
        self.ele_Plane_semantic = "semantic"
        self.ele_Plane_filename = "fileName"

        self.ele_MeshLabProject = "MeshLabProject"
        self.ele_MeshGroup = "MeshGroup"
        self.ele_RasterGroup = "RasterGroup"

        self.DOCTYPE = "MeshLabDocument"

    def add_mesh(self, mesh):
        self.meshGroup.append(mesh)

    def add_raster(self, raster):
        self.rasterGroup.append(raster)

    def convert_mesh(self, root, mesh):
        MLMesh = et.SubElement(root, self.ele_MLMesh)

        MLMesh.set(self.ele_MLMesh_filename, mesh.filename)
        MLMesh.set(self.ele_MLMesh_label, mesh.label)

        MLMatrix44 = et.SubElement(MLMesh, self.ele_MLMatrix44)
        sio = io.StringIO()
        np.savetxt(sio, mesh.mlMatrix, fmt="%f %f %f %f ")
        MLMatrix44.text = "\n%s" % (sio.getvalue())

        return MLMesh

    def get_matrix_string_one_line(self, m, f):
        """m is a list, numpy array, numpy vector, numpy matrix."""

        sio = io.StringIO()
        mr = m.reshape(1, -1)
        np.savetxt(sio, mr, fmt=f)

        return sio.getvalue()

    def convert_raster(self, root, raster):
        # MLRaster element.
        MLRaster = et.SubElement(root, self.ele_MLRaster)
        # Attributes for MLRaster.
        MLRaster.set(self.ele_MLRaster_label, raster.label)

        # VCGCamera element as the child element of MLRaster.
        VCGCamera = et.SubElement(MLRaster, self.ele_VCGCamera)

        # Attributes for VCGCamera.
        rotationMatrixString = self.get_matrix_string_one_line(raster.rotatingMatrix, "%f")
        VCGCamera.set(self.ele_VCGCamera_RotationMatrix, rotationMatrixString[:-1])

        VCGCamera.set(self.ele_VCGCamera_FocalMm, str(raster.focalMm))
        twoElementListString = "%f %f" % (raster.pixelSizeMm[0], raster.pixelSizeMm[1])
        VCGCamera.set(self.ele_VCGCamera_PixelSizeMm, twoElementListString)
        twoElementListString = "%d %d" % (raster.viewportPx[0], raster.viewportPx[1])
        VCGCamera.set(self.ele_VCGCamera_ViewportPx, twoElementListString)
        VCGCamera.set(self.ele_VCGCamera_CameraType, str(raster.cameraType))
        twoElementListString = "%f %f" % (raster.lensDistortion[0], raster.lensDistortion[1])
        VCGCamera.set(self.ele_VCGCamera_LensDistortion, twoElementListString)

        translationVectorString = self.get_matrix_string_one_line( raster.translationVector, "%f" )
        VCGCamera.set(self.ele_VCGCamera_TranslationVector, translationVectorString[:-1])

        twoElementListString = "%d %d" % (raster.centerPx[0], raster.centerPx[1])
        VCGCamera.set(self.ele_VCGCamera_CenterPx, twoElementListString)
        
        # Plane element as the child element of MLRaster.
        Plane = et.SubElement(MLRaster, self.ele_Plane)

        # Attributes for Plane.
        Plane.set(self.ele_Plane_semantic, str(raster.plane_semantic))
        Plane.set(self.ele_Plane_filename, raster.filename)

        return MLRaster

    def write_mlp(self, fn):
        """Write the mlp file."""

        # Open the file for output.
        with open(fn, "w") as fp:
            openingString = "<!DOCTYPE %s>" % (self.DOCTYPE)
            fp.write(openingString)

            MeshLabProject = et.Element(self.ele_MeshLabProject)

            MeshGroup = et.SubElement(MeshLabProject, self.ele_MeshGroup)

            for mesh in self.meshGroup:
                self.convert_mesh(MeshGroup, mesh)

            RasterGroup = et.SubElement(MeshLabProject, self.ele_RasterGroup)

            for raster in self.rasterGroup:
                self.convert_raster(RasterGroup, raster)
            
            xmlstr = minidom.parseString(et.tostring( MeshLabProject )).toprettyxml(indent="    ")

            fp.write( xmlstr[22:] )

            fp.close()

def get_uv_coordinates(nRows, nCols, mask = None):
    # Create local temporary index matrices.
    row = np.linspace(0, nRows-1, nRows, dtype = np.float32)
    col = np.linspace(0, nCols-1, nCols, dtype = np.float32)

    x, y = np.meshgrid( col, row )

    # Normalize.
    x = x / ( nCols - 1 )
    y = y / ( nRows - 1 )

    # Apply the mask.
    if ( mask is None ):
        u = x
        v = y
    else:
        u = x[mask]
        v = y[mask]

    return u, v

def write_ply(fn, verts, colors):
    verts  = verts.reshape(-1, 3)
    colors = colors.reshape(-1, 3)
    verts  = np.hstack([verts, colors])

    with open(fn, 'wb') as f:
        f.write((ply_header % dict(vert_num=len(verts))).encode('utf-8'))
        np.savetxt(f, verts, fmt='%f %f %f %d %d %d ')

def write_ply_uv(fn, verts, colors, u, v, tf):
    verts  = verts.reshape(-1, 3)
    colors = colors.reshape(-1, 3)
    u = u.reshape(-1, 1)
    v = v.reshape(-1, 1)
    verts  = np.hstack([verts, colors, u, v])

    with open(fn, 'wb') as f:
        f.write((ply_header_uv % dict(vert_num=len(verts), texture_file=tf)).encode('utf-8'))
        np.savetxt(f, verts, fmt='%f %f %f %d %d %d %f %f ')

def write_ply_binary(fn, verts, colors):
    verts  = verts.reshape(-1, 3)
    colors = colors.reshape(-1, 3)
    verts  = np.hstack([verts, colors])

    with open(fn, 'wb') as f:
        f.write((ply_header_binary % dict(vert_num=len(verts))).encode('utf-8'))
        verts.tofile(f, sep="", fmt='%f %f %f %d %d %d ')

class PreProcess ( threading.Thread ):
    def __init__(self, name, parent, idx, workingDir, imgFilename, enableVignettingCorrector, enableVCOnlyA):
        threading.Thread.__init__(self)
        self.name = name
        self.parent = parent
        self.idx = idx
        self.workingDir  = workingDir
        self.imgFilename = imgFilename
        self.enableVignettingCorrector = enableVignettingCorrector
        self.enableVCOnlyA = enableVCOnlyA

        self.imgRectifiedGray  = None
        self.imgRectifiedColor = None

    def run(self):
        """Perform preprocess on a single image."""

        # Read the image file.
        imgOri = cv2.imread( self.imgFilename )

        # Convert the image into grayscale image.
        img = cv2.cvtColor( imgOri, cv2.COLOR_BGR2GRAY )

        # Vignetting correction.
        if ( self.enableVignettingCorrector ):
            self.print_msg("Begin vignetting correction, this will last for 1 minute...")
            vcorrector = VignettingCorrector(self.name)
            vcorrector.onlyA = self.enableVCOnlyA

            self.print_msg("Correcting image...")
            startTime = time.time()
            img = vcorrector.correct( img )

            self.print_msg("Save corrected images.")
            saveName = "VC_Gray_%s.bmp" % (self.name)
            cv2.imwrite(self.workingDir + "/" + saveName, img)
            endTime = time.time()
            self.print_msg("Vignetting correction done with %ds." % (endTime - startTime))

        # Get the image size.
        imageSize = ( img.shape[1], img.shape[0] )

        self.parent.get_remap_by_idx(imageSize, self.idx)

        self.imgRectifiedGray = self.parent.rectify_image( img, self.idx )

        self.imgRectifiedColor = self.parent.rectify_image( imgOri, self.idx ) 

        # Save the images.
        saveName = "Rectified_%s.jpg" % (self.name)
        cv2.imwrite(self.workingDir +  "/" + saveName, self.imgRectifiedGray, [int(cv2.IMWRITE_JPEG_QUALITY), 100])

        saveName = "Rectified_%s_color.jpg" % (self.name)
        cv2.imwrite(self.workingDir +  "/" + saveName, self.imgRectifiedColor, [int(cv2.IMWRITE_JPEG_QUALITY), 100])

    def print_msg(self, msg):
        print("%s: %s" % ( self.name, msg ))

class ComputeDisparity ( threading.Thread ):
    def __init__(self, name, parent, idx, matcher, img0, img1):
        threading.Thread.__init__(self)
        self.name = name
        self.parent = parent
        self.idx = idx

        self.matcher = matcher
        self.img0 = img0
        self.img1 = img1
        self.disparity = None

    def run(self):
        self.print_msg('computing disparity...')
        startTime = time.time()
        self.disparity = self.matcher.compute( self.img0, self.img1 ).astype(np.int16)
        endTime = time.time()
        self.print_msg('%ds elapsed.' % ( (int)(endTime - startTime) ))

    def print_msg(self, msg):
        print("%s: %s" % (self.name, msg))

class Reconstructor(object):
    def __init__(self, paramDir):
        self.paramDir = paramDir
        
        # Camera matrices.
        self.cameraMatrix = [[], []]

        # Distortion coefficients.
        self.distortionCoefficients = [[], []]
        
        # Load R1 and R2.
        self.R12 = []

        # Load P1 and P2.
        self.P12 = []

        # Load Q.
        self.Q = None

        # Load Roi1 and Roi2.
        self.Roi12 = []

        self.isOpenCV = False

        self.minDisp           = 1
        self.numDisp           = 192
        self.blockSize         = 11
        # self.SGBM_P1           = 8  * 3 * self.block**2
        # self.SGBM_P2           = 32 * 3 * self.block**2
        self.SGBM_P1           = 600
        self.SGBM_P2           = 2400
        self.disp12MaxDiff     = 10
        self.uniquenessRatio   = 1
        self.speckleWindowSize = 150
        self.speckleRange      = 2
        self.preFilterCap      = 4

        self.rmap = [[], []]

        self.haveWLSFilter = False
        self.WLSF_Lambda   = 8000.0
        self.WLSF_Sigma    = 1.5

        self.enableVignettingCorrector = False
        self.enableVCOnlyA             = False

        # The information of the camera
        self.sensorPixelSize  = [1.0, 1.0] # The actual pixel size, heigth and width, mm.

        # For MeshLab.
        self.scriptDir = SCRIPT_DIR
        self.scriptMeshLabGenerateMesh            = SCRIPT_MESHLAB_GENERATE_MESH
        self.scriptMeshLabParameterizationTexture = SCRIPT_MESHLAB_PARAMETERIZATION_TEXTURE
        self.scriptGenerateTexturedMeshSh         = SCRIPT_GENERATE_TEXTURED_MESH_SH

    def reload_params(self, isOpenCV = 0):
        # Camera matrices.
        self.cameraMatrix = [[], []]
        self.cameraMatrix[G_IDX_L] =\
            np.loadtxt( self.paramDir + "/" + FN_CAMERA_MATRIX_LEFT  )
        self.cameraMatrix[G_IDX_R] =\
            np.loadtxt( self.paramDir + "/" + FN_CAMERA_MATRIX_RIGHT )

        # Distortion coefficients.
        self.distortionCoefficients = [[], []]
        self.distortionCoefficients[G_IDX_L] =\
            np.loadtxt( self.paramDir + "/" + FN_DISTORTION_COEFFICIENT_LEFT  )
        self.distortionCoefficients[G_IDX_R] =\
            np.loadtxt( self.paramDir + "/" + FN_DISTORTION_COEFFICIENT_RIGHT )
        
        # Load R1 and R2.
        self.R12 = []
        filename = self.paramDir + "/" + FN_R1
        self.R12.append( np.loadtxt(filename) )
        filename = self.paramDir + "/" + FN_R2
        self.R12.append( np.loadtxt(filename) )

        # Load P1 and P2.
        self.P12 = []
        filename = self.paramDir + "/" + FN_P1
        self.P12.append( np.loadtxt(filename) )
        filename = self.paramDir + "/" + FN_P2
        self.P12.append( np.loadtxt(filename) )

        if ( isOpenCV > 0 ):
            print("Reloading higher level of details.")
            # Load Q.
            filename = self.paramDir + "/" + FN_Q
            self.Q = np.loadtxt(filename)

            # Load Roi1 and Roi2.
            '''
            self.Roi12 = []
            filename = self.paramDir + "/" + FN_ROI_1
            self.Roi12.append( np.loadtxt(filename) )
            filename = self.paramDir + "/" + FN_ROI_2
            self.Roi12.append( np.loadtxt(filename) )
            '''
            self.isOpenCV = True
        else:
            self.isOpenCV = False

    def get_remap(self, imageSize):
        """
        imageSize - Two-element tuple, width and height.
        """
        
        self.rmap = [[], []]

        # Left camera.
        map1, map2 = cv2.initUndistortRectifyMap(\
            self.cameraMatrix[G_IDX_L], self.distortionCoefficients[G_IDX_L],\
            self.R12[G_IDX_L], self.P12[G_IDX_L],\
            imageSize, cv2.CV_16SC2)
        self.rmap[G_IDX_L] = [map1, map2]

        # Right camera.
        map1, map2 = cv2.initUndistortRectifyMap(\
            self.cameraMatrix[G_IDX_R], self.distortionCoefficients[G_IDX_R],\
            self.R12[G_IDX_R], self.P12[G_IDX_R],\
            imageSize, cv2.CV_16SC2)
        self.rmap[G_IDX_R] = [map1, map2]

    def get_remap_by_idx(self, imageSize, idx):
        """
        imageSize - Two-element tuple, width and height.
        """

        map1, map2 = cv2.initUndistortRectifyMap(\
            self.cameraMatrix[idx], self.distortionCoefficients[idx],\
            self.R12[idx], self.P12[idx],\
            imageSize, cv2.CV_16SC2)
        self.rmap[idx] = [map1, map2]

    def rectify_image(self, img, idx):
        rimg = cv2.remap( img, self.rmap[idx][0], self.rmap[idx][1], cv2.INTER_LINEAR )

        if ( True == self.isOpenCV ):
            # Get the valid region by using the ROI. Note the idx.
            pass
        
        return rimg

    def reconstruct(self, workingDir, imgFileNames):
        test_create_path(workingDir)
        
        print('loading images...')
        # imgL = cv2.pyrDown( cv2.imread('../data/aloeL.jpg') )  # downscale images for faster processing
        # imgR = cv2.pyrDown( cv2.imread('../data/aloeR.jpg') )

        imgsOri = []
        imgsOri.append( cv2.imread( imgFileNames[G_IDX_L] ) )
        imgsOri.append( cv2.imread( imgFileNames[G_IDX_R] ) )

        imgs = []
        imgs.append( cv2.cvtColor( imgsOri[G_IDX_L], cv2.COLOR_BGR2GRAY ) )
        imgs.append( cv2.cvtColor( imgsOri[G_IDX_R], cv2.COLOR_BGR2GRAY ) )
        # imgs.append( imgsOri[G_IDX_L] )
        # imgs.append( imgsOri[G_IDX_R] )

        if ( True == self.enableVignettingCorrector ):
            print("Begin vignetting correction, this will last for 1 minute...")
            vcorrector = VignettingCorrector("CV")
            vcorrector.onlyA = self.enableVCOnlyA

            print("Left image...")
            imgs[G_IDX_L] = vcorrector.correct( imgs[G_IDX_L] )
            print("Right image...")
            imgs[G_IDX_R] = vcorrector.correct( imgs[G_IDX_R] )

            print("Save corrected images.")
            cv2.imwrite(workingDir + "/VC_Gray_Left.bmp",  imgs[G_IDX_L])
            cv2.imwrite(workingDir + "/VC_Gray_Right.bmp", imgs[G_IDX_R])
            print("Vignetting correction done.")

        # Get the image size.
        imageSize = ( imgs[G_IDX_L].shape[1], imgs[G_IDX_L].shape[0] )
        self.get_remap(imageSize)

        imgRectified = []
        imgRectified.append( self.rectify_image( imgs[G_IDX_L], G_IDX_L ) )
        imgRectified.append( self.rectify_image( imgs[G_IDX_R], G_IDX_R ) )

        imgRectifiedColor = []
        imgRectifiedColor.append( self.rectify_image( imgsOri[G_IDX_L], G_IDX_L ) )
        imgRectifiedColor.append( self.rectify_image( imgsOri[G_IDX_R], G_IDX_R ) )

        # Save the images.
        cv2.imwrite(workingDir +  "/Left.jpg", imgRectified[G_IDX_L], [int(cv2.IMWRITE_JPEG_QUALITY), 100])
        cv2.imwrite(workingDir + "/Right.jpg", imgRectified[G_IDX_R], [int(cv2.IMWRITE_JPEG_QUALITY), 100])

        imgRectifiedGray = []
        # imgRectifiedGray.append( cv2.cvtColor( imgRectified[G_IDX_L], cv2.COLOR_BGR2GRAY ) )
        # imgRectifiedGray.append( cv2.cvtColor( imgRectified[G_IDX_R], cv2.COLOR_BGR2GRAY ) )
        imgRectifiedGray.append( imgRectified[G_IDX_L] )
        imgRectifiedGray.append( imgRectified[G_IDX_R] )

        # disparity range should be tuned
        leftMatcher = cv2.StereoSGBM_create(minDisparity = self.minDisp,
            numDisparities = self.numDisp,
            blockSize = self.blockSize,
            P1 = self.SGBM_P1,
            P2 = self.SGBM_P2,
            disp12MaxDiff = self.disp12MaxDiff,
            uniquenessRatio = self.uniquenessRatio,
            speckleWindowSize = self.speckleWindowSize,
            speckleRange = self.speckleRange,
            preFilterCap = self.preFilterCap
        )

        if ( True == self.haveWLSFilter ):
            wlsFilter    = cv2.ximgproc.createDisparityWLSFilter(leftMatcher)
            rightMatcher = cv2.ximgproc.createRightMatcher(leftMatcher)

        print('computing disparity...')
        startTime = time.time()
        disp = leftMatcher.compute( imgRectifiedGray[G_IDX_L], imgRectifiedGray[G_IDX_R] ).astype(np.int16)
        endTime = time.time()
        print('%ds elpased.' % ( (int)(endTime - startTime) ))

        if ( True == self.haveWLSFilter ):
            print('Comput right disparity...')
            startTime = time.time()
            dispR = rightMatcher.compute( imgRectifiedGray[G_IDX_R], imgRectifiedGray[G_IDX_L] ).astype(np.int16)
            endTime = time.time()
            print('%ds elapsed.' % ( (int)(endTime - startTime) ))

            wlsFilter.setLambda(self.WLSF_Lambda)
            wlsFilter.setSigmaColor(self.WLSF_Sigma)

            print('Begin filtering...')
            startTime = time.time()
            filteredDisp = wlsFilter.filter(disp, imgRectifiedGray[G_IDX_L], disparity_map_right=dispR)
            endTime = time.time()
            print('%ds elapsed.' % ( (int)(endTime - startTime) ))

            confidenceMap = wlsFilter.getConfidenceMap()

        if ( True == self.haveWLSFilter ):
            filteredDisp = filteredDisp / 16.0
            disp = filteredDisp.astype(np.float32)
        else:
            disp  = disp.astype(np.float32) / 16.0

        print('generating 3d point cloud...',)
        h, w = imgRectified[G_IDX_L].shape[:2]

        f = self.cameraMatrix[G_IDX_L][0][0]

        Q = np.array([[1, 0, 0, -0.5*w],
                        [0,-1, 0,  0.5*h], # turn points 180 deg around x-axis,
                        [0, 0, 0,     -f], # so that y-axis looks up
                        [0, 0, 1,      0]], dtype = np.float32)
        points = cv2.reprojectImageTo3D(disp, Q)
        colors = cv2.cvtColor(imgRectifiedColor[G_IDX_L], cv2.COLOR_BGR2RGB)
        mask = disp > disp.min()
        out_points = points[mask]
        out_colors = colors[mask]
        out_fn = workingDir + '/out.ply'
        write_ply(out_fn, out_points, out_colors)
        print('%s saved' % 'out.ply')

        cv2.imshow('left', imgRectified[G_IDX_L])
        cv2.imshow('disparity', (disp-self.minDisp)/self.numDisp)
        cv2.waitKey()
        cv2.destroyAllWindows()

    def reconstruct_multithreading(self, workingDir, imgFileNames):
        """Do the work in parellel."""

        test_create_path(workingDir)
        
        # Create two threads and start.
        threadLeft = PreProcess(\
            "Left", self, G_IDX_L,\
            workingDir, imgFileNames[G_IDX_L],\
            self.enableVignettingCorrector, self.enableVCOnlyA)

        threadRight = PreProcess(\
            "Right", self, G_IDX_R,\
            workingDir, imgFileNames[G_IDX_R],\
            self.enableVignettingCorrector, self.enableVCOnlyA)

        threads = [threadLeft, threadRight]

        for t in threads:
            t.start()

        for t in threads:
            t.join()

        imgRectifiedGray  = [ threads[G_IDX_L].imgRectifiedGray,  threads[G_IDX_R].imgRectifiedGray ]
        imgRectifiedColor = [ threads[G_IDX_L].imgRectifiedColor, threads[G_IDX_R].imgRectifiedColor ]

        # disparity range should be tuned
        leftMatcher = cv2.StereoSGBM_create(minDisparity = self.minDisp,
            numDisparities = self.numDisp,
            blockSize = self.blockSize,
            P1 = self.SGBM_P1,
            P2 = self.SGBM_P2,
            disp12MaxDiff = self.disp12MaxDiff,
            uniquenessRatio = self.uniquenessRatio,
            speckleWindowSize = self.speckleWindowSize,
            speckleRange = self.speckleRange,
            preFilterCap = self.preFilterCap
        )

        computeDisparityLeft = ComputeDisparity("Left", self, G_IDX_L,\
            leftMatcher, imgRectifiedGray[G_IDX_L], imgRectifiedGray[G_IDX_R])

        threads = []
        threads.append( computeDisparityLeft )

        if ( True == self.haveWLSFilter ):
            wlsFilter    = cv2.ximgproc.createDisparityWLSFilter(leftMatcher)
            rightMatcher = cv2.ximgproc.createRightMatcher(leftMatcher)

            computeDisparityRight = ComputeDisparity("Right", self, G_IDX_R,\
                rightMatcher, imgRectifiedGray[G_IDX_R], imgRectifiedGray[G_IDX_L])

            threads.append( computeDisparityRight )

        for t in threads:
            t.start()

        for t in threads:
            t.join()

        disp = computeDisparityLeft.disparity

        if ( True == self.haveWLSFilter ):
            dispR = computeDisparityRight.disparity

            wlsFilter.setLambda(self.WLSF_Lambda)
            wlsFilter.setSigmaColor(self.WLSF_Sigma)

            print('Begin filtering...')
            startTime = time.time()
            filteredDisp = wlsFilter.filter(disp, imgRectifiedGray[G_IDX_L], disparity_map_right=dispR)
            endTime = time.time()
            print('%ds elapsed.' % ( (int)(endTime - startTime) ))

            confidenceMap = wlsFilter.getConfidenceMap()

        if ( True == self.haveWLSFilter ):
            filteredDisp = filteredDisp / 16.0
            disp = filteredDisp.astype(np.float32)
        else:
            disp  = disp.astype(np.float32) / 16.0

        print('generating 3d point cloud...',)
        h, w = imgRectifiedGray[G_IDX_L].shape[:2]

        if ( self.Q is not None ):
            Q = self.Q

            f = Q[2,3]

            Q[1,1] = -1.0 * Q[1,1]
            Q[1,3] = -1.0 * Q[1,3]
            Q[2,3] = -1.0 * Q[2,3]
        else:
            f = self.cameraMatrix[G_IDX_L][0][0]
            Q = np.array([[1, 0, 0, -0.5*w],
                        [0,-1, 0,  0.5*h], # turn points 180 deg around x-axis,
                        [0, 0, 0,     -f], # so that y-axis looks up
                        [0, 0, 1,      0]], dtype = np.float32)

        points = cv2.reprojectImageTo3D(disp, Q)
        colors = cv2.cvtColor(imgRectifiedColor[G_IDX_L], cv2.COLOR_BGR2RGB)
        mask = disp > disp.min()

        dispMasked = disp[mask]
        print("min_disp = %f, max_disp = %f, mean_disp = %f." % (np.min(dispMasked), np.max(dispMasked), np.mean(dispMasked)))

        # Prepare the uv coordinates for texture mapping.
        print("disp.shape = (%d, %d)" % (disp.shape[0], disp.shape[1]))
        u, v = get_uv_coordinates( disp.shape[0], disp.shape[1], mask )

        v = 1 - v

        out_points = points[mask]
        out_colors = colors[mask]
        out_fn = workingDir + '/ply_01_OriginalPointCloud.ply'
        write_ply(out_fn, out_points, out_colors)
        # write_ply_uv(out_fn, out_points, out_colors, u, v, 'Rectified_Left_color.jpg')
        print('%s saved' % out_fn)

        # MeshLab project file.
        mesh = MLP_Mesh("ply_01_OriginalPointCloud.ply", "ply_01_OriginalPointCloud", np.eye(4,4,dtype=np.float32))
        dummyTranslationVector = np.zeros((1,4), dtype=np.float32)
        dummyTranslationVector[0, 2] = 1e-6
        dummyTranslationVector[0, 3] = 1.0
        focalLengthMm = math.fabs(f * self.sensorPixelSize[0])
        raster = MLP_Raster("Rectified_Left_color.jpg", "Rectified_Left_color.jpg",\
            focalLengthMm, self.sensorPixelSize,\
            [imgRectifiedGray[0].shape[1], imgRectifiedGray[0].shape[0]],\
            dummyTranslationVector)

        # raster.centerPx = [ (int)(math.fabs(Q[0,3])), (int)(math.fabs(Q[1,3])) ]
        raster.centerPx = [\
            (int)( w - math.fabs(Q[0,3]) ),\
            (int)( h - math.fabs(Q[1,3]) )]

        rotationMatrix = np.zeros((4,4), dtype=np.float32)
        rotationMatrix[3,3] = 1.0
        # rotationMatrix[:3,:3] = self.R12[G_IDX_L]
        rotationMatrix[:3,:3] = np.eye(3, 3, dtype=np.float32)
        raster.rotatingMatrix = rotationMatrix

        mlp = MeshLabProjectFile()
        mlp.add_mesh(mesh)
        mlp.add_raster(raster)
        mlpFile = workingDir + "/mlp_01_OriginalPointCloud.mlp"
        mlp.write_mlp(mlpFile)

        mesh.filename = "Poissonmesh_out.ply"
        mesh.label    = "Poissonmesh_out.ply"
        mlp = MeshLabProjectFile()
        mlp.add_mesh(mesh)
        mlp.add_raster(raster)
        mlpFile = workingDir + "/mlp_03_TriangularMesh.mlp"
        mlp.write_mlp(mlpFile)

        # Copy the scripts into workingDir.
        shutil.copy2(self.scriptDir + "/" + self.scriptMeshLabGenerateMesh, workingDir)
        shutil.copy2(self.scriptDir + "/" + self.scriptMeshLabParameterizationTexture, workingDir)
        shutil.copy2(self.scriptDir + "/" + self.scriptGenerateTexturedMeshSh, workingDir)
        # Change the permission of the .sh script.
        os.chmod( workingDir + "/" + self.scriptGenerateTexturedMeshSh, stat.S_IRWXU )

        cv2.namedWindow("left", cv2.WINDOW_NORMAL)
        cv2.imshow('left', imgRectifiedGray[G_IDX_L])
        cv2.namedWindow("disparity", cv2.WINDOW_NORMAL)
        cv2.imshow('disparity', (disp-self.minDisp)/self.numDisp)
        cv2.waitKey()
        cv2.destroyAllWindows()
        return points,imgRectifiedColor[G_IDX_L],mask