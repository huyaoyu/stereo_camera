from __future__ import print_function

import numpy as np
import cv2
import glob
import os
from StereoCalibrator import StereoCalibrator

G_GRID_ROW = 6
G_GRID_COL = 9

G_CHECKERBOARD_SIZE = 37 # mm.

G_L_IMAGE_DIR = "../Data_20180501/left_dst_resized"
# G_L_IMAGE_DIR = "../Data_20180501/left_small_resized"
G_R_IMAGE_DIR = "../Data_20180501/right_dst"
# G_R_IMAGE_DIR = "../Data_20180501/right_small"

G_OUT_DIR = "./TempCalibrated"

if __name__ == "__main__":
    calib = StereoCalibrator(\
        (G_GRID_COL, G_GRID_ROW),\
         G_CHECKERBOARD_SIZE, G_L_IMAGE_DIR, G_R_IMAGE_DIR, G_OUT_DIR)

    calib.load_calibration_results()
    calib.show_calibration_results()

    avgErr = calib.check_calibration_quality()
    print("avgErr = %f." % (avgErr))

    # Stereo rectify.
    calib.R1, calib.R2, calib.P1, calib.P2, calib.Q,\
    calib.Roi1, calib.Roi2 = calib.stereo_rectify()
    calib.write_rectified_matrices()
    calib.show_rectified_matrices()

    calib.compute_remap_and_create_rectified_image()
