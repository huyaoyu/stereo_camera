#!/usr/bin/python

from __future__ import print_function

import argparse
import os
import sys

# Modify the python search path.
for _p in os.environ["CUSTOM_PYTHON_PATH"].split(":"):
    if ( 0 != len(_p) ):
        sys.path.insert( 0, _p )

# import ipdb; ipdb.set_trace()

from StereoROSScripts.BagExtraction.AlignStereoImages import align_stereo_images
    
if __name__ == "__main__":
    # Test.
    # align_stereo_images("/home/yyhu/expansion/Shimizu/20180923/2018-09-23-16-16-55/Stereo")
    # align_stereo_images("/home/yyhu/expansion/Shimizu/20180925/camera_data_1/Stereo")

    parser = argparse.ArgumentParser(description='Align the image information for an unsteady scene..')

    parser.add_argument("--input_dir", help = "The the directory in which the images have been extracted.", default = "../images/calibration/stereo_calibration_01")
    parser.add_argument("--dir_0", help = "The 0 directory.", default = "Left")
    parser.add_argument("--dir_1", help = "The 1 directory.", default = "Right")

    args = parser.parse_args()

    align_stereo_images(args.input_dir, args.dir_0, args.dir_1)
