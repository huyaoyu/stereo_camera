#!/usr/bin/env python

from __future__ import print_function

import argparse
import glob
import json
import math
import os
import time
import sys

# Modify the python search path.
for _p in os.environ["CUSTOM_PYTHON_PATH"].split(":"):
    if ( 0 != len(_p) ):
        sys.path.insert( 0, _p )

from StereoPython.StereoCamera.StereoCamera import StereoCamera 

# Sample JSON file
# {
#     "DR_PARAM": "/home/yyhu/Documents/CMU/AirLab/Shimizu/Stereo/Calibration/calibrationdata_20180725_01/OpenCV",
#     "IMG_L": "/home/yyhu/Documents/CMU/AirLab/Shimizu/Stereo/CapturedImages/Data_20180725/take03/left/frame0010.jpg",
#     "IMG_R": "/home/yyhu/Documents/CMU/AirLab/Shimizu/Stereo/CapturedImages/Data_20180725/take03/right/frame0010.jpg",
#     "WORKING_DIR": "/home/yyhu/Documents/CMU/AirLab/Shimizu/Stereo/CapturedImages/Data_20180725/take03/Reconstruction",
#     "SGBM": {
#         "minDisp": 900,
#         "numDisp": 160,
#         "blockSize": 15,
#         "disp12MaxDiff": 10,
#         "uniquenessRatio": 10,
#         "preFilterCap": 31,
#         "SGBM_P1": 1800,
#         "SGBM_P2": 7200,
#         "speckleWindowSize": 1000,
#         "speckleRange": 2,
#         "enableVignettingCorrector": true,
#         "enforceVignettingCorrection": false,
#         "enableVCOnlyA": false,
#         "haveWLSFilter": true,
#         "sensorPixelSize": [0.00346, 0.00346]
#     }
# }

DEFAULT_INPUT_DIR = "../images/color/stereo_reconstruction_input"

INPUT_JSON = []

FLAG_BATCH = True

if __name__ == "__main__":

    parser = argparse.ArgumentParser(description='Extract images of steady scenes from multiple bagfiles.')

    parser.add_argument("--input_dir", help = "The the directory which holds all the input JSON files of steady scences.", default = DEFAULT_INPUT_DIR)
    parser.add_argument("--test_skip_reconstruction", help = "Skip the reconstruction. Test use.", action = "store_true", default = False)

    args = parser.parse_args()

    # Search for input JSON files.
    INPUT_JSON = sorted( glob.glob( args.input_dir + "/*.json" ) )

    for inputJSON in INPUT_JSON:
        print("Process %s." % (inputJSON))
        fp = open(inputJSON)
        projectDict = json.load(fp)
        fp.close()

        DR_PARAM    = projectDict["DR_PARAM"]
        IMG_L       = projectDict["IMG_L"]
        IMG_R       = projectDict["IMG_R"]
        WORKING_DIR = projectDict["WORKING_DIR"]

        sc = StereoCamera(DR_PARAM)

        sc.isBatchMode = FLAG_BATCH

        sc.minDisp           = projectDict["SGBM"]["minDisp"]
        sc.numDisp           = projectDict["SGBM"]["numDisp"]
        sc.blockSize         = projectDict["SGBM"]["blockSize"]
        sc.disp12MaxDiff     = projectDict["SGBM"]["disp12MaxDiff"]
        sc.uniquenessRatio   = projectDict["SGBM"]["uniquenessRatio"]
        sc.preFilterCap      = projectDict["SGBM"]["preFilterCap"]
        sc.SGBM_P1           = projectDict["SGBM"]["SGBM_P1"]
        sc.SGBM_P2           = projectDict["SGBM"]["SGBM_P2"]
        # Speckle filter.
        sc.speckleWindowSize = projectDict["SGBM"]["speckleWindowSize"]
        sc.speckleRange      = projectDict["SGBM"]["speckleRange"]
        # WLS filter.
        sc.enableVignettingCorrector   = projectDict["SGBM"]["enableVignettingCorrector"]
        sc.enforceVignettingCorrection = projectDict["SGBM"]["enforceVignettingCorrection"]
        sc.enableVCOnlyA               = projectDict["SGBM"]["enableVCOnlyA"]
        sc.haveWLSFilter               = projectDict["SGBM"]["haveWLSFilter"]
        # Camera sensor.
        sc.sensorPixelSize = projectDict["SGBM"]["sensorPixelSize"] # Actual size of the pixel on the camera sensor.
        sc.reload_params(1) # Reload with higher level of detail.

        sc.enable_thermal = 0

        # TODO: Change this ugly design.
        sc.scriptDir = "/home/yyhu/Documents/CMU/AirLab/stereo_camera/Python/Scripts"

        startTime = time.time()

        if ( False == args.test_skip_reconstruction ):
            # sc.reconstruct(WORKING_DIR, (IMG_L, IMG_R))
            if ( 0 == len( projectDict["depthReferenceFilename"] ) ):
                print("========== Reconstruction without LIDAR guidance. ==========")
                sc.reconstruct_multithreading(WORKING_DIR, (IMG_L, IMG_R), None)
            else:
                print("========== Reconstruction with LIDAR guidance. ==========")
                sc.reconstruct_grid(WORKING_DIR, (IMG_L, IMG_R), projectDict["depthReferenceFilename"], None)

        else:
            print(">>>>>>>>>> Reconstruction skipped due to --test_skip_reconstruction swith. <<<<<<<<<<")

        endTime = time.time()
        
        print("%ds for reconstruction." % ( endTime - startTime ))
        print("")
    
    print("Reconstruction done.")
