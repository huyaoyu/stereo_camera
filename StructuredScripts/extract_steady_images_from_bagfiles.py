#!/usr/bin/python
# Author: Yaoyu Hu <yaoyuh@andrew.cmu.edu>, 20180927

from __future__ import print_function

import argparse
import glob
import os
import sys

# Modify the python search path.
for _p in os.environ["CUSTOM_PYTHON_PATH"].split(":"):
    if ( 0 != len(_p) ):
        sys.path.insert( 0, _p )

# import ipdb; ipdb.set_trace()

from StereoROSScripts.BagExtraction.BagExtraction import StereoTopicSteady

DEFAULT_INPUT_DIR        = "../bags/camera"
DEFAULT_OUTPUT_DIR       = "../images"

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Extract images of steady scenes from multiple bagfiles.')

    parser.add_argument("--inputDir", help = "The the directory which holds all the bagfiles for the steady scences.", default = DEFAULT_INPUT_DIR)
    parser.add_argument("--outputDir", help = "The output directory.", default = DEFAULT_OUTPUT_DIR)
    parser.add_argument("--idxToBeSaved", help= "The desired single index of the data to be saved.", default = 5, type = int)
    parser.add_argument("--debug", help = "Enable debug mode.", action = "store_true", default = False)

    args = parser.parse_args()

    # Search all the input bagfiles.
    bagfiles = sorted( glob.glob( args.inputDir + "/*.bag" ) )

    # Process.
    sts = StereoTopicSteady(["/xic_stereo/left/image_raw", "/xic_stereo/right/image_raw", "/xic_stereo/sxc_test_msg"],\
        ["color/left", "color/right", "color"])

    if ( args.idxToBeSaved < 0 ):
        raise("idxToBeSaved = %d should be a positive integer." % (args.idxToBeSaved))
    else:
        sts.indexToBeSaved = args.idxToBeSaved

    if ( True == args.debug ):
        sts.set_debug()
    
    sts.process( bagfiles, args.outputDir )
