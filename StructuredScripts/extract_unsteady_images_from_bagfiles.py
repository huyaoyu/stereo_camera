#!/usr/bin/python
# Author: Yaoyu Hu <yaoyuh@andrew.cmu.edu>, 20180927

from __future__ import print_function

import argparse
import glob
import os
import sys

import rosbag

# Modify the python search path.
for _p in os.environ["CUSTOM_PYTHON_PATH"].split(":"):
    if ( 0 != len(_p) ):
        sys.path.insert( 0, _p )

# import ipdb; ipdb.set_trace()

from StereoROSScripts.BagExtraction.BagExtraction import StereoTopic

DEFAULT_INPUT_DIR  = "../bags/stereo_calibration"
DEFAULT_OUTPUT_DIR = "../images"

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Extract images of steady scenes from multiple bagfiles.')

    parser.add_argument("--inputDir", help = "The the directory which holds all the bagfiles for the steady scences.", default = DEFAULT_INPUT_DIR)
    parser.add_argument("--outputDir", help = "The output directory.", default = DEFAULT_OUTPUT_DIR)
    parser.add_argument("--debug", help = "Enable debug mode.", action = "store_true", default = False)

    args = parser.parse_args()

    # Search all the input bagfiles.
    bagfiles = sorted( glob.glob( args.inputDir + "/*.bag" ) )

    for bf in bagfiles:
        print("========== Process %s ==========\n" % (bf))

        bag = rosbag.Bag(bf)

        filename = os.path.splitext( os.path.split(bf)[1] )[0]

        subDir = "calibration/%s" % (filename)  

        # Process.
        st = StereoTopic(["/xic_stereo/left/image_raw", "/xic_stereo/right/image_raw", "/xic_stereo/sxc_test_msg", "/ximea_timestamp"],\
            [subDir + "/left", subDir + "/right", subDir, subDir])
        
        if ( True == args.debug ):
            st.set_debug()

        st.process( bag, args.outputDir )
