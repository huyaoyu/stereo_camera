#!/usr/bin/python
# Author: Yaoyu Hu <yaoyuh@andrew.cmu.edu>, 20180927

from __future__ import print_function

import argparse
import glob
import os
import sys

DEFAULT_INPUT_DIR       = "../images/color"
DEFAULT_OUTPUT_DIR      = "../images/color/stereo_reconstruction_input"
DEFAULT_WORKING_DIR     = "../results/stereo_reconstruction"
DEFAULT_CALIBRATION_DIR = "../results/calibration/stereo"
DEFAULT_EXT             = "jpg"
DEFAULT_DISPARITY_MIN   = 500
DEFAULT_DISPARITY_NUM   = 512

TEMPLATE = \
'''{
    "DR_PARAM": "%s",
    "IMG_L": "%s",
    "IMG_R": "%s",
    "WORKING_DIR": "%s",
    "depthReferenceFilename": "%s",
    "SGBM": {
        "minDisp": %d,
        "numDisp": %d,
        "blockSize": 15,
        "disp12MaxDiff": 1,
        "uniquenessRatio": 10,
        "preFilterCap": 31,
        "SGBM_P1": 1800,
        "SGBM_P2": 7200,
        "speckleWindowSize": 1000,
        "speckleRange": 2,
        "enableVignettingCorrector": %s,
        "enforceVignettingCorrection": %s,
        "enableVCOnlyA": false,
        "haveWLSFilter": %s,
        "sensorPixelSize": [0.00346, 0.00346]
    }
}'''

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Extract images of steady scenes from multiple bagfiles.')

    parser.add_argument("--input_dir", help = "The the directory which holds all left images for the steady scences.", default = DEFAULT_INPUT_DIR)
    parser.add_argument("--output_dir", help = "The output directory.", default = DEFAULT_OUTPUT_DIR)
    parser.add_argument("--working_dir", help = "The output directory for the reconstruction.", default = DEFAULT_WORKING_DIR)
    parser.add_argument("--calibration_dir", help = "The directory of the calibration results.", default = DEFAULT_CALIBRATION_DIR)
    parser.add_argument("--ext", help = "The file extension of the images.", default = DEFAULT_EXT)
    parser.add_argument("--disparity_min", help = "The global minimun disparity.", default = DEFAULT_DISPARITY_MIN, type = int)
    parser.add_argument("--disparity_num", help = "The global disparity number.", default = DEFAULT_DISPARITY_NUM, type = int)
    parser.add_argument("--disable_vignetting_correction", help = "Disable the vignetting correction.", action = "store_true", default = False)
    parser.add_argument("--enforce_vignetting_correction", help = "Enforce vignetting correction.", action = "store_true", default = False)
    parser.add_argument("--wls_filter", help = "Enable the WLS filter.", action = "store_true", default = False)
    
    args = parser.parse_args()

    if ( args.disparity_min <= 0 ):
        raise ValueError("disparity_min (%d) must be a positive integer." % (args.disparity_min))

    if ( args.disparity_num <= 0 ):
        raise ValueError("disparity_num (%d) must be a positive integer." % (args.disparity_num))

    if ( args.disparity_num % 16 != 0 ):
        raise ValueError("disparity_num (%d) must be able to be divided by 16." % (args.disparity_num))

    if ( True == args.disable_vignetting_correction ):
        vignettingCorrection = "false"
    else:
        vignettingCorrection = "true"

    if ( True == args.enforce_vignetting_correction ):
        enforceVignettingCorrection = "true"
    else:
        enforceVignettingCorrection = "false"
    
    if ( True == args.wls_filter ):
        wlsFilter = "true"
    else:
        wlsFilter = "false"

    # Search all the input images.
    imgs = sorted( glob.glob( args.input_dir + "/left/*." + args.ext ) )

    for img in imgs:
        fn = os.path.split(img)[1]
        f  = os.path.splitext(fn)[0]

        imgL = args.input_dir + "/left/"  + fn
        imgR = args.input_dir + "/right/" + fn

        outDir = args.output_dir + "/" + f

        # Write the file.
        s = TEMPLATE % ( \
            args.calibration_dir, \
            imgL, imgR, \
            args.working_dir + "/" + f, \
            "", \
            args.disparity_min, args.disparity_num, \
            vignettingCorrection, enforceVignettingCorrection, \
            wlsFilter )
        
        outFile = args.output_dir + "/" + f + ".json"
        fp = open( outFile, "w" )
        fp.write(s)
        fp.close()

        print("Written to %s" % (outFile))
