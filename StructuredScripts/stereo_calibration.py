#!/usr/bin/python

from __future__ import print_function

import argparse
import os
import sys

# Modify the python search path.
for _p in os.environ["CUSTOM_PYTHON_PATH"].split(":"):
    if ( 0 != len(_p) ):
        sys.path.insert( 0, _p )

# import ipdb; ipdb.set_trace()

from StereoPython.StereoCalibrator import StereoCalibrator

G_GRID_ROW = 6
G_GRID_COL = 8

G_CHECKERBOARD_SIZE = 0.119 # m.

G_FILE_EXT = "png"

G_INPUT_DIR   = "../images/calibration/Sample"
G_L_IMAGE_DIR = "left"
G_R_IMAGE_DIR = "right"
G_OUT_DIR     = "../results/calibration/stereo"
G_INDIVIDUALLY_CALIBRATION = False

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Perform the stereo calibration.')

    parser.add_argument("--inputDir", help = "The the directory which holds all the bagfiles for the steady scences.", default = G_INPUT_DIR)
    parser.add_argument("--outputDir", help = "The output directory.", default = G_OUT_DIR)
    parser.add_argument("--inputLeft", help = "The subdirectory for the left camera.", default = G_L_IMAGE_DIR)
    parser.add_argument("--inputRight", help = "The subdirectory fot the right camera.", default = G_R_IMAGE_DIR)
    parser.add_argument("--ext", help = "The file extention of the input images.", default = "png")
    parser.add_argument("--gridRow", help = "The row number of crosses on the checker board.", default = G_GRID_ROW, type = int)
    parser.add_argument("--gridCol", help = "The column number of crosses on the checker board.", default = G_GRID_COL, type = int)
    parser.add_argument("--gridSize", help = "The grid size (edge length of one grid) of the checker board (m).", default = G_CHECKERBOARD_SIZE, type = float)
    parser.add_argument("--individual", help = "Enable individually calibration mode.", action = "store_true", default = False)
    parser.add_argument("--debug", help = "Enable debug mode.", action = "store_true", default = False)
    parser.add_argument("--debugNImages", help = "The number of images to process in the debug mode.", default = 20)

    args = parser.parse_args()

    # Check the input argument.
    if ( args.gridRow <= 0 ):
        raise ValueError("gridRow (%d) must be a positive integer." % (args.gridRow))
    
    if ( args.gridCol <= 0 ):
        raise ValueError("gridCol (%d) must be a positive integer." % (args.gridCol))

    if ( args.gridRow < 6 or args.gridCol < 8 ):
        print( \
"""A checker board with 6x8 or hihger number of crosses is highly recommended to do a proper calibration.
Right now it is %dx%d.""" % (args.gridRow, args.gridCol) )

    if ( args.gridSize <= 0 ):
        raise ValueError("gridSize (%f) must be a positive number." % (args.gridSize))
    
    if ( args.gridSize > 1 ):
        print("A very large grid size (%f) is encounted. Please make sure to use milimeter as the unit." % (args.gridSize))
    
    # Argument check done.

    print("Processing calibration images in %s..." % (args.inputDir))

    calib = StereoCalibrator( \
        (args.gridCol, args.gridRow), \
         args.gridSize, \
         args.inputDir + "/" + args.inputLeft, \
         args.inputDir + "/" + args.inputRight, \
         args.outputDir )

    calib.isDebugging          = args.debug
    calib.debuggingNImagePairs = args.debugNImages

    calib.filenamePattern          = "*." + args.ext
    calib.isIndividuallyCalibrated = args.individual

    calib.calibrate()
    calib.show_calibration_results()
    calib.write_calibration_results(args.outputDir)

    print("Calibration results are written to %s." % (args.outputDir))
    