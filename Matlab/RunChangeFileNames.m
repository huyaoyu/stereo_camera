clear;
clc;
close all;

% WORKING_DIR = '../Data_20180501/right_small';
WORKING_DIR = '../Data_20180501/right';

OMIT_LENGTH = 2;

ORIGINAL_PART = 'left';
NEW_PART      = 'right';

fileList = dir(WORKING_DIR);

nFiles = size(fileList);

for I = 1:1:nFiles
    sFile = fileList(I);
    
    % Check if the length of the file name is longer than OMIT_LENGTH.
    if ( length(sFile.name) <= OMIT_LENGTH )
        continue;
    end
    
    % Compose the new file name.
    newFileName = strrep(sFile.name, ORIGINAL_PART, NEW_PART);
    
    % Rename the file.
    movefile([WORKING_DIR, '/', sFile.name], [WORKING_DIR, '/', newFileName]);
    
    % Show information.
    fprintf('Rename %s to %s.\n', sFile.name, newFileName);
end % I
