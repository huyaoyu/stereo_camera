%
% Description
% ===========
%
% 3D reconstruction from a pair of image frames from a stereo camera.
% 
% Author
% ======
%
% Yaoyu Hu <yyhu_live@outlook.com>
% 
% Date
% ====
%
% Craeted on 2018-05-05 @ Carnegie Mellon University
%

% ================ Clean the workspace. ===============

clear;
clc;
close all;

% ====== User defined varibales and constants. ========

PARAMS_DIR = '/home/yyhu/Documents/CMU/AirLab/Zhimizu/Stereo/Calibration/calibrationdata_20180625_02/MatlabCalibrated';
PARAMS_IN_FILE_MAT = 'Params.mat';

WORKING_DIR = '/home/yyhu/Documents/CMU/AirLab/Zhimizu/Stereo/CapturedImages/Data_20180625/09';

IMAGE_FRAME_LEFT  = [WORKING_DIR, '/16_0.bmp'];
IMAGE_FRAME_RIGHT = [WORKING_DIR, '/16_1.bmp'];

RANGE_LOW_DISPARITY  = 440; % The lower limit for computing the disparity.
RANGE_HIGH_DISPARITY = 920; % The upper limit for computing the disparity.

OUT_DIR = [WORKING_DIR, '/MatlabReconstruct'];

% ==================== External data. ==========================

% Read back the parameters.
load([PARAMS_DIR, '/', PARAMS_IN_FILE_MAT]); % The name of the object is "params".
% Show extrinsic parameters.
showExtrinsics(params);
% Save the extrinsic figure.
outFileName = [OUT_DIR, '/Extrinsics'];
saveas(gcf, outFileName, 'fig');
saveas(gcf, outFileName, 'png');

% ================== Process image data. =======================

% Read the two images.
imgLeft  = imread(IMAGE_FRAME_LEFT);
imgRight = imread(IMAGE_FRAME_RIGHT);

% Rectify the frames.
[frameLeftRect, frameRightRect] = ...
    rectifyStereoImages(imgLeft, imgRight, params);

h = figure('Name', 'Recitfied frames',...
    'NumberTitle', 'off');
imshow(stereoAnaglyph(frameLeftRect, frameRightRect));
title('Rectified frames');
% Save the rectified figure.
outFileName = [OUT_DIR, '/RectifiedFrames'];
saveas(h, outFileName, 'fig');
saveas(h, outFileName, 'png');

% Compute the disparity map.
frameLeftGray  = rgb2gray(frameLeftRect);
frameRightGray = rgb2gray(frameRightRect);
disparityMap   = disparity(frameLeftGray, frameRightGray,...
    'DisparityRange', [RANGE_LOW_DISPARITY, RANGE_HIGH_DISPARITY],...
    'ContrastThreshold', 0.5,...
    'UniquenessThreshold', 20,...
    'DistanceThreshold', 64);

h = figure('Name', 'Disparity map',...
    'NumberTitle', 'off');
imshow(disparityMap, [RANGE_LOW_DISPARITY, RANGE_HIGH_DISPARITY]);
title('Disparity map');
colormap jet;
colorbar;
% Save the disparity map.
outFileName = [OUT_DIR, '/DisparityMap'];
saveas(h, outFileName, 'fig');
saveas(h, outFileName, 'png');

% Reconstruct the 3D Scene.
points3D = reconstructScene(disparityMap, params);
points3D = points3D ./ 1000;
ptCloud  = pointCloud(points3D, 'Color', frameLeftRect);

lower   = min([ptCloud.XLimits ptCloud.YLimits]);
upper   = max([ptCloud.XLimits ptCloud.YLimits]);
xLimits = [lower upper];
yLimits = [lower upper];
zLimits = ptCloud.ZLimits;

player3D = pcplayer(...
    xLimits,...
    yLimits,...
    zLimits,...
    'VerticalAxis', 'Y',...
    'VerticalAxisDir', 'Down');

xlabel(player3D.Axes, 'X (m)');
ylabel(player3D.Axes, 'Y (m)');
zlabel(player3D.Axes, 'Z (m)');

view(player3D, ptCloud);
% Save the point cloud.

