%
% Description
% ===========
%
% Stereo calibration. 
% 
% Author
% ======
%
% Yaoyu Hu <yyhu_live@outlook.com>
% 
% Date
% ====
%
% Craeted on 2018-05-05 @ Carnegie Mellon University
%

% ================ Clean the workspace. ===============

clear;
clc;
close all;

% ====== User defined varibales and constants. ========

% DIR_LEFT  = '../Data_20180501/left_small_resized';
% DIR_RIGHT = '../Data_20180501/right_small';

% DIR_LEFT    = '../Data_20180501/left_dst_resized';
% DIR_RIGHT   = '../Data_20180501/right_dst';
% OUT_DIR     = './Calibrated';
% SQUARE_SIZE = 37; % Unit: mm.

% WORK_DIR     = '../../Wenshan/LentinCalibration/calibration_data_17-05-2018/calib_17_05_00_Static';
% DIR_LEFT     = [WORK_DIR, '/LeftOri'];
% DIR_RIGHT    = [WORK_DIR, '/RightCropped'];
% OUT_DIR      = [WORK_DIR, '/Calibrated'];
% SQUARE_SIZE  = 107; % Unit: mm.

% WORK_DIR     = '../../Zeng/Test';
% DIR_LEFT     = [WORK_DIR, '/SeparatedImages_L'];
% DIR_RIGHT    = [WORK_DIR, '/SeparatedImages_R'];
% OUT_DIR      = [WORK_DIR, '/Calibrated'];
% SQUARE_SIZE  = 107; % Unit: mm.

WORK_DIR     = '/home/yyhu/Documents/CMU/AirLab/Zhimizu/Stereo/Calibration/calibrationdata_20180625_02';
DIR_LEFT     = [WORK_DIR, '/Left'];
DIR_RIGHT    = [WORK_DIR, '/Right'];
OUT_DIR      = [WORK_DIR, '/MatlabCalibrated'];
SQUARE_SIZE  = 119; % Unit: mm.

PARAMS_OUT_FILE_MAT        = 'Params.mat';
PARAMS_OUT_FILE_JSON       = 'Params.json';
IMAGE_POINTS_OUT_FILE_MAT  = 'ImagePoints.mat';
IMAGE_POINTS_OUT_FILE_JSON = 'ImagePoints.json';
WORLD_POINTS_OUT_FILE_MAT  = 'WorldPoints.mat';
WORLD_POINTS_OUT_FILE_DAT  = 'WorldPoints.dat';

% ============= Single camera calibration. ==========

% % Calibrate the left camera.
% fprintf('Begin calibrating the left camera...\n');
% [paramsL, imagePointsL, worldPointsL, imagesL] = calibrate_single_camera(...
%     DIR_LEFT, SQUARE_SIZE);
% 
% % Calibrate the right camera.
% fprintf('Begin calibrating the right camera...\n');
% [paramsR, imagePointsR, worldPointsR, imagesR] = calibrate_single_camera(...
%     DIR_RIGHT, SQUARE_SIZE);

% ============= Stereo camera calibration. ==========

% Calibrate the stereo camera.
fprintf('Begin calibrating the stereo camera...\n');
[params, imagePoints, worldPoints, imagesL, imagesR] = calibrate_stereo_camera(...
    DIR_LEFT, DIR_RIGHT, SQUARE_SIZE);

% Change the object into struct.
paramsStruct = params.toStruct();
% Transform the struct into JSON string.
paramsJSON = jsonencode(paramsStruct);
% Save the JSON string into a file.
outFile = [ OUT_DIR, '/', PARAMS_OUT_FILE_JSON ];
fp = fopen( outFile, 'w');
fprintf(fp, '%s', paramsJSON);
fclose(fp);
% Save params as mat file.
outFile = [ OUT_DIR, '/', PARAMS_OUT_FILE_MAT ];
save(outFile, 'params');

% Save image points.
imagePointsJSON = jsonencode(imagePoints);
outFile = [ OUT_DIR, '/', IMAGE_POINTS_OUT_FILE_JSON ];
fp = fopen( outFile, 'w' );
fprintf(fp, '%s', imagePointsJSON);
fclose(fp);
% Save imagePoints as mat file.
outFile = [ OUT_DIR, '/', IMAGE_POINTS_OUT_FILE_MAT ];
save(outFile, 'imagePoints');

% Save world points.
outFile = [ OUT_DIR, '/', WORLD_POINTS_OUT_FILE_DAT ];
save(outFile, 'worldPoints', '-ascii');
outFile = [ OUT_DIR, '/', WORLD_POINTS_OUT_FILE_MAT ];
save(outFile, 'worldPoints');

% Show infomation.
figure;
showExtrinsics(params);
figure;
showReprojectionErrors(params);
