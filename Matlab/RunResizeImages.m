%
% Description
% ===========
%
% Resize the images captured by the camera with larger resolution.
%
% The user should specify variable RESIZE_ROW to indicate the desired
% resolution. Note that the user should check upon the aspect ratio first.
% 
% 
% Author
% ======
%
% Yaoyu Hu <yyhu_live@outlook.com>
% 
% Date
% ====
%
% Craeted on 2018-05-05 @ Carnegie Mellon University
%

clear;
clc;
close all;

% SOURCE_DIR = '../Data_20180501/left_dst';
% TARGET_DIR = '../Data_20180501/left_dst_resized';

% SOURCE_DIR = '../Data_20180501/left_small';
% TARGET_DIR = '../Data_20180501/left_small_resized';

SOURCE_DIR = '../Data_20180508/Bag02C02';
TARGET_DIR = '../Data_20180508/Bag02C02_resized';

RESIZE_ROW = 2672; % The target number of rows after resizing.

% Find all the image files.
fileList = dir(SOURCE_DIR);
% Get the number of files contained in the folder.
nFiles = length(fileList);

% Jump over the first two files, which are the "." and ".." files.
for I = 3:1:nFiles
    % Read a image.
    fileStruct = fileList(I);
    inputFileName = [fileStruct.folder, '/', fileStruct.name];
    img = imread(inputFileName);
    
    % Resize.
    imgResized = imresize(img, [RESIZE_ROW, NaN]);
    
    % Write the resized image to disk.
    outputFileName = [TARGET_DIR, '/', fileStruct.name];
    imwrite(imgResized, outputFileName);
    
    % Show information.
    fprintf('%s processed.\n', inputFileName);
end % I