function [params, imagePoints, worldPoints, images] =...
    calibrate_single_camera(imageFolderName, squareSize)
% imageFolderName - The folder name of the image folder.
% squareSize - The size of the square on the checkerboard in mm.
%

% Create a imageSet object.
images = imageSet(imageFolderName);
imageFileNames = images.ImageLocation;

% Detect the calibration pattern.
[imagePoints, boardSize] = detectCheckerboardPoints(imageFileNames);

% Generate the world coordinates.
worldPoints = generateCheckerboardPoints(boardSize, squareSize);

% Calibration.
% img = readimage(images, 1);
% imageSize = [ size(img, 1), size(img, 2) ];

params = estimateCameraParameters(imagePoints, worldPoints);
