clear;
clc;
close all;

CAMERA_FOCAL_LENGTH        = 1.4053;   % Focal length, mm.
% CAMERA_FOCAL_LENGTH        = 0.755;   % Focal length, mm.
% CAMERA_SENSOR_SIZE         = Stereo.SensorSize.fullFrame.w;   % Sensor size, sensor width, mm.
% CAMERA_SENSOR_SIZE         = Stereo.SensorSize.SonyICX834.w;   % Sensor size, sensor width, mm.
CAMERA_SENSOR_SIZE         = Stereo.SensorSize.OV07251.w;   % Sensor size, sensor width, mm.
CAMERA_INITIAL_SQUARE_SIZE = 640; % Default square resolution, pixel.
CAMERA_IMAGE_SIZE_W        = 640; % Actual image size, pixel.
CAMERA_IMAGE_SIZE_H        = 480; % Actual image size, pixel.
CAMERA_ENCODING_LEGNTH     = 8;    % Encoding length, bit.
CAMERA_MAXIMUM_FRAME_RATE  = 120;  % Maximum frame rate of the camera, fps.
CAMERA_PIXELS_IN_CRACK     = 0.01;    % 2 pixels across a crack, pix/mm.

STEREO_INITIAL_BASELINE    = 0.4;  % Default baseline for the stereo camera, m.
STEREO_DESIRED_Z_PRECISION = 20; % m/pix.
STEREO_INITIAL_FRAME_RATE  = 3;    % Default frame rate of the stereo camera, fps.
STEREO_OBJECTIVE_DISTANCE  = 100;    % Objective distance, m.

GIGABIT_BASE = 1e9;
BAND_WIDTH   = GIGABIT_BASE * 5;

% Create a default camera.
C = Stereo.CameraBase(...
    CAMERA_FOCAL_LENGTH,...
    CAMERA_SENSOR_SIZE,...
    CAMERA_INITIAL_SQUARE_SIZE,...
    CAMERA_ENCODING_LEGNTH,...
    CAMERA_MAXIMUM_FRAME_RATE);

% Get the desired resolution.
[R] = C.calc_desired_image_square_size(...
    STEREO_OBJECTIVE_DISTANCE,...
    CAMERA_PIXELS_IN_CRACK);

if ( R > CAMERA_IMAGE_SIZE_W )
    fprintf('# WARNING:\nThe actual image szie (%d) is smaller than the desired size (%d).\n\n',...
        CAMERA_IMAGE_SIZE_W, R);
end

% Refine the square resolution.
C = C.set_image_size(CAMERA_IMAGE_SIZE_W, CAMERA_IMAGE_SIZE_H);

% Get the pixel density at distance.
[c] = C.calc_pixel_density_at_distance(STEREO_OBJECTIVE_DISTANCE);

if ( c < CAMERA_PIXELS_IN_CRACK )
    fprintf('# WARNING!\nPixel in crack (%.2f) is less than desired value (%d).\n',...
        c, CAMERA_PIXELS_IN_CRACK);
end

% Get the angle of view.
[alphaD] = C.get_angle_of_view_degree();

% Create a simple stereo camera.
ST = Stereo.StereoSimple(...
    STEREO_INITIAL_BASELINE, C);

% Get the desired baseline.
[b] = ST.calc_desired_baseline(...
    STEREO_OBJECTIVE_DISTANCE,...
    STEREO_DESIRED_Z_PRECISION);

if ( b > STEREO_INITIAL_BASELINE )
    fprintf('# WARNING!\nThe initial base line (%.3f) is shorter than the desired value (%.3f).\n\n',...
        STEREO_INITIAL_BASELINE, b);
end

% Get the percentage of overlap V.
[V] = ST.calc_overlap(STEREO_OBJECTIVE_DISTANCE);

% Get the theoretical bandwidth requirement.
[B] = ST.calc_desired_bandwidth(STEREO_INITIAL_FRAME_RATE) / BAND_WIDTH;

% Get the theoretical maximum frame rate.
[qm] = ST.calc_maximum_frame_rate(BAND_WIDTH);

if ( qm < STEREO_INITIAL_FRAME_RATE )
    fprintf('# WARNING\nThe theoretical maximum frame rate (%.2f) is lower than the desired value (%.2f).\n\n',...
        qm, STEREO_INITIAL_FRAME_RATE);
end

% Show information.
fprintf('========== Camera info ==========\n');
ST.camera.show_info();
fprintf('\n');

fprintf('========== Stereo info ==========\n');
ST.show_info();
fprintf('\n');

fprintf('========== Selection info ==========\n');
fprintf('Objective distance (m): %.2f.\n', STEREO_OBJECTIVE_DISTANCE);
fprintf('Desired pixels in crack (pix): %d.\n', CAMERA_PIXELS_IN_CRACK);
fprintf('Actual pixels in crack (pix): %.2f\n', c);
fprintf('Desired depth precision (m/pix): %f.\n', STEREO_DESIRED_Z_PRECISION);
fprintf('Calculated baseline (m): %.3f.\n', b);
fprintf('Overlap percentage (%%): %.2f.\n', V * 100);
fprintf('Desired frame rate (fps): %d.\n', STEREO_INITIAL_FRAME_RATE);
fprintf('Theoretical bandwidth requirement (%.1f GBit/s): %.3f.\n', BAND_WIDTH / GIGABIT_BASE, B);
fprintf('Theoretical maximum frame rate (fps): %.2f.\n', qm)
