% This script is composed specifically for LenaCV 3D Camera.
%
% Author
% ======
%
% Yaoyu Hu <yyhu_live@outlook.com>
%
% Date
% ====
%
% Created on 2018-05-21 @ Carnegie Mellon University.
%

clear;
clc;
close all;

% ============== Constants. ==================

MATLAB_EXIST_FOLDER = 7;

% ============= User input. ==================

WORKING_DIR = '../../ZED/CapturedImages';
DIR_ORI     = 'OriImages';
DIR_OUT_L   = 'SeparatedImages_L';
DIR_OUT_R   = 'SeparatedImages_R';

% DIR_OUT_L   = 'SeparatedSingleFolder';
% DIR_OUT_R   = 'SeparatedSingleFolder';

FLAG_SINGLE_DIR = 0;
FLAG_INVERSE    = 0;

WIDTH  = 2560; % Width of the original image, pixel.
HEIGHT = 720;  % Height of the original image, pixel.
HALF   = WIDTH / 2;

% =========== End of user input. =================

% Prepare filenames.
pathOri      = [ WORKING_DIR, '/', DIR_ORI ];
pathOutLeft  = [ WORKING_DIR, '/', DIR_OUT_L ];
pathOutRight = [ WORKING_DIR, '/', DIR_OUT_R ];

% Test if the output directories exist.

if ( MATLAB_EXIST_FOLDER ~= exist(pathOutLeft) )
    fprintf('Output folder %s does not exist. Automatically create that folder.\n', pathOutLeft);
    mkdir(pathOutLeft)
end

if ( MATLAB_EXIST_FOLDER ~= exist(pathOutRight) )
    fprintf('Output folder %s does not exist. Automatically create that folder.\n', pathOutRight);
    mkdir(pathOutRight)
end

% Gather the filenames.
imageSetOri = imageSet(pathOri);
imageFilenamesOri = imageSetOri.ImageLocation;
% Transpose to let it be easy to read by human.
imageFilenamesOri = imageFilenamesOri';

% Looping.
nImages = length( imageFilenamesOri );

for I = 1:1:nImages
    % Read the image.
    filename = imageFilenamesOri{I};
    
    fprintf('Processing %s...\n', filename);
    
    img = imread(filename);
    
    % Separate.
    imgL = img(...
        :,...
        1 : HALF,...
        : );
    
    imgR = img(...
        :,...
        HALF + 1 : end,...
        : );
    
    if ( 1 == FLAG_INVERSE )
        temp = imgR;
        imgR = imgL;
        imgL = temp;
    end
    
    % Get the filename parts.
    [path, name, ext] = fileparts(filename);
    
    % Save the separated image.
    if ( 0 == FLAG_SINGLE_DIR )
        nameL = [ pathOutLeft,  '/', [name, '_L'], ext ];
        nameR = [ pathOutRight, '/', [name, '_R'], ext ];
    else
        nameL = sprintf('%s/left-%04d%s', pathOutLeft, I, ext);
        nameR = sprintf('%s/right-%04d%s', pathOutLeft, I, ext);
    end
    
    imwrite(imgL, nameL);
    imwrite(imgR, nameR);
end % I

fprintf('Done.\n');
