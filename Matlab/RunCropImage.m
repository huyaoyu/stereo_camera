clear;
clc;
close all;

WORKING_DIR = '../../Wenshan/LentinCalibration/calibration_data_17-05-2018/calib_17_05_00_Static';
DIR_ORI     = 'RightOri';
DIR_OUT     = 'RightCropped';

CROP_SIZE_TOP    = 60;
CROP_SIZE_BOTTOM = 60;
CROP_SIZE_LEFT   = 0;
CROP_SIZE_RIGHT  = 0;

% Prepare filenames.
pathOri = [ WORKING_DIR, '/', DIR_ORI ];
pathOut = [ WORKING_DIR, '/', DIR_OUT ];

imageSetOri = imageSet(pathOri);
imageFilenamesOri = imageSetOri.ImageLocation;

imageFilenamesOri = imageFilenamesOri';

nImages = length( imageFilenamesOri );

for I = 1:1:nImages
    % Read the image.
    filename = imageFilenamesOri{I};
    
    fprintf('Processing %s...\n', filename);
    
    img = imread(filename);
    imgCropped = img(...
        CROP_SIZE_TOP  + 1 : end - CROP_SIZE_BOTTOM,...
        CROP_SIZE_LEFT + 1 : end - CROP_SIZE_RIGHT,...
        : );
    
    % Get the filename parts.
    [path, name, ext] = fileparts(filename);
    
    % Save the cropped image.
    newName = [ pathOut, '/', name, ext ];
    imwrite(imgCropped, newName);
end % I

fprintf('Done.\n');
