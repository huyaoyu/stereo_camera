classdef StereoSimple < Stereo.StereoSystemBase
    % Assuming the two cameras are identical.
    
    properties
        camera;
    end
    
    methods
        function obj = StereoSimple(b, camera)
            obj@Stereo.StereoSystemBase(b);
            
            obj = obj.add_cameras(camera, camera);
            obj.camera = obj.cameras(1);
        end
        
        function [desrB] = calc_desired_baseline(obj, Z, Ez)
            % Z - Reconstructed distance, m.
            % Ez - Desired precision of depth reconstruction, m/pix.
            
            desrB = Z^2 / ( obj.camera.fc *  Ez  );
        end
        
        function [V] = calc_overlap(obj, L)
            V = 1 - obj.b / ( 2 * L * tan( obj.camera.alpha / 2 ) );
            if ( V < 0 )
                V = 0;
            end
        end
        
    end
end