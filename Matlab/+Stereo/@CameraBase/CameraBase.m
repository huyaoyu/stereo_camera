classdef CameraBase
    properties
        S     = 0; % Sensor size, mm.
        f     = 0; % Focal length, mm.
        fc    = 0; % Focal length, pix.
        R     = 0; % Square length, pixels per edge, pix
        Rh    = 0; % Height, pix.
        Rw    = 0; % Width, pix.
        alpha = 0; % Horizontal angle of view, rad.
        en    = 8; % Encoding length.
        qm    = 0; % Maximum frame rate, fps.
    end
    
    methods
        % Constructor.
        function obj = CameraBase( f, S, R, en, qm )
            obj.f  = f;
            obj.S  = S;
            obj.R  = R;
            obj.Rh = R;
            obj.Rw = R;
            obj.en = en;
            obj.qm = qm;
            
            obj.alpha = obj.calc_angle_of_view();
            obj.fc = obj.f / obj.S * obj.Rw;
        end
        
        function [alpha] = calc_angle_of_view(obj)
            % f - Focal length, mm
            % S - Senor size, sensor width, mm
            %

            alpha = 2 * atan( obj.S / (2 * obj.f) );
        end
        
        function [alphaD] = get_angle_of_view_degree(obj)
            alphaD = obj.alpha / pi * 180;
        end
        
        function [desrR] = calc_desired_image_square_size(obj, L, c)
            desrR = 2 * L * tan( obj.alpha / 2 ) * 1000 * c;
        end
        
        function [c] = calc_pixel_density_at_distance(obj, L)
            c = obj.Rw / ( 2 * L * tan( obj.alpha / 2 ) * 1000 );
        end
        
        function obj = set_image_square_size(obj, R)
            obj.R  = R;
            obj.Rh = R;
            obj.Rw = R;
            
            obj.fc = obj.f / obj.S * obj.Rw;
        end
        
        function obj = set_image_size(obj, Rw, Rh)
            obj.Rh = Rh;
            obj.Rw = Rw;
            
            obj.R  = Rw;
            
            obj.fc = obj.f / obj.S * obj.Rw;
        end
        
        function show_info(obj)
            fprintf('Sensor size (mm): %d.\n', obj.S);
            fprintf('Focal length (mm): %d.\n', obj.f);
            fprintf('Focal length in pixel (pix): %f.\n', obj.fc);
            fprintf('Image size (pix): %d x %d.\n', obj.Rw, obj.Rh);
            fprintf('Angle of view (rad): %f.\n', obj.alpha);
            fprintf('Angle of view (degree): %f.\n', obj.get_angle_of_view_degree());
            fprintf('Encoding length (bit): %d.\n', obj.en);
            fprintf('Maximum frame rate (fps): %d.\n', obj.qm);
        end

    end
end