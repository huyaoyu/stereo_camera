classdef SensorSize
    properties (Constant)
        % Full frame.
        fullFrame = struct('w',36, 'h', 24);

        % APS-H.
        APS_H = struct('w', 27.9, 'h', 18.6);

        % APS-C.
        APS_C  = struct('w', 23.6, 'h', 15.6);

        % APS-C (Canon).
        APS_C_C = struct('w', 22.2, 'h', 14.8);

        % 1.5"
        OneFiveInches = struct('w', 18.7, 'h', 14);
        
        % Sony ICX834.
        SonyICX834 = struct('w', 13.2, 'h', 8.8);
        
        % Sony IMX253
        SonyIMX253 = struct('w', 14.19, 'h', 10.38);
        
        % OV07251
        OV07251 = struct('w', 1.968, 'h', 1.488);
    end
end