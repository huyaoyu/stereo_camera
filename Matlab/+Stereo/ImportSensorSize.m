% Sensor sizes.
% https://newatlas.com/camera-sensor-size-guide/26684/

% Units are all mm.

% Full frame.
SENSOR_SIZE.fullFrame.w = 36;
SENSOR_SIZE.fullFrame.h = 24;

% APS-H.
SENSOR_SIZE.APS_H.w = 27.9;
SENSOR_SIZE.APS_H.h = 18.6;

% APS-C.
SENSOR_SIZE.APS_C.w = 23.6;
SENSOR_SIZE.APS_C.h = 15.6;

% APS-C (Canon).
SENSOR_SIZE.APS_C_C.w = 22.2;
SENSOR_SIZE.APS_C_C.h = 14.8;

% 1.5"
SENSOR_SIZE.OneFiveInches.w = 18.7;
SENSOR_SIZE.OneFiveInches.h = 14;