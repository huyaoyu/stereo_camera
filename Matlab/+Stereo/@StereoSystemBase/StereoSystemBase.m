classdef StereoSystemBase
    properties
        cameras; % The two camers
        b = 0; % Baseline, m.
    end
    
    methods
        function obj = StereoSystemBase(b)
            obj.b = b;
        end
        
        function obj = add_cameras(obj, c1, c2)
            obj.cameras = [c1, c2];
        end
        
        function obj = set.b(obj, b)
            obj.b = b;
        end
        
        function [desrBandwidth] = calc_desired_bandwidth(obj, q)
            % q - Frame rate, fps.
            
            frameSize1 = obj.cameras(1).Rh * obj.cameras(1).Rw * obj.cameras(1).en;
            frameSize2 = obj.cameras(2).Rh * obj.cameras(2).Rw * obj.cameras(1).en;
            
            desrBandwidth = ( frameSize1 + frameSize2 ) * q;
        end
        
        function [qm] = calc_maximum_frame_rate(obj, B)
            frameSize1 = obj.cameras(1).Rh * obj.cameras(1).Rw * obj.cameras(1).en;
            frameSize2 = obj.cameras(2).Rh * obj.cameras(2).Rw * obj.cameras(1).en;
            
            qm = B / ( frameSize1 + frameSize2 );
        end
        
        function show_info(obj)
            fprintf('Baseline (m): %f\n', obj.b);
        end
        
    end
end