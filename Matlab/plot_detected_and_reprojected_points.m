function plot_detected_and_reprojected_points(imageFileName, imagePoints, params)
% imageFileName - The file name, string.
% imagePoints - An 3D array.
% params - A cameraParameters object.
%

figure;
imshow(imageFileName);
hold on;
plot(imagePoints(:,1,1), imagePoints(:,2,1),'go');
plot(params.ReprojectedPoints(:,1,1),params.ReprojectedPoints(:,2,1),'r+');
legend('Detected Points','ReprojectedPoints');
hold off;
