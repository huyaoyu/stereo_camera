function [params, imagePoints, worldPoints, imagesL, imagesR] =...
    calibrate_stereo_camera(imageFolderNameL, imageFolderNameR, squareSize)
% imageFolderNameL - The folder name for the images from the left camera.
% imageFolderNameR - The folder name for the images from the right camera.
% squareSize - The size of the square on the checkerboard in mm.
% params - The calibrated parameters.
% imagePoints - The detected checkerboard points, coordinates expressed in
% pixel. Contains all the detected checkerboard points of every image.
% worldPoints - The generated checkerboard points, coordinates expressed in
% mm.
% imagesL - The image set representing the left camera.
% imagesR - The image set representing the right camera.
%

% Author
% ======
%
% Yaoyu Hu <yyhu_live@outlook.com>
% 
% Date
% ====
%
% Craeted on 2018-05-05 @ Carnegie Mellon University
%

% Create a imageSet object.
imagesL = imageSet(imageFolderNameL);
imageFileNamesL = imagesL.ImageLocation;

imagesR = imageSet(imageFolderNameR);
imageFileNamesR = imagesR.ImageLocation;

% Detect the calibration pattern.
[imagePoints, boardSize] =...
    detectCheckerboardPoints(imageFileNamesL, imageFileNamesR);

% Generate the world coordinates.
worldPoints = generateCheckerboardPoints(boardSize, squareSize);

% Calibration.
params = estimateCameraParameters(imagePoints, worldPoints);
